package com.proyectotpv.proyectotpv.interfaces;

import com.proyectotpv.proyectotpv.modelos.cambioclave.CambioClaveModel;

public interface CambioClaveInterface {
    public int cambiarClave(CambioClaveModel datosCambioClave) throws Exception;
    public void cerrarConexion();
}
