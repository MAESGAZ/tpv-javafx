package com.proyectotpv.proyectotpv.interfaces;

import com.proyectotpv.proyectotpv.modelos.logininicio.InicioModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.RolModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import javafx.collections.ObservableList;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public interface UsuarioInterface {
    public void nuevoUsuario(UsuarioModel usuarioModel) throws Exception;
    public UsuarioModel iniciarSesion(InicioModel usuario) throws Exception;
    public String obtenerClaveUsuario(InicioModel usuario) throws Exception;
    public ObservableList<RolModel> obtenerRoles() throws Exception;
    public void activarUsuario(UsuarioModel usuarioModel) throws Exception;
    public void desactivarUsuario(UsuarioModel usuarioModel) throws Exception;
    public void modificarUsuario(UsuarioModel usuarioModel) throws Exception;
    public ObservableList<UsuarioModel> listadoUsuarios() throws Exception;
    public void resetearClave(UsuarioModel usuarioModel) throws Exception;
    public void eliminarUsuario(UsuarioModel usuarioModel) throws Exception;
    public void cerrarConexion();
}
