package com.proyectotpv.proyectotpv.interfaces;

import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import javafx.collections.ObservableList;

public interface PedidoInterface {
    public int crearPedido(PedidoModel pedido) throws Exception;
    public ObservableList<PedidoModel> listadoPedidos() throws Exception;
    public void obtenerIdPedido(PedidoModel pedido) throws Exception;
    public PedidoModel obtenerPedido(PedidoModel pedido) throws Exception;
    public int modificarPedido(PedidoModel pedido) throws Exception;
    public void cerrarConexion();
}
