package com.proyectotpv.proyectotpv.interfaces;

import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import javafx.collections.ObservableList;

public interface CategoriaInterface {
    public int nuevaCategoria(CategoriaModel categoria) throws Exception;
    public int modificarCategoria(CategoriaModel categoria) throws Exception;
    public int activarCategoria(CategoriaModel categoria) throws Exception;
    public int desactivarCategoria(CategoriaModel categoria) throws Exception;
    public ObservableList<CategoriaModel> listadoCategorias() throws Exception;
    public ObservableList<CategoriaModel> listadoCategoriasActivas() throws Exception;
    public int eliminarCategoria(CategoriaModel categoria) throws Exception;
    public void eliminarProductosCategoria(CategoriaModel categoria) throws Exception;
    public void cerrarConexion();
}
