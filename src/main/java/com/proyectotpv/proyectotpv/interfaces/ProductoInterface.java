package com.proyectotpv.proyectotpv.interfaces;

import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import com.proyectotpv.proyectotpv.modelos.main.producto.ProductoModel;
import javafx.collections.ObservableList;

public interface ProductoInterface {
    public ObservableList<ProductoModel> listadoProductos() throws Exception;
    public ObservableList<ProductoModel> listadoProductosPorCategoria(CategoriaModel categoria) throws Exception;
    public ObservableList<ProductoModel> listadoProductosActivos() throws Exception;
    public int nuevoProducto(ProductoModel producto) throws Exception;
    public int activarProducto(ProductoModel producto) throws Exception;
    public int desactivarProducto(ProductoModel producto) throws Exception;
    public int modificarProducto(ProductoModel producto) throws Exception;
    public int eliminarProducto(ProductoModel producto) throws Exception;
    public void cerrarConexion();
}
