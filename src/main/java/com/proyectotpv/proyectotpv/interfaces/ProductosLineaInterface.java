package com.proyectotpv.proyectotpv.interfaces;

import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import com.proyectotpv.proyectotpv.modelos.pedido.ProductoLineaModel;
import javafx.collections.ObservableList;

public interface ProductosLineaInterface {
    public int agregarProductoLinea(ProductoLineaModel productoLineaModel) throws Exception;
    public ObservableList<ProductoLineaModel> obtenerProductosPedido(PedidoModel pedido) throws Exception;
    public int cambiarCantidadProductoLinea(ProductoLineaModel productoLinea) throws Exception;
    public int cambiarPrecioProductoLinea(ProductoLineaModel productoLinea) throws Exception;
    public int eliminarProductoLinea(ProductoLineaModel producto) throws Exception;
    public void eliminarPedido(PedidoModel pedido) throws Exception;
    public void guardarEnHistorico(ObservableList<ProductoLineaModel> productos) throws Exception;
    public void rellenarProducto(ProductoLineaModel producto) throws Exception;
    public void cerrarConexion();
}
