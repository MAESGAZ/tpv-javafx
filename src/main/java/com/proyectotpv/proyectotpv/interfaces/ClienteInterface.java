package com.proyectotpv.proyectotpv.interfaces;

import com.proyectotpv.proyectotpv.modelos.pedido.ClienteModel;
import javafx.collections.ObservableList;

public interface ClienteInterface {
    public int nuevoCliente(ClienteModel cliente) throws Exception;
    public ObservableList<ClienteModel> listadoClientes() throws Exception;
    public int activarCliente(ClienteModel cliente) throws Exception;
    public int desactivarCliente(ClienteModel cliente) throws Exception;
    public int modificarCliente(ClienteModel cliente) throws Exception;
    public int eliminarCliente(ClienteModel cliente) throws Exception;
    public void buscarCliente(ClienteModel cliente) throws Exception;
    public void cerrarConexion();
}
