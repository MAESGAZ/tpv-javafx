package com.proyectotpv.proyectotpv.interfaces;

import com.proyectotpv.proyectotpv.modelos.pedido.ClienteModel;
import com.proyectotpv.proyectotpv.modelos.pedido.PedidoDomicilioModel;
import javafx.collections.ObservableList;

public interface PedidosDomicilioInterface {
    public ObservableList<PedidoDomicilioModel> listadoPedidosDomicilio() throws Exception;
    public int crearPedidoDomicilio(PedidoDomicilioModel pedido) throws Exception;
    public PedidoDomicilioModel obtenerPedidoPorCliente(ClienteModel cliente) throws Exception;
    public boolean comprobarClientePedidoActivos(ClienteModel cliente) throws Exception;
    public void cerrarConexion();
}
