package com.proyectotpv.proyectotpv.basedatos;
import com.proyectotpv.proyectotpv.recursos.Constantes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDatos {
    private Connection connection;

    public BaseDatos() throws SQLException, ClassNotFoundException {
        Class.forName(Constantes.BBDD_CONF_DRIVER);
        this.connection = DriverManager.getConnection(Constantes.BBDD_CONF_URL + Constantes.BBDD_CONF_HOSTNAME + ":" +
                Constantes.BBDD_CONF_PORT+"/"+Constantes.BBDD_CONF_DATABASE, Constantes.BBDD_CONF_USUARIO, Constantes.BBDD_CONF_PASSWORD);
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
