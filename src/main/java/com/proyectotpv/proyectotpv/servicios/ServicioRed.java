package com.proyectotpv.proyectotpv.servicios;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class ServicioRed {
    private HttpURLConnection urlConnection;
    private URL url;

    public ServicioRed(String url) throws IOException {
        this.url = new URL(url);
        this.urlConnection = (HttpURLConnection) this.url.openConnection();
        this.urlConnection.setRequestProperty("Content-Type", "application/json");
        this.urlConnection.setRequestProperty("Accept", "application/json");
    }

    public void setProtocoloRed(String protocolo) throws ProtocolException {
        this.urlConnection.setRequestMethod(protocolo);
    }

    public void setSalidaDatos(boolean salida) {
        this.urlConnection.setDoOutput(salida);
    }

    public boolean response200FromServer() throws IOException {
        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
            return true;
        else
            return false;
    }

    public void enviarDatosSalida(String datos) throws IOException {
        OutputStream os = this.urlConnection.getOutputStream();
        byte[] salida = datos.getBytes(StandardCharsets.UTF_8);
        os.write(salida, 0, salida.length);
        os.close();
    }

    public String obtenerMensajeRespuesta() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    public void cerrarConexion() {
        this.urlConnection.disconnect();
    }
}
