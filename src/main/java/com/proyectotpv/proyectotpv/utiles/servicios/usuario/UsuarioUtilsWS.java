package com.proyectotpv.proyectotpv.utiles.servicios.usuario;

import com.proyectotpv.proyectotpv.interfaces.UsuarioInterface;
import com.proyectotpv.proyectotpv.modelos.logininicio.InicioModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.RolModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import javafx.collections.ObservableList;

public class UsuarioUtilsWS implements UsuarioInterface {
    @Override
    public void nuevoUsuario(UsuarioModel usuarioModel) throws Exception {

    }

    @Override
    public UsuarioModel iniciarSesion(InicioModel usuario) throws Exception {
        return null;
    }

    @Override
    public String obtenerClaveUsuario(InicioModel usuario) throws Exception {
        return null;
    }

    @Override
    public ObservableList<RolModel> obtenerRoles() throws Exception {
        return null;
    }

    @Override
    public void activarUsuario(UsuarioModel usuarioModel) throws Exception {

    }

    @Override
    public void desactivarUsuario(UsuarioModel usuarioModel) throws Exception {

    }

    @Override
    public void modificarUsuario(UsuarioModel usuarioModel) throws Exception {

    }

    @Override
    public ObservableList<UsuarioModel> listadoUsuarios() throws Exception {
        return null;
    }

    @Override
    public void resetearClave(UsuarioModel usuarioModel) throws Exception {

    }

    @Override
    public void eliminarUsuario(UsuarioModel usuarioModel) throws Exception {

    }

    @Override
    public void cerrarConexion() {

    }
}
