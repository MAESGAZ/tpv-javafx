package com.proyectotpv.proyectotpv.utiles.servicios.cambioclave;

import com.proyectotpv.proyectotpv.interfaces.CambioClaveInterface;
import com.proyectotpv.proyectotpv.modelos.cambioclave.CambioClaveModel;
import com.proyectotpv.proyectotpv.recursos.Constantes;
import com.proyectotpv.proyectotpv.servicios.ServicioRed;

import java.io.IOException;
import java.net.HttpURLConnection;

public class CambioClaveUtilsWS implements CambioClaveInterface {

    private ServicioRed servicioRed;

    public CambioClaveUtilsWS() throws IOException {
        this.servicioRed = new ServicioRed("prueba");
    }
    @Override
    public int cambiarClave(CambioClaveModel datosCambioClave) throws Exception {
        servicioRed.setProtocoloRed(Constantes.HTTP_POST);
        return 0;
    }

    @Override
    public void cerrarConexion() {
        servicioRed.cerrarConexion();
    }
}
