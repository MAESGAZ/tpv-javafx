package com.proyectotpv.proyectotpv.utiles.basedatos.categoria;

import com.proyectotpv.proyectotpv.basedatos.BaseDatos;
import com.proyectotpv.proyectotpv.interfaces.CategoriaInterface;
import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class CategoriasUtilsBD implements CategoriaInterface {
    private BaseDatos conexion;

    public CategoriasUtilsBD() throws SQLException, ClassNotFoundException {
        this.conexion = new BaseDatos();
    }

    public int nuevaCategoria(CategoriaModel categoria) throws SQLException {
        PreparedStatement pstmt = null;
        int ejecucionQuery = 0;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "INSERT INTO Categoria (nombreCategoria, estadoCategoria, fechaCreacion, idUsuarioCreacion)"
                            + " VALUES (?, ?, ?, ?)");
            pstmt.setString(1, categoria.getNombreCategoria());
            pstmt.setBoolean(2, true);
            pstmt.setString(3, LocalDateTime.now().toString());
            pstmt.setInt(4, categoria.getUsuarioCreacion());

            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return ejecucionQuery;
    }

    public int modificarCategoria(CategoriaModel categoria) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;

        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "UPDATE Categoria SET nombreCategoria = ?, fechaModificacion = ? " + "WHERE idCategoria = ?");
            pstmt.setString(1, categoria.getNombreCategoria());
            pstmt.setString(2, LocalDateTime.now().toString());
            pstmt.setInt(3, categoria.getIdCategoria());

            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null) pstmt.close();
        }
        return ejecucionQuery;
    }

    public int activarCategoria(CategoriaModel categoria) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("UPDATE Categoria SET estadoCategoria = ? WHERE idCategoria = ?");
            pstmt.setBoolean(1, true);
            pstmt.setInt(2, categoria.getIdCategoria());
            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null) pstmt.close();
        }
        return ejecucionQuery;
    }

    public int desactivarCategoria(CategoriaModel categoria) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("UPDATE Categoria SET estadoCategoria = ? WHERE idCategoria = ?");
            pstmt.setBoolean(1, false);
            pstmt.setInt(2, categoria.getIdCategoria());
            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null) pstmt.close();
        }
        return ejecucionQuery;
    }

    public ObservableList<CategoriaModel> listadoCategorias() throws SQLException {
        ObservableList<CategoriaModel> listado = FXCollections.observableArrayList();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection()
                    .prepareStatement("SELECT idCategoria, nombreCategoria, estadoCategoria, fechaCreacion, "
                            + "fechaModificacion, idUsuarioCreacion FROM Categoria");

            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                CategoriaModel categoria = new CategoriaModel();
                categoria.setIdCategoria(res.getInt("idCategoria"));
                categoria.setNombreCategoria(res.getString("nombreCategoria"));
                categoria.setEstadoCategoria(res.getBoolean("estadoCategoria"));
                categoria.setFechaCreacion(LocalDateTime.parse(res.getString("fechaCreacion")));
                String fechaModificacion = res.getString("fechaModificacion");
                if (fechaModificacion != null)
                    categoria.setFechaModificacion(LocalDateTime.parse(fechaModificacion));
                categoria.setUsuarioCreacion(res.getInt("idUsuarioCreacion"));
                listado.add(categoria);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null) pstmt.close();
        }
        return listado;
    }

    public ObservableList<CategoriaModel> listadoCategoriasActivas() throws SQLException {
        ObservableList<CategoriaModel> listado = FXCollections.observableArrayList();
        for (CategoriaModel categoria : this.listadoCategorias()) {
            if (categoria.isEstadoCategoria())
                listado.add(categoria);
        }
        return listado;
    }

    public int eliminarCategoria(CategoriaModel categoria) throws SQLException {
        PreparedStatement pstmt = null;
        this.eliminarProductosCategoria(categoria);
        try {
            pstmt = this.conexion.getConnection().prepareStatement("DELETE FROM Categoria WHERE idCategoria = ?");
            pstmt.setInt(1, categoria.getIdCategoria());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public void eliminarProductosCategoria(CategoriaModel categoria) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("DELETE FROM Producto WHERE idCategoria = ?");
            pstmt.setInt(1, categoria.getIdCategoria());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public void cerrarConexion() {
        try {
            this.conexion.getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
