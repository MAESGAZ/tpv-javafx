package com.proyectotpv.proyectotpv.utiles.basedatos.pedido;

import com.proyectotpv.proyectotpv.basedatos.BaseDatos;
import com.proyectotpv.proyectotpv.interfaces.ProductosLineaInterface;
import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import com.proyectotpv.proyectotpv.modelos.main.producto.ProductoModel;
import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import com.proyectotpv.proyectotpv.modelos.pedido.ProductoLineaModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class ProductosLineaUtilsBD implements ProductosLineaInterface {
    private BaseDatos conexion;

    public ProductosLineaUtilsBD() throws SQLException, ClassNotFoundException {
        this.conexion = new BaseDatos();
    }

    public int agregarProductoLinea(ProductoLineaModel producto) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "INSERT INTO Producto_pedido (idProducto, idPedido, precioModificado, cantidadProducto) "
                            + "VALUES (?, ?, ?, ?)");
            pstmt.setInt(1, producto.getProducto().getIdProducto());
            pstmt.setInt(2, producto.getPedido().getIdPedido());
            pstmt.setDouble(3, producto.getPrecioModificado());
            pstmt.setInt(4, producto.getCantidad());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public ObservableList<ProductoLineaModel> obtenerProductosPedido(PedidoModel pedido) throws SQLException {
        ObservableList<ProductoLineaModel> listadoProductos = FXCollections.observableArrayList();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "SELECT pp.idProducto, pr.nombreProducto, pr.descripcionProducto, pr.cantidad, pr.precioProducto, pr.estadoProducto, "
                            + "pr.fechaCreacion, pr.fechaModificacion, pr.idUsuarioCreacion, pr.idCategoria, pp.precioModificado, pp.cantidadProducto, "
                            + "ca.nombreCategoria, ca.estadoCategoria, ca.fechaCreacion, ca.fechaModificacion, ca.idUsuarioCreacion "
                            + "FROM Producto_pedido pp INNER JOIN Producto pr ON pp.idProducto = pr.idProducto INNER JOIN Categoria ca ON pr.idCategoria = ca.idCategoria "
                            + "WHERE pp.idPedido = ?");
            pstmt.setInt(1, pedido.getIdPedido());
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                ProductoLineaModel proLin = new ProductoLineaModel();
                proLin.setPedido(pedido);
                ProductoModel producto = new ProductoModel();
                producto.setIdProducto(res.getInt("pp.idProducto"));
                producto.setNombreProducto(res.getString("pr.nombreProducto"));
                producto.setDescripcionProducto(res.getString("pr.descripcionProducto"));
                producto.setCantidad(res.getInt("pr.cantidad"));
                producto.setPrecioProducto(res.getDouble("pr.precioProducto"));
                producto.setEstadoProducto(res.getBoolean("pr.estadoProducto"));
                producto.setFechaCreacion(LocalDateTime.parse(res.getString("pr.fechaCreacion")));
                String fechaModificacion = res.getString("pr.fechaModificacion");
                if (fechaModificacion != null)
                    producto.setFechaModificacion(LocalDateTime.parse(fechaModificacion));
                producto.setIdUsuarioCreacion(res.getInt("pr.idUsuarioCreacion"));
                CategoriaModel categoria = new CategoriaModel();
                categoria.setIdCategoria(res.getInt("pr.idCategoria"));
                categoria.setNombreCategoria(res.getString("ca.nombreCategoria"));
                categoria.setEstadoCategoria(res.getBoolean("ca.estadoCategoria"));
                categoria.setFechaCreacion(LocalDateTime.parse(res.getString("ca.fechaCreacion")));
                String fechaModificacionCategoria = res.getString("ca.fechaModificacion");
                if (fechaModificacionCategoria != null)
                    categoria.setFechaModificacion(LocalDateTime.parse(fechaModificacionCategoria));
                categoria.setUsuarioCreacion(res.getInt("ca.idUsuarioCreacion"));
                producto.setCategoria(categoria);
                proLin.setCantidad(res.getInt("pp.cantidadProducto"));
                proLin.setPrecioModificado(res.getDouble("pp.precioModificado"));
                proLin.setProducto(producto);
                listadoProductos.add(proLin);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return listadoProductos;
    }

    public int cambiarCantidadProductoLinea(ProductoLineaModel producto) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "UPDATE producto_pedido SET cantidadProducto = ? WHERE idProducto = ? AND idPedido = ?");
            pstmt.setInt(1, producto.getCantidad());
            pstmt.setInt(2, producto.getProducto().getIdProducto());
            pstmt.setInt(3, producto.getPedido().getIdPedido());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public int cambiarPrecioProductoLinea(ProductoLineaModel producto) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "UPDATE producto_pedido SET precioModificado = ? WHERE idProducto = ? AND idPedido = ?");
            pstmt.setDouble(1, producto.getPrecioModificado());
            pstmt.setInt(2, producto.getProducto().getIdProducto());
            pstmt.setInt(3, producto.getPedido().getIdPedido());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public int eliminarProductoLinea(ProductoLineaModel producto) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection()
                    .prepareStatement("DELETE FROM producto_pedido WHERE idPedido IN (?) AND idProducto = (?)");
            pstmt.setInt(1, producto.getPedido().getIdPedido());
            pstmt.setInt(2, producto.getProducto().getIdProducto());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public void eliminarPedido(PedidoModel producto) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("DELETE FROM Producto_pedido WHERE idPedido IN (?)");
            pstmt.setInt(1, producto.getIdPedido());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public void guardarEnHistorico(ObservableList<ProductoLineaModel> productos) throws SQLException {
        PreparedStatement pstmt = null;
        for (ProductoLineaModel producto : productos) {
            try {
                pstmt = this.conexion.getConnection().prepareStatement("INSERT INTO producto_pedidoHistorico (idProducto, idPedido, precioModificado, cantidadProducto) "
                        + "VALUES (?, ?, ?, ?)");
                pstmt.setInt(1, producto.getProducto().getIdProducto());
                pstmt.setInt(2, producto.getPedido().getIdPedido());
                pstmt.setDouble(3, producto.getPrecioModificado());
                pstmt.setInt(4, producto.getCantidad());
                pstmt.executeUpdate();
            } catch (SQLException e) {
                throw e;
            } finally {
                if (pstmt != null)
                    pstmt.close();
            }
        }
    }

    public void rellenarProducto(ProductoLineaModel producto) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "SELECT prod.nombreProducto, prod.descripcionProducto, prod.cantidad, prod.precioProducto, prod.estadoProducto, "
                            + "prod.fechaCreacion, prod.fechaModificacion, prod.idUsuarioCreacion, prod.idCategoria, cat.nombreCategoria, cat.estadoCategoria, cat.fechaCreacion, "
                            + "cat.fechaModificacion, cat.idUsuarioCreacion FROM producto prod INNER JOIN Categoria cat ON prod.idCategoria = cat.idCategoria WHERE prod.idProducto = ?");
            pstmt.setInt(1, producto.getProducto().getIdProducto());
            ResultSet res = pstmt.executeQuery();
            if (res.next()) {
                producto.getProducto().setNombreProducto(res.getString("prod.nombreProducto"));
                producto.getProducto().setDescripcionProducto(res.getString("prod.descripcionProducto"));
                producto.getProducto().setCantidad(res.getInt("prod.cantidad"));
                producto.getProducto().setPrecioProducto(res.getDouble("prod.precioProducto"));
                producto.getProducto().setEstadoProducto(res.getBoolean("prod.estadoProducto"));
                producto.getProducto().setFechaCreacion(LocalDateTime.parse(res.getString("prod.fechaCreacion")));
                String fechaModificacion = res.getString("prod.fechaModificacion");
                if (fechaModificacion != null)
                    producto.getProducto().setFechaModificacion(LocalDateTime.parse(fechaModificacion));
                producto.getProducto().setIdUsuarioCreacion(res.getInt("prod.idUsuarioCreacion"));
                CategoriaModel categoria = new CategoriaModel();
                categoria.setIdCategoria(res.getInt("prod.idCategoria"));
                categoria.setNombreCategoria(res.getString("cat.nombreCategoria"));
                categoria.setEstadoCategoria(res.getBoolean("cat.estadoCategoria"));
                categoria.setFechaCreacion(LocalDateTime.parse(res.getString("cat.fechaCreacion")));
                String fechaCatModi = res.getString("cat.fechaModificacion");
                if (fechaCatModi != null)
                    categoria.setFechaModificacion(LocalDateTime.parse(fechaCatModi));
                categoria.setUsuarioCreacion(res.getInt("cat.idUsuarioCreacion"));
                producto.getProducto().setCategoria(categoria);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public void cerrarConexion() {
        try {
            this.conexion.getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
