package com.proyectotpv.proyectotpv.utiles.basedatos.cliente;

import com.proyectotpv.proyectotpv.basedatos.BaseDatos;
import com.proyectotpv.proyectotpv.interfaces.ClienteInterface;
import com.proyectotpv.proyectotpv.modelos.pedido.ClienteModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class ClientesUtilsBD implements ClienteInterface {
    private BaseDatos conexion;

    public ClientesUtilsBD() throws SQLException, ClassNotFoundException {
        this.conexion = new BaseDatos();
    }

    public int nuevoCliente(ClienteModel cliente) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "INSERT INTO CLIENTE (numeroTelefono, provincia, ciudad, codigoPostal, direccionCliente, "
                            + "numeroPuertaCliente, letraPuertaCliente, estadoCliente, fechaCreacion, idUsuarioCreacion) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            pstmt.setInt(1, cliente.getNumeroTelefono());
            pstmt.setString(2, cliente.getProvincia());
            pstmt.setString(3, cliente.getCiudad());
            pstmt.setInt(4, cliente.getCodigoPostal());
            pstmt.setString(5, cliente.getDireccionCliente());
            pstmt.setInt(6, cliente.getNumeroPuertaCliente());
            pstmt.setString(7, cliente.getLetraPuertaCliente());
            pstmt.setBoolean(8, true);
            pstmt.setString(9, LocalDateTime.now().toString());
            pstmt.setInt(10, cliente.getIdUsuarioCreacion());
            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return ejecucionQuery;
    }

    public ObservableList<ClienteModel> listadoClientes() throws SQLException {
        ObservableList<ClienteModel> listadoClientes = FXCollections.observableArrayList();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "SELECT idCliente, numeroTelefono, provincia, ciudad, codigoPostal, direccionCliente, "
                            + "numeroPuertaCliente, letraPuertaCliente, estadoCliente, fechaCreacion, fechaModificacion, idUsuarioCreacion FROM CLIENTE WHERE numeroTelefono NOT IN (0)");
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                ClienteModel cliente = new ClienteModel();
                cliente.setIdCliente(res.getInt("idCliente"));
                cliente.setNumeroTelefono(res.getInt("numeroTelefono"));
                cliente.setProvincia(res.getString("provincia"));
                cliente.setCiudad(res.getString("ciudad"));
                cliente.setCodigoPostal(res.getInt("codigoPostal"));
                cliente.setDireccionCliente(res.getString("direccionCliente"));
                cliente.setNumeroPuertaCliente(res.getInt("numeroPuertaCliente"));
                String letraPuertaCliente = res.getString("letraPuertaCliente");
                if (letraPuertaCliente != null)
                    cliente.setLetraPuertaCliente(letraPuertaCliente);
                cliente.setEstadoCliente(res.getBoolean("estadoCliente"));
                cliente.setFechaCreacion(LocalDateTime.parse(res.getString("fechaCreacion")));
                String fechaModificacion = res.getString("fechaModificacion");
                if (fechaModificacion != null)
                    cliente.setFechaModificacion(LocalDateTime.parse(fechaModificacion));
                cliente.setIdUsuarioCreacion(res.getInt("idUsuarioCreacion"));
                listadoClientes.add(cliente);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return listadoClientes;
    }

    public int activarCliente(ClienteModel cliente) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection()
                    .prepareStatement("UPDATE Cliente SET estadoCliente = ? WHERE idCliente = ?");
            pstmt.setBoolean(1, true);
            pstmt.setInt(2, cliente.getIdCliente());
            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return ejecucionQuery;
    }

    public int desactivarCliente(ClienteModel cliente) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection()
                    .prepareStatement("UPDATE Cliente SET estadoCliente = ? WHERE idCliente = ?");
            pstmt.setBoolean(1, false);
            pstmt.setInt(2, cliente.getIdCliente());
            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return ejecucionQuery;
    }

    public int modificarCliente(ClienteModel cliente) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "UPDATE Cliente SET numeroTelefono = ?, provincia = ?, ciudad = ?, codigoPostal = ?, direccionCliente = ?, "
                            + "numeroPuertaCliente = ?, letraPuertaCliente = ?, fechaModificacion = ? WHERE idCliente = ?");
            pstmt.setInt(1, cliente.getNumeroTelefono());
            pstmt.setString(2, cliente.getProvincia());
            pstmt.setString(3, cliente.getCiudad());
            pstmt.setInt(4, cliente.getCodigoPostal());
            pstmt.setString(5, cliente.getDireccionCliente());
            pstmt.setInt(6, cliente.getNumeroPuertaCliente());
            pstmt.setString(7, cliente.getLetraPuertaCliente());
            pstmt.setString(8, LocalDateTime.now().toString());
            pstmt.setInt(9, cliente.getIdCliente());
            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return ejecucionQuery;
    }

    public int eliminarCliente(ClienteModel cliente) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("DELETE FROM Cliente WHERE idCliente = ?");
            pstmt.setInt(1, cliente.getIdCliente());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public void buscarCliente(ClienteModel cliente) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("SELECT idCliente, provincia, ciudad, codigoPostal, direccionCliente, "
                    + "numeroPuertaCliente, letraPuertaCliente, estadoCliente, fechaCreacion, fechaModificacion, idUsuarioCreacion FROM CLIENTE "
                    + "WHERE numeroTelefono = ?");
            pstmt.setInt(1, cliente.getNumeroTelefono());
            ResultSet res = pstmt.executeQuery();
            if (res.next()) {
                cliente.setIdCliente(res.getInt("idCliente"));
                cliente.setProvincia(res.getString("provincia"));
                cliente.setCiudad(res.getString("ciudad"));
                cliente.setCodigoPostal(res.getInt("codigoPostal"));
                cliente.setDireccionCliente(res.getString("direccionCliente"));
                cliente.setNumeroPuertaCliente(res.getInt("numeroPuertaCliente"));
                String letraPuerta = res.getString("letraPuertaCliente");
                if (letraPuerta != null)
                    cliente.setLetraPuertaCliente(res.getString("letraPuertaCliente"));
                cliente.setEstadoCliente(res.getBoolean("estadoCliente"));
                cliente.setFechaCreacion(LocalDateTime.parse(res.getString("fechaCreacion")));
                String fechaModificacion = res.getString("fechaModificacion");
                if (fechaModificacion != null)
                    cliente.setFechaModificacion(LocalDateTime.parse(fechaModificacion));
                cliente.setIdUsuarioCreacion(res.getInt("idUsuarioCreacion"));
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    public void cerrarConexion() {
        try {
            this.conexion.getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
