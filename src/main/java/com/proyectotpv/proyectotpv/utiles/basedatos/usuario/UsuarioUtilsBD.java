package com.proyectotpv.proyectotpv.utiles.basedatos.usuario;

import com.proyectotpv.proyectotpv.basedatos.BaseDatos;
import com.proyectotpv.proyectotpv.interfaces.UsuarioInterface;
import com.proyectotpv.proyectotpv.modelos.logininicio.InicioModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.RolModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.recursos.CifradorClave;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class UsuarioUtilsBD implements UsuarioInterface {
    private BaseDatos conexion;
    private CifradorClave cifrador;

    public UsuarioUtilsBD() throws SQLException, ClassNotFoundException {
        this.conexion = new BaseDatos();
        this.cifrador = new CifradorClave();
    }


    @Override
    public void nuevoUsuario(UsuarioModel usuarioModel) throws SQLException, NoSuchAlgorithmException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection()
                    .prepareStatement("INSERT INTO USUARIO (usuario, nombreUsuario, apellidosUsuario, estadoUsuario,"
                            + "correoElectronico, claveUsuario, cambioClave, fechaCreacion, fechaCambioClave, idRol) VALUES (?, ?, ?, ?, ?, ? ,? ,? ,?, ?)");
            pstmt.setString(1, usuarioModel.getUsuario().getValueSafe());
            pstmt.setString(2, usuarioModel.getNombreUsuario().getValueSafe());
            pstmt.setString(3, usuarioModel.getApellidosUsuario().getValueSafe());
            pstmt.setBoolean(4, false);
            pstmt.setString(5, usuarioModel.getCorreoElectronico().getValueSafe());
            pstmt.setString(6, cifrador.generarSSHA("prueba"));
            pstmt.setBoolean(7, true);
            pstmt.setString(8, LocalDateTime.now().toString());
            pstmt.setString(9, LocalDateTime.now().plusDays(30).toString());
            pstmt.setInt(10, usuarioModel.getRolUsuario().getValue().getIdRol().get());

            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } catch (NoSuchAlgorithmException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();

        }
    }

    @Override
    public UsuarioModel iniciarSesion(InicioModel usuario) throws SQLException {
        PreparedStatement pstmt = null;
        UsuarioModel usuarioSesion = new UsuarioModel();
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "SELECT us.id_Usuario, us.usuario, us.nombreUsuario, us.apellidosUsuario, us.estadoUsuario, us.correoElectronico,"
                            + " us.claveUsuario, us.cambioClave, us.fechaCreacion, us.fechaCambioClave, us.idRol, ro.nombreRol FROM usuario us INNER JOIN Rol ro ON us.idRol = ro.idRol"
                            + " where usuario = ?");
            pstmt.setString(1, usuario.getUsuario().getValueSafe());

            ResultSet res = pstmt.executeQuery();
            if (res.next()) {
                usuarioSesion.setIdUsuario(new SimpleIntegerProperty(res.getInt("us.id_Usuario")));
                usuarioSesion.setUsuario(new SimpleStringProperty(res.getString("us.usuario")));
                usuarioSesion.setNombreUsuario(new SimpleStringProperty(res.getString("us.nombreUsuario")));
                usuarioSesion.setApellidosUsuario(new SimpleStringProperty(res.getString("us.apellidosUsuario")));
                usuarioSesion.setEstadoUsuario(new SimpleBooleanProperty(res.getBoolean("us.estadoUsuario")));
                usuarioSesion.setClaveUsuario(new SimpleStringProperty(res.getString("us.claveUsuario")));
                usuarioSesion.setCorreoElectronico(new SimpleStringProperty(res.getString("us.correoElectronico")));
                usuarioSesion.setCambioClave(new SimpleBooleanProperty(res.getBoolean("us.cambioClave")));
                usuarioSesion.setFechaCreacion(new SimpleObjectProperty<LocalDateTime>(
                        LocalDateTime.parse(res.getString("us.fechaCreacion"))));
                usuarioSesion.setFechaCambioClave(new SimpleObjectProperty<LocalDateTime>(
                        LocalDateTime.parse(res.getString("us.fechaCambioClave"))));
                usuarioSesion.setRolUsuario(new SimpleObjectProperty<RolModel>(new RolModel(res.getInt("us.idRol"), res.getString("ro.nombreRol"))));

            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return usuarioSesion;
    }

    @Override
    public String obtenerClaveUsuario(InicioModel usuario) throws SQLException {
        PreparedStatement pstmt = null;
        String clave = "";
        try {
            pstmt = this.conexion.getConnection().prepareStatement("SELECT claveUsuario FROM usuario WHERE usuario = ?");
            pstmt.setString(1, usuario.getUsuario().getValueSafe());
            ResultSet res = pstmt.executeQuery();
            if (res.next()) {
                clave = res.getString("claveUsuario");
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            throw e;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }

        return clave;
    }

    @Override
    public ObservableList<RolModel> obtenerRoles() throws SQLException {
        ObservableList<RolModel> listadoRoles = FXCollections.observableArrayList();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("SELECT idRol, nombreRol FROM ROL");
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                listadoRoles.add(new RolModel(res.getInt("idRol"), res.getString("nombreRol")));
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return listadoRoles;
    }

    @Override
    public void activarUsuario(UsuarioModel usuarioModel) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection()
                    .prepareStatement("UPDATE Usuario SET estadoUsuario = ? WHERE id_Usuario = ?");
            pstmt.setBoolean(1, true);
            pstmt.setInt(2, usuarioModel.getIdUsuario().getValue());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    @Override
    public void desactivarUsuario(UsuarioModel usuarioModel) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection()
                    .prepareStatement("UPDATE Usuario SET estadoUsuario = ? WHERE id_Usuario = ?");
            pstmt.setBoolean(1, false);
            pstmt.setInt(2, usuarioModel.getIdUsuario().getValue());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    @Override
    public void modificarUsuario(UsuarioModel usuarioModel) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection()
                    .prepareStatement("UPDATE Usuario SET "
                            + "usuario = ?, nombreUsuario = ?, apellidosUsuario = ?, correoElectronico = ?, idRol = ? "
                            + "WHERE id_Usuario = ?");
            pstmt.setString(1, usuarioModel.getUsuario().getValueSafe());
            pstmt.setString(2, usuarioModel.getNombreUsuario().getValueSafe());
            pstmt.setString(3, usuarioModel.getApellidosUsuario().getValueSafe());
            pstmt.setString(4, usuarioModel.getCorreoElectronico().getValueSafe());
            pstmt.setInt(5, usuarioModel.getRolUsuario().getValue().getIdRol().getValue());
            pstmt.setInt(6, usuarioModel.getIdUsuario().getValue());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    @Override
    public ObservableList<UsuarioModel> listadoUsuarios() throws SQLException {
        ObservableList<UsuarioModel> listado = FXCollections.observableArrayList();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("SELECT us.id_usuario, us.usuario, "
                    + "us.nombreUsuario, us.apellidosUsuario, us.estadoUsuario, us.correoElectronico, us.claveUsuario, us.cambioClave, us.fechaCreacion, "
                    + "us.fechaCambioClave, us.idRol, ro.nombreRol FROM Usuario us INNER JOIN Rol ro ON us.idRol = ro.idRol");
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                UsuarioModel usuario = new UsuarioModel();
                usuario.setIdUsuario(new SimpleIntegerProperty(res.getInt("us.id_usuario")));
                usuario.setUsuario(new SimpleStringProperty(res.getString("us.usuario")));
                usuario.setNombreUsuario(new SimpleStringProperty(res.getString("us.nombreUsuario")));
                usuario.setApellidosUsuario(new SimpleStringProperty(res.getString("us.apellidosUsuario")));
                usuario.setEstadoUsuario(new SimpleBooleanProperty(res.getBoolean("us.estadoUsuario")));
                usuario.setCorreoElectronico(new SimpleStringProperty(res.getString("us.correoElectronico")));
                usuario.setClaveUsuario(new SimpleStringProperty(res.getString("us.claveUsuario")));
                usuario.setCambioClave(new SimpleBooleanProperty(res.getBoolean("us.cambioClave")));
                usuario.setFechaCreacion(new SimpleObjectProperty<LocalDateTime>(
                        LocalDateTime.parse(res.getString("us.fechaCreacion"))));
                usuario.setFechaCambioClave(new SimpleObjectProperty<LocalDateTime>(
                        LocalDateTime.parse(res.getString("us.fechaCambioClave"))));
                usuario.setRolUsuario(new SimpleObjectProperty<RolModel>(
                        new RolModel(res.getInt("idRol"), res.getString("ro.nombreRol"))));
                listado.add(usuario);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return listado;
    }

    @Override
    public void resetearClave(UsuarioModel usuarioModel) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection()
                    .prepareStatement("UPDATE Usuario SET cambioClave = ? " + "WHERE id_usuario = ?");
            pstmt.setBoolean(1, true);
            pstmt.setInt(2, usuarioModel.getIdUsuario().getValue());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    @Override
    public void eliminarUsuario(UsuarioModel usuarioModel) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("DELETE FROM Usuario WHERE id_usuario = ?");
            pstmt.setInt(1, usuarioModel.getIdUsuario().getValue());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    @Override
    public void cerrarConexion() {
        try {
            this.conexion.getConnection().close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
