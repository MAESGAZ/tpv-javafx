package com.proyectotpv.proyectotpv.utiles.basedatos.producto;

import com.proyectotpv.proyectotpv.basedatos.BaseDatos;
import com.proyectotpv.proyectotpv.interfaces.ProductoInterface;
import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import com.proyectotpv.proyectotpv.modelos.main.producto.ProductoModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class ProductoUtilsBD implements ProductoInterface {
    private BaseDatos conexion;

    public ProductoUtilsBD() throws SQLException, ClassNotFoundException {
        this.conexion = new BaseDatos();
    }

    public ObservableList<ProductoModel> listadoProductos() throws SQLException {
        ObservableList<ProductoModel> listado = FXCollections.observableArrayList();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("SELECT po.idProducto, po.nombreProducto, po.descripcionProducto, po.cantidad,"
                    + " po.precioProducto, po.estadoProducto, po.fechaCreacion, po.fechaModificacion, po.idUsuarioCreacion, po.idCategoria, ca.nombreCategoria, ca.estadoCategoria, ca.fechaCreacion, "
                    + "	ca.fechaModificacion, ca.idUsuarioCreacion FROM Producto po INNER JOIN Categoria ca ON po.idCategoria = ca.idCategoria");
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                ProductoModel producto = new ProductoModel();
                producto.setIdProducto(res.getInt("po.idProducto"));
                producto.setNombreProducto(res.getString("po.nombreProducto"));
                producto.setDescripcionProducto(res.getString("po.descripcionProducto"));
                producto.setCantidad(res.getInt("po.cantidad"));
                producto.setPrecioProducto(res.getDouble("po.precioProducto"));
                producto.setEstadoProducto(res.getBoolean("po.estadoProducto"));
                producto.setFechaCreacion(LocalDateTime.parse(res.getString("po.fechaCreacion")));
                String fechaModificacion = res.getString("po.fechaModificacion");
                if (fechaModificacion != null)
                    producto.setFechaModificacion(LocalDateTime.parse(fechaModificacion));
                producto.setIdUsuarioCreacion(res.getInt("po.idUsuarioCreacion"));
                CategoriaModel categoria = new CategoriaModel();
                categoria.setIdCategoria(res.getInt("po.idCategoria"));
                categoria.setNombreCategoria(res.getString("ca.nombreCategoria"));
                categoria.setEstadoCategoria(res.getBoolean("ca.estadoCategoria"));
                categoria.setFechaCreacion(LocalDateTime.parse(res.getString("ca.fechaCreacion")));
                String fechaModiCate = res.getString("ca.fechaModificacion");
                if (fechaModiCate != null)
                    categoria.setFechaModificacion(LocalDateTime.parse(fechaModiCate));
                categoria.setUsuarioCreacion(res.getInt("ca.idUsuarioCreacion"));
                producto.setCategoria(categoria);
                listado.add(producto);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return listado;
    }

    public ObservableList<ProductoModel> listadoProductosPorCategoria(CategoriaModel categoria) throws SQLException {
        ObservableList<ProductoModel> listadoProductos = FXCollections.observableArrayList();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("SELECT idProducto, nombreProducto, descripcionProducto, cantidad,"
                    + " precioProducto, estadoProducto, fechaCreacion, fechaModificacion, idUsuarioCreacion FROM Producto WHERE idCategoria = ? AND estadoProducto = TRUE");
            pstmt.setInt(1, categoria.getIdCategoria());
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                ProductoModel producto = new ProductoModel();
                producto.setIdProducto(res.getInt("idProducto"));
                producto.setNombreProducto(res.getString("nombreProducto"));
                producto.setDescripcionProducto(res.getString("descripcionProducto"));
                producto.setCantidad(res.getInt("cantidad"));
                producto.setPrecioProducto(res.getDouble("precioProducto"));
                producto.setEstadoProducto(res.getBoolean("estadoProducto"));
                producto.setFechaCreacion(LocalDateTime.parse(res.getString("fechaCreacion")));
                String fechaModificacion = res.getString("fechaModificacion");
                if (fechaModificacion != null)
                    producto.setFechaModificacion(LocalDateTime.parse(fechaModificacion));
                producto.setIdUsuarioCreacion(res.getInt("idUsuarioCreacion"));
                producto.setCategoria(categoria);
                listadoProductos.add(producto);
            }

        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
        }
        return listadoProductos;
    }

    public ObservableList<ProductoModel> listadoProductosActivos() throws SQLException {
        ObservableList<ProductoModel> listado = FXCollections.observableArrayList();
        for (ProductoModel producto : this.listadoProductos()) {
            if (producto.isEstadoProducto()) {
                if (producto.getCategoria().isEstadoCategoria())
                    listado.add(producto);
            }
        }
        return listado;
    }

    public int nuevoProducto(ProductoModel producto) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("INSERT INTO Producto (nombreProducto, descripcionProducto, cantidad, "
                    + "precioProducto, estadoProducto, fechaCreacion, idUsuarioCreacion, idCategoria) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            pstmt.setString(1, producto.getNombreProducto());
            pstmt.setString(2, producto.getDescripcionProducto());
            pstmt.setInt(3, producto.getCantidad());
            pstmt.setDouble(4, producto.getPrecioProducto());
            pstmt.setBoolean(5, true);
            pstmt.setString(6, LocalDateTime.now().toString());
            pstmt.setInt(7, producto.getIdUsuarioCreacion());
            pstmt.setInt(8, producto.getCategoria().getIdCategoria());
            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return ejecucionQuery;
    }

    public int activarProducto(ProductoModel producto) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("UPDATE Producto SET estadoProducto = ? WHERE idProducto = ?");
            pstmt.setBoolean(1, true);
            pstmt.setInt(2, producto.getIdProducto());
            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return ejecucionQuery;
    }

    public int desactivarProducto(ProductoModel producto) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("UPDATE Producto SET estadoProducto = ? WHERE idProducto = ?");
            pstmt.setBoolean(1, false);
            pstmt.setInt(2, producto.getIdProducto());
            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return ejecucionQuery;
    }

    public int modificarProducto(ProductoModel producto) throws SQLException {
        int ejecucionQuery = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("UPDATE Producto SET nombreProducto = ?, descripcionProducto = ?, "
                    + "cantidad = ?, precioProducto = ?, fechaModificacion = ?, idCategoria = ? WHERE idProducto = ?");
            pstmt.setString(1, producto.getNombreProducto());
            pstmt.setString(2, producto.getDescripcionProducto());
            pstmt.setInt(3, producto.getCantidad());
            pstmt.setDouble(4, producto.getPrecioProducto());
            pstmt.setString(5, LocalDateTime.now().toString());
            pstmt.setInt(6, producto.getCategoria().getIdCategoria());
            pstmt.setInt(7, producto.getIdProducto());
            ejecucionQuery = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return ejecucionQuery;
    }

    public int eliminarProducto(ProductoModel producto) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("DELETE FROM Producto WHERE idProducto = ?");
            pstmt.setInt(1, producto.getIdProducto());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public void cerrarConexion() {
        try {
            this.conexion.getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
