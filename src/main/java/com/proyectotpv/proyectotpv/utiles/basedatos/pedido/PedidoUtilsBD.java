package com.proyectotpv.proyectotpv.utiles.basedatos.pedido;

import com.proyectotpv.proyectotpv.basedatos.BaseDatos;
import com.proyectotpv.proyectotpv.interfaces.PedidoInterface;
import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import com.proyectotpv.proyectotpv.recursos.Constantes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class PedidoUtilsBD implements PedidoInterface {
    private BaseDatos conexion;

    public PedidoUtilsBD() throws SQLException, ClassNotFoundException {
        this.conexion = new BaseDatos();
    }

    public int crearPedido(PedidoModel pedido) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection()
                    .prepareStatement("INSERT INTO Pedido (sitioPedido, numeroSitio, pedidoTerminado, subestados, "
                            + "fechaCreacion, idUsuarioCreacion, idCliente) VALUES (?, ?, ?, ?, ?, ?, ?)");
            pstmt.setString(1, pedido.getSitioPedido());
            pstmt.setInt(2, pedido.getNumeroSitio());
            pstmt.setBoolean(3, false);
            pstmt.setString(4, Constantes.ESTADO_INICIADO);
            pstmt.setString(5, LocalDateTime.now().toString());
            pstmt.setInt(6, pedido.getIdUsuario());
            pstmt.setInt(7, pedido.getIdCliente());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public ObservableList<PedidoModel> listadoPedidos() throws SQLException {
        ObservableList<PedidoModel> listadoPedidos = FXCollections.observableArrayList();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "SELECT idPedido, sitioPedido, numeroSitio, subestados, fechaCreacion, fechaModificacion, "
                            + "idUsuarioCreacion, idCliente FROM Pedido WHERE pedidoTerminado = FALSE AND sitioPedido NOT IN ('Pedido Domicilio')");
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                PedidoModel pedido = new PedidoModel();
                pedido.setIdPedido(res.getInt("idPedido"));
                pedido.setSitioPedido(res.getString("sitioPedido"));
                pedido.setNumeroSitio(res.getInt("numeroSitio"));
                pedido.setPedidoTerminado(false);
                pedido.setSubestados(res.getString("subestados"));
                pedido.setFechaCreacion(
                        LocalDateTime.parse(res.getString("fechaCreacion")));
                String fechaModificacion = res.getString("fechaModificacion");
                if (fechaModificacion != null)
                    pedido.setFechaModificacion(
                            LocalDateTime.parse(fechaModificacion));
                pedido.setIdUsuario(res.getInt("idUsuarioCreacion"));
                pedido.setIdCliente(res.getInt("idCliente"));
                listadoPedidos.add(pedido);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return listadoPedidos;
    }

    public void obtenerIdPedido(PedidoModel pedido) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement(
                    "SELECT idPedido WHERE pedidoTerminado = FALSE AND sitioPedido = ? AND numeroSitio = ?");
            pstmt.setString(1, pedido.getSitioPedido());
            pstmt.setInt(2, pedido.getNumeroSitio());
            ResultSet res = pstmt.executeQuery();
            if (res.next()) {
                pedido.setIdPedido(res.getInt("idPedido"));
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public PedidoModel obtenerPedido(PedidoModel pedidoModel) throws SQLException {
        PedidoModel pedido = new PedidoModel();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("SELECT idPedido, sitioPedido, numeroSitio, pedidoTerminado, subestados, "
                    + "fechaCreacion, fechaModificacion, idUsuarioCreacion, idCliente FROM Pedido WHERE sitioPedido = ? AND numeroSitio = ? AND pedidoTerminado = FALSE");
            pstmt.setString(1, pedidoModel.getSitioPedido());
            pstmt.setInt(2, pedidoModel.getNumeroSitio());
            ResultSet res = pstmt.executeQuery();
            if (res.next()) {
                pedido.setIdPedido(res.getInt("idPedido"));
                pedido.setSitioPedido(res.getString("sitioPedido"));
                pedido.setNumeroSitio(res.getInt("numeroSitio"));
                pedido.setPedidoTerminado(res.getBoolean("pedidoTerminado"));
                pedido.setSubestados(res.getString("subestados"));
                pedido.setFechaCreacion(LocalDateTime.parse(res.getString("fechaCreacion")));
                String fechaModificacion = res.getString("fechaModificacion");
                if (fechaModificacion != null)
                    pedido.setFechaModificacion(LocalDateTime.parse(fechaModificacion));
                pedido.setIdUsuario(res.getInt("idUsuarioCreacion"));
                pedido.setIdCliente(res.getInt("idCliente"));
            }
            return pedido;
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public int modificarPedido(PedidoModel pedido) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("UPDATE Pedido SET pedidoTerminado = ?, subestados = ?, fechaModificacion = ? WHERE idPedido = ?");
            pstmt.setBoolean(1, pedido.isPedidoTerminado());
            pstmt.setString(2, pedido.getSubestados());
            pstmt.setString(3, LocalDateTime.now().toString());
            pstmt.setInt(4, pedido.getIdPedido());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public void cerrarConexion() {
        try {
            this.conexion.getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
