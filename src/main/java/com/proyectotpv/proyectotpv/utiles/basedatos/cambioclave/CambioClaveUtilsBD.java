package com.proyectotpv.proyectotpv.utiles.basedatos.cambioclave;

import com.proyectotpv.proyectotpv.basedatos.BaseDatos;
import com.proyectotpv.proyectotpv.interfaces.CambioClaveInterface;
import com.proyectotpv.proyectotpv.modelos.cambioclave.CambioClaveModel;
import com.proyectotpv.proyectotpv.recursos.CifradorClave;

import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class CambioClaveUtilsBD implements CambioClaveInterface {
    private BaseDatos conexion;
    private CifradorClave cifrador;

    public CambioClaveUtilsBD() throws SQLException, ClassNotFoundException {
        this.conexion = new BaseDatos();
        this.cifrador = new CifradorClave();
    }

    public int cambiarClave(CambioClaveModel datosCambioClave) throws SQLException, NoSuchAlgorithmException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("UPDATE USUARIO SET claveUsuario = ?, fechaCambioClave = ?, cambioClave = ? WHERE id_usuario = ?");
            pstmt.setString(1, cifrador.generarSSHA(datosCambioClave.getClaveNueva()));
            pstmt.setString(2, LocalDateTime.now().plusDays(30).toString());
            pstmt.setBoolean(3, false);
            pstmt.setInt(4, datosCambioClave.getIdUsuario());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } catch (NoSuchAlgorithmException e) {
            throw e;
        } finally {
            if (pstmt != null) pstmt.close();
        }
    }

    public void cerrarConexion() {
        try {
            this.conexion.getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
