package com.proyectotpv.proyectotpv.utiles.basedatos.pedido;

import com.proyectotpv.proyectotpv.basedatos.BaseDatos;
import com.proyectotpv.proyectotpv.interfaces.PedidosDomicilioInterface;
import com.proyectotpv.proyectotpv.modelos.pedido.ClienteModel;
import com.proyectotpv.proyectotpv.modelos.pedido.PedidoDomicilioModel;
import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import com.proyectotpv.proyectotpv.recursos.Constantes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class PedidosDomicilioUtilsBD implements PedidosDomicilioInterface {
    private BaseDatos conexion;

    public PedidosDomicilioUtilsBD() throws SQLException, ClassNotFoundException {
        this.conexion = new BaseDatos();
    }

    public ObservableList<PedidoDomicilioModel> listadoPedidosDomicilio() throws SQLException {
        ObservableList<PedidoDomicilioModel> listadoPedidos = FXCollections.observableArrayList();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("SELECT pe.idPedido, pe.sitioPedido, pe.numeroSitio, pe.pedidoTerminado, pe.subestados, "
                    + "pe.fechaCreacion, pe.fechaModificacion, pe.idUsuarioCreacion, pe.idCliente, cl.numeroTelefono, cl.provincia, cl.ciudad, cl.codigoPostal, cl.direccionCliente, "
                    + "cl.numeroPuertaCliente, cl.letraPuertaCliente, cl.estadoCliente, cl.fechaCreacion, cl.fechaModificacion, cl.idUsuarioCreacion "
                    + "FROM pedido pe INNER JOIN Cliente cl ON pe.idCliente = cl.idCliente WHERE pe.pedidoTerminado = FALSE AND pe.sitioPedido = 'Pedido Domicilio'");
            ResultSet res = pstmt.executeQuery();
            while (res.next()) {
                PedidoModel pedido = new PedidoModel();
                pedido.setIdPedido(res.getInt("pe.idPedido"));
                pedido.setSitioPedido(res.getString("pe.sitioPedido"));
                pedido.setNumeroSitio(res.getInt("pe.numeroSitio"));
                pedido.setPedidoTerminado(res.getBoolean("pe.pedidoTerminado"));
                pedido.setSubestados(res.getString("pe.subestados"));
                pedido.setFechaCreacion(LocalDateTime.parse(res.getString("pe.fechaCreacion")));
                String fechaModificacionPedido = res.getString("pe.fechaModificacion");
                if (fechaModificacionPedido != null)
                    pedido.setFechaModificacion(LocalDateTime.parse(fechaModificacionPedido));
                pedido.setIdUsuario(res.getInt("pe.idUsuarioCreacion"));
                int idCliente = res.getInt("pe.idCliente");
                pedido.setIdCliente(idCliente);

                ClienteModel cliente = new ClienteModel();
                cliente.setIdCliente(idCliente);
                cliente.setNumeroTelefono(res.getInt("cl.numeroTelefono"));
                cliente.setProvincia(res.getString("cl.provincia"));
                cliente.setCiudad(res.getString("cl.ciudad"));
                int cdPostal = res.getInt("cl.codigoPostal");
                cliente.setCodigoPostal(cdPostal);
                cliente.setDireccionCliente(res.getString("cl.direccionCliente"));
                cliente.setNumeroPuertaCliente(res.getInt("cl.numeroPuertaCliente"));
                String letraPuertaCliente = res.getString("cl.letraPuertaCliente");
                if (letraPuertaCliente != null)
                    cliente.setLetraPuertaCliente(letraPuertaCliente);
                cliente.setEstadoCliente(res.getBoolean("cl.estadoCliente"));
                cliente.setFechaCreacion(LocalDateTime.parse(res.getString("cl.fechaCreacion")));
                String fechaModificacionCliente = res.getString("cl.fechaModificacion");
                if (fechaModificacionCliente != null)
                    cliente.setFechaModificacion(LocalDateTime.parse(fechaModificacionCliente));
                cliente.setIdUsuarioCreacion(res.getInt("cl.idUsuarioCreacion"));

                PedidoDomicilioModel pedidoDomicilio = new PedidoDomicilioModel(pedido, cliente);
                listadoPedidos.add(pedidoDomicilio);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
        return listadoPedidos;
    }

    public int crearPedidoDomicilio(PedidoDomicilioModel pedido) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("INSERT INTO Pedido (sitioPedido, numeroSitio, pedidoTerminado, subestados, "
                    + "fechaCreacion, idUsuarioCreacion, idCliente) VALUES (?, ?, ?, ?, ?, ?, ?)");
            pstmt.setString(1, pedido.getPedido().getSitioPedido());
            pstmt.setInt(2, pedido.getPedido().getNumeroSitio());
            pstmt.setBoolean(3, false);
            pstmt.setString(4, "inicio");
            pstmt.setString(5, LocalDateTime.now().toString());
            pstmt.setInt(6, pedido.getPedido().getIdUsuario());
            pstmt.setInt(7, pedido.getClientePedido().getIdCliente());
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt != null)
                pstmt.close();
        }
    }

    public PedidoDomicilioModel obtenerPedidoPorCliente(ClienteModel clientePedido) throws SQLException {
        PedidoDomicilioModel pediDomi = new PedidoDomicilioModel();
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("SELECT pe.idPedido, pe.sitioPedido, pe.numeroSitio, pe.pedidoTerminado, pe.subestados, "
                    + "pe.fechaCreacion, pe.fechaModificacion, pe.idUsuarioCreacion, pe.idCliente, cl.numeroTelefono, cl.provincia, cl.ciudad, cl.codigoPostal, cl.direccionCliente, "
                    + "cl.numeroPuertaCliente, cl.letraPuertaCliente, cl.estadoCliente, cl.fechaCreacion, cl.fechaModificacion, cl.idUsuarioCreacion "
                    + "FROM pedido pe INNER JOIN Cliente cl ON pe.idCliente = cl.idCliente WHERE pe.pedidoTerminado = FALSE AND pe.sitioPedido = 'Pedido Domicilio' AND pe.idCliente = ?");
            pstmt.setInt(1, clientePedido.getIdCliente());
            ResultSet res = pstmt.executeQuery();
            if (res.next()) {
                PedidoModel pedido = new PedidoModel();
                pedido.setIdPedido(res.getInt("pe.idPedido"));
                pedido.setSitioPedido(res.getString("pe.sitioPedido"));
                pedido.setNumeroSitio(res.getInt("pe.numeroSitio"));
                pedido.setPedidoTerminado(res.getBoolean("pe.pedidoTerminado"));
                pedido.setSubestados(res.getString("pe.subestados"));
                pedido.setFechaCreacion(LocalDateTime.parse(res.getString("pe.fechaCreacion")));
                String fechaModificacionPedido = res.getString("pe.fechaModificacion");
                if (fechaModificacionPedido != null)
                    pedido.setFechaModificacion(LocalDateTime.parse(fechaModificacionPedido));
                pedido.setIdUsuario(res.getInt("pe.idUsuarioCreacion"));
                int idCliente = res.getInt("pe.idCliente");
                pedido.setIdCliente(idCliente);

                ClienteModel cliente = new ClienteModel();
                cliente.setIdCliente(idCliente);
                cliente.setNumeroTelefono(res.getInt("cl.numeroTelefono"));
                cliente.setProvincia(res.getString("cl.provincia"));
                cliente.setCiudad(res.getString("cl.ciudad"));
                int cdPostal = res.getInt("cl.codigoPostal");
                cliente.setCodigoPostal(cdPostal);
                cliente.setDireccionCliente(res.getString("cl.direccionCliente"));
                cliente.setNumeroPuertaCliente(res.getInt("cl.numeroPuertaCliente"));
                String letraPuertaCliente = res.getString("cl.letraPuertaCliente");
                if (letraPuertaCliente != null)
                    cliente.setLetraPuertaCliente(letraPuertaCliente);
                cliente.setEstadoCliente(res.getBoolean("cl.estadoCliente"));
                cliente.setFechaCreacion(LocalDateTime.parse(res.getString("cl.fechaCreacion")));
                String fechaModificacionCliente = res.getString("cl.fechaModificacion");
                if (fechaModificacionCliente != null)
                    cliente.setFechaModificacion(LocalDateTime.parse(fechaModificacionCliente));
                cliente.setIdUsuarioCreacion(res.getInt("cl.idUsuarioCreacion"));

                pediDomi = new PedidoDomicilioModel(pedido, cliente);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pstmt!= null) {
                pstmt.close();
            }
        }
        return pediDomi;
    }

    public boolean comprobarClientePedidoActivos(ClienteModel cliente) throws SQLException {
        boolean pedidoActivo = false;
        PreparedStatement pstmt = null;
        try {
            pstmt = this.conexion.getConnection().prepareStatement("SELECT idPedido FROM Pedido WHERE pedidoTerminado = FALSE AND idCliente = ? AND sitioPedido = ?");
            pstmt.setInt(1, cliente.getIdCliente());
            pstmt.setString(2, Constantes.DOMICILIO_PEDIDO);
            ResultSet res = pstmt.executeQuery();
            pedidoActivo = res.next();
        } catch (SQLException e) {
            throw e;
        }
        return pedidoActivo;
    }

    public void cerrarConexion() {
        try {
            this.conexion.getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
