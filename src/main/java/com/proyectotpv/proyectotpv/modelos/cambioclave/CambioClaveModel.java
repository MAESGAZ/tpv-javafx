package com.proyectotpv.proyectotpv.modelos.cambioclave;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CambioClaveModel implements Cloneable {
    private IntegerProperty idUsuario;
    private StringProperty claveNueva;
    private StringProperty claveNuevaRepetida;

    public CambioClaveModel() {
        this.idUsuario = new SimpleIntegerProperty();
        this.claveNueva = new SimpleStringProperty();
        this.claveNuevaRepetida = new SimpleStringProperty();
    }

    public CambioClaveModel(int idUsuario, String claveNueva, String claveNuevaRepetida) {
        this.idUsuario = new SimpleIntegerProperty(idUsuario);
        this.claveNueva = new SimpleStringProperty(claveNueva);
        this.claveNuevaRepetida = new SimpleStringProperty(claveNuevaRepetida);
    }

    public int getIdUsuario() {
        return idUsuario.get();
    }

    public IntegerProperty idUsuarioProperty() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario.set(idUsuario);
    }

    public String getClaveNueva() {
        return claveNueva.get();
    }

    public StringProperty claveNuevaProperty() {
        return claveNueva;
    }

    public void setClaveNueva(String claveNueva) {
        this.claveNueva.set(claveNueva);
    }

    public String getClaveNuevaRepetida() {
        return claveNuevaRepetida.get();
    }

    public StringProperty claveNuevaRepetidaProperty() {
        return claveNuevaRepetida;
    }

    public void setClaveNuevaRepetida(String claveNuevaRepetida) {
        this.claveNuevaRepetida.set(claveNuevaRepetida);
    }
}
