package com.proyectotpv.proyectotpv.modelos.pedido;

import javafx.beans.property.*;

import java.time.LocalDateTime;

public class ClienteModel {
    private IntegerProperty idCliente;
    private IntegerProperty numeroTelefono;
    private StringProperty provincia;
    private StringProperty ciudad;
    private IntegerProperty codigoPostal;
    private StringProperty direccionCliente;
    private IntegerProperty numeroPuertaCliente;
    private StringProperty letraPuertaCliente;
    private BooleanProperty estadoCliente;
    private ObjectProperty<LocalDateTime> fechaCreacion;
    private ObjectProperty<LocalDateTime> fechaModificacion;
    private IntegerProperty idUsuarioCreacion;

    public ClienteModel() {
        this.idCliente = new SimpleIntegerProperty();
        this.numeroTelefono = new SimpleIntegerProperty();
        this.provincia = new SimpleStringProperty();
        this.ciudad = new SimpleStringProperty();
        this.codigoPostal = new SimpleIntegerProperty();
        this.direccionCliente = new SimpleStringProperty();
        this.numeroPuertaCliente = new SimpleIntegerProperty();
        this.letraPuertaCliente = new SimpleStringProperty();
        this.estadoCliente = new SimpleBooleanProperty();
        this.fechaCreacion = new SimpleObjectProperty<LocalDateTime>();
        this.fechaModificacion = new SimpleObjectProperty<LocalDateTime>();
        this.idUsuarioCreacion = new SimpleIntegerProperty();
    }

    public ClienteModel(int idCliente, int numeroTelefono, String provincia, String ciudad, int codigoPostal, String direccionCliente,
                        int numeroPuertaCliente, String letraPuertaCliente, boolean estadoCliente, LocalDateTime fechaCreacion, LocalDateTime fechaModificacion, int idUsuarioCreacion) {
        this.idCliente = new SimpleIntegerProperty(idCliente);
        this.numeroTelefono = new SimpleIntegerProperty(numeroTelefono);
        this.provincia = new SimpleStringProperty(provincia);
        this.ciudad = new SimpleStringProperty(ciudad);
        this.codigoPostal = new SimpleIntegerProperty(codigoPostal);
        this.direccionCliente = new SimpleStringProperty(direccionCliente);
        this.numeroPuertaCliente = new SimpleIntegerProperty(numeroPuertaCliente);
        this.letraPuertaCliente = new SimpleStringProperty(letraPuertaCliente);
        this.estadoCliente = new SimpleBooleanProperty(estadoCliente);
        this.fechaCreacion = new SimpleObjectProperty<LocalDateTime>(fechaCreacion);
        this.fechaModificacion = new SimpleObjectProperty<LocalDateTime>(fechaModificacion);
        this.idUsuarioCreacion = new SimpleIntegerProperty(idUsuarioCreacion);
    }

    public int getIdCliente() {
        return idCliente.get();
    }

    public IntegerProperty idClienteProperty() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente.set(idCliente);
    }

    public int getNumeroTelefono() {
        return numeroTelefono.get();
    }

    public IntegerProperty numeroTelefonoProperty() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(int numeroTelefono) {
        this.numeroTelefono.set(numeroTelefono);
    }

    public String getProvincia() {
        return provincia.get();
    }

    public StringProperty provinciaProperty() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia.set(provincia);
    }

    public String getCiudad() {
        return ciudad.get();
    }

    public StringProperty ciudadProperty() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad.set(ciudad);
    }

    public int getCodigoPostal() {
        return codigoPostal.get();
    }

    public IntegerProperty codigoPostalProperty() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal.set(codigoPostal);
    }

    public String getDireccionCliente() {
        return direccionCliente.get();
    }

    public StringProperty direccionClienteProperty() {
        return direccionCliente;
    }

    public void setDireccionCliente(String direccionCliente) {
        this.direccionCliente.set(direccionCliente);
    }

    public int getNumeroPuertaCliente() {
        return numeroPuertaCliente.get();
    }

    public IntegerProperty numeroPuertaClienteProperty() {
        return numeroPuertaCliente;
    }

    public void setNumeroPuertaCliente(int numeroPuertaCliente) {
        this.numeroPuertaCliente.set(numeroPuertaCliente);
    }

    public String getLetraPuertaCliente() {
        return letraPuertaCliente.get();
    }

    public StringProperty letraPuertaClienteProperty() {
        return letraPuertaCliente;
    }

    public void setLetraPuertaCliente(String letraPuertaCliente) {
        this.letraPuertaCliente.set(letraPuertaCliente);
    }

    public boolean isEstadoCliente() {
        return estadoCliente.get();
    }

    public BooleanProperty estadoClienteProperty() {
        return estadoCliente;
    }

    public void setEstadoCliente(boolean estadoCliente) {
        this.estadoCliente.set(estadoCliente);
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion.get();
    }

    public ObjectProperty<LocalDateTime> fechaCreacionProperty() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion.set(fechaCreacion);
    }

    public LocalDateTime getFechaModificacion() {
        return fechaModificacion.get();
    }

    public ObjectProperty<LocalDateTime> fechaModificacionProperty() {
        return fechaModificacion;
    }

    public void setFechaModificacion(LocalDateTime fechaModificacion) {
        this.fechaModificacion.set(fechaModificacion);
    }

    public int getIdUsuarioCreacion() {
        return idUsuarioCreacion.get();
    }

    public IntegerProperty idUsuarioCreacionProperty() {
        return idUsuarioCreacion;
    }

    public void setIdUsuarioCreacion(int idUsuarioCreacion) {
        this.idUsuarioCreacion.set(idUsuarioCreacion);
    }

    public ClienteModel clone() {
        return new ClienteModel(this.idCliente.get(), this.numeroTelefono.get(), this.provincia.get(), this.ciudad.get(), this.codigoPostal.get(),
                this.direccionCliente.get(), this.numeroPuertaCliente.get(), this.letraPuertaCliente.get(), this.estadoCliente.get(),
                this.fechaCreacion.get(), this.fechaModificacion.get(), this.idUsuarioCreacion.get());
    }
}
