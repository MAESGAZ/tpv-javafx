package com.proyectotpv.proyectotpv.modelos.pedido;

import com.proyectotpv.proyectotpv.modelos.main.producto.ProductoModel;
import javafx.beans.property.*;

public class ProductoLineaModel implements Cloneable {
    private ObjectProperty<PedidoModel> pedido;
    private ObjectProperty<ProductoModel> producto;
    private DoubleProperty precioModificado;
    private IntegerProperty cantidad;

    public ProductoLineaModel() {
        this.pedido = new SimpleObjectProperty<PedidoModel>();
        this.producto = new SimpleObjectProperty<ProductoModel>();
        this.precioModificado = new SimpleDoubleProperty();
        this.cantidad = new SimpleIntegerProperty();
    }

    public ProductoLineaModel(PedidoModel pedido, ProductoModel producto, double precioModificado, int cantidad) {
        this.pedido = new SimpleObjectProperty<>(pedido);
        this.producto = new SimpleObjectProperty<>(producto);
        this.precioModificado = new SimpleDoubleProperty(precioModificado);
        this.cantidad = new SimpleIntegerProperty(cantidad);
    }

    public PedidoModel getPedido() {
        return pedido.get();
    }

    public ObjectProperty<PedidoModel> pedidoProperty() {
        return pedido;
    }

    public void setPedido(PedidoModel pedido) {
        this.pedido.set(pedido);
    }

    public ProductoModel getProducto() {
        return producto.get();
    }

    public ObjectProperty<ProductoModel> productoProperty() {
        return producto;
    }

    public void setProducto(ProductoModel producto) {
        this.producto.set(producto);
    }

    public double getPrecioModificado() {
        return precioModificado.get();
    }

    public DoubleProperty precioModificadoProperty() {
        return precioModificado;
    }

    public void setPrecioModificado(double precioModificado) {
        this.precioModificado.set(precioModificado);
    }

    public int getCantidad() {
        return cantidad.get();
    }

    public IntegerProperty cantidadProperty() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad.set(cantidad);
    }

    public ProductoLineaModel clone() {
        return new ProductoLineaModel(this.pedido.getValue(), this.producto.getValue(),
                this.precioModificado.getValue(), this.cantidad.getValue());
    }
}
