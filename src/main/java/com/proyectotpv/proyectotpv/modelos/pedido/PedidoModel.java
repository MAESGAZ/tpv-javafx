package com.proyectotpv.proyectotpv.modelos.pedido;

import javafx.beans.property.*;

import java.time.LocalDateTime;

public class PedidoModel implements Cloneable {
    private IntegerProperty idPedido;
    private StringProperty sitioPedido;
    private IntegerProperty numeroSitio;
    private BooleanProperty pedidoTerminado;
    private StringProperty subestados;
    private ObjectProperty<LocalDateTime> fechaCreacion;
    private ObjectProperty<LocalDateTime> fechaModificacion;
    private IntegerProperty idCliente;
    private IntegerProperty idUsuario;

    public PedidoModel() {
        this.idPedido = new SimpleIntegerProperty();
        this.sitioPedido = new SimpleStringProperty();
        this.numeroSitio = new SimpleIntegerProperty();
        this.pedidoTerminado = new SimpleBooleanProperty();
        this.subestados = new SimpleStringProperty();
        this.fechaCreacion = new SimpleObjectProperty<LocalDateTime>();
        this.fechaModificacion = new SimpleObjectProperty<LocalDateTime>();
        this.idCliente = new SimpleIntegerProperty();
        this.idUsuario = new SimpleIntegerProperty();
    }

    public PedidoModel(int idPedido, String sitioPedido, int numeroSitio, boolean pedidoTerminado, String subestados,
                       LocalDateTime fechaCreacion, LocalDateTime fechaModificacion, int idCliente, int idUsuario) {
        this.idPedido = new SimpleIntegerProperty(idPedido);
        this.sitioPedido = new SimpleStringProperty(sitioPedido);
        this.numeroSitio = new SimpleIntegerProperty(numeroSitio);
        this.pedidoTerminado = new SimpleBooleanProperty(pedidoTerminado);
        this.subestados = new SimpleStringProperty(subestados);
        this.fechaCreacion = new SimpleObjectProperty<LocalDateTime>(fechaCreacion);
        this.fechaModificacion = new SimpleObjectProperty<LocalDateTime>(fechaModificacion);
        this.idCliente = new SimpleIntegerProperty(idCliente);
        this.idUsuario = new SimpleIntegerProperty(idUsuario);
    }

    public int getIdPedido() {
        return idPedido.get();
    }

    public IntegerProperty idPedidoProperty() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido.set(idPedido);
    }

    public String getSitioPedido() {
        return sitioPedido.get();
    }

    public StringProperty sitioPedidoProperty() {
        return sitioPedido;
    }

    public void setSitioPedido(String sitioPedido) {
        this.sitioPedido.set(sitioPedido);
    }

    public int getNumeroSitio() {
        return numeroSitio.get();
    }

    public IntegerProperty numeroSitioProperty() {
        return numeroSitio;
    }

    public void setNumeroSitio(int numeroSitio) {
        this.numeroSitio.set(numeroSitio);
    }

    public boolean isPedidoTerminado() {
        return pedidoTerminado.get();
    }

    public BooleanProperty pedidoTerminadoProperty() {
        return pedidoTerminado;
    }

    public void setPedidoTerminado(boolean pedidoTerminado) {
        this.pedidoTerminado.set(pedidoTerminado);
    }

    public String getSubestados() {
        return subestados.get();
    }

    public StringProperty subestadosProperty() {
        return subestados;
    }

    public void setSubestados(String subestados) {
        this.subestados.set(subestados);
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion.get();
    }

    public ObjectProperty<LocalDateTime> fechaCreacionProperty() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion.set(fechaCreacion);
    }

    public LocalDateTime getFechaModificacion() {
        return fechaModificacion.get();
    }

    public ObjectProperty<LocalDateTime> fechaModificacionProperty() {
        return fechaModificacion;
    }

    public void setFechaModificacion(LocalDateTime fechaModificacion) {
        this.fechaModificacion.set(fechaModificacion);
    }

    public int getIdCliente() {
        return idCliente.get();
    }

    public IntegerProperty idClienteProperty() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente.set(idCliente);
    }

    public int getIdUsuario() {
        return idUsuario.get();
    }

    public IntegerProperty idUsuarioProperty() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario.set(idUsuario);
    }

    public PedidoModel clone() {
        return new PedidoModel(this.getIdPedido(), this.getSitioPedido(), this.getNumeroSitio(), this.isPedidoTerminado(),
                this.getSubestados(), this.getFechaCreacion(), this.getFechaModificacion(), this.getIdCliente(), this.getIdUsuario());
    }
}
