package com.proyectotpv.proyectotpv.modelos.pedido;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class PedidoDomicilioModel {
    private ObjectProperty<PedidoModel> pedido;
    private ObjectProperty<ClienteModel> clientePedido;

    public PedidoDomicilioModel() {
        this.pedido = new SimpleObjectProperty<>();
        this.clientePedido = new SimpleObjectProperty<>();
    }

    public PedidoDomicilioModel(PedidoModel pedido, ClienteModel cliente) {
        this.pedido = new SimpleObjectProperty<>(pedido);
        this.clientePedido = new SimpleObjectProperty<>(cliente);
    }

    public PedidoModel getPedido() {
        return pedido.get();
    }

    public ObjectProperty<PedidoModel> pedidoProperty() {
        return pedido;
    }

    public void setPedido(PedidoModel pedido) {
        this.pedido.set(pedido);
    }

    public ClienteModel getClientePedido() {
        return clientePedido.get();
    }

    public ObjectProperty<ClienteModel> clientePedidoProperty() {
        return clientePedido;
    }

    public void setClientePedido(ClienteModel clientePedido) {
        this.clientePedido.set(clientePedido);
    }

    public PedidoDomicilioModel clone() {
        return new PedidoDomicilioModel(this.pedido.getValue(), this.clientePedido.getValue());
    }
}
