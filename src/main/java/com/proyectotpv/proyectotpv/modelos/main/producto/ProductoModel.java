package com.proyectotpv.proyectotpv.modelos.main.producto;

import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import javafx.beans.property.*;

import java.time.LocalDateTime;

public class ProductoModel implements Cloneable {
    private IntegerProperty idProducto;
    private StringProperty nombreProducto;
    private StringProperty descripcionProducto;
    private IntegerProperty cantidad;
    private DoubleProperty precioProducto;
    private BooleanProperty estadoProducto;
    private ObjectProperty<LocalDateTime> fechaCreacion;
    private ObjectProperty<LocalDateTime> fechaModificacion;
    private IntegerProperty idUsuarioCreacion;
    private ObjectProperty<CategoriaModel> categoria;

    public ProductoModel() {
        this.idProducto = new SimpleIntegerProperty();
        this.nombreProducto = new SimpleStringProperty();
        this.descripcionProducto = new SimpleStringProperty();
        this.cantidad = new SimpleIntegerProperty();
        this.precioProducto = new SimpleDoubleProperty();
        this.estadoProducto = new SimpleBooleanProperty();
        this.fechaCreacion = new SimpleObjectProperty<LocalDateTime>();
        this.fechaModificacion = new SimpleObjectProperty<LocalDateTime>();
        this.idUsuarioCreacion = new SimpleIntegerProperty();
        this.categoria = new SimpleObjectProperty<CategoriaModel>();
    }

    public ProductoModel(int idProducto, String nombreProducto, String descripcionProducto, int cantidad,
                         double precioProducto, boolean estadoProducto, LocalDateTime fechaCreacion, LocalDateTime fechaModificacion,
                         int idUsuarioCreacion, CategoriaModel categoria) {
        this.idProducto = new SimpleIntegerProperty(idProducto);
        this.nombreProducto = new SimpleStringProperty(nombreProducto);
        this.descripcionProducto = new SimpleStringProperty(descripcionProducto);
        this.cantidad = new SimpleIntegerProperty(cantidad);
        this.precioProducto = new SimpleDoubleProperty(precioProducto);
        this.estadoProducto = new SimpleBooleanProperty(estadoProducto);
        this.fechaCreacion = new SimpleObjectProperty<LocalDateTime>(fechaCreacion);
        this.fechaModificacion = new SimpleObjectProperty<LocalDateTime>(fechaModificacion);
        this.idUsuarioCreacion = new SimpleIntegerProperty(idUsuarioCreacion);
        this.categoria = new SimpleObjectProperty<CategoriaModel>(categoria);
    }

    public int getIdProducto() {
        return idProducto.get();
    }

    public IntegerProperty idProductoProperty() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto.set(idProducto);
    }

    public String getNombreProducto() {
        return nombreProducto.get();
    }

    public StringProperty nombreProductoProperty() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto.set(nombreProducto);
    }

    public String getDescripcionProducto() {
        return descripcionProducto.get();
    }

    public StringProperty descripcionProductoProperty() {
        return descripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto.set(descripcionProducto);
    }

    public int getCantidad() {
        return cantidad.get();
    }

    public IntegerProperty cantidadProperty() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad.set(cantidad);
    }

    public double getPrecioProducto() {
        return precioProducto.get();
    }

    public DoubleProperty precioProductoProperty() {
        return precioProducto;
    }

    public void setPrecioProducto(double precioProducto) {
        this.precioProducto.set(precioProducto);
    }

    public boolean isEstadoProducto() {
        return estadoProducto.get();
    }

    public BooleanProperty estadoProductoProperty() {
        return estadoProducto;
    }

    public void setEstadoProducto(boolean estadoProducto) {
        this.estadoProducto.set(estadoProducto);
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion.get();
    }

    public ObjectProperty<LocalDateTime> fechaCreacionProperty() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion.set(fechaCreacion);
    }

    public LocalDateTime getFechaModificacion() {
        return fechaModificacion.get();
    }

    public ObjectProperty<LocalDateTime> fechaModificacionProperty() {
        return fechaModificacion;
    }

    public void setFechaModificacion(LocalDateTime fechaModificacion) {
        this.fechaModificacion.set(fechaModificacion);
    }

    public int getIdUsuarioCreacion() {
        return idUsuarioCreacion.get();
    }

    public IntegerProperty idUsuarioCreacionProperty() {
        return idUsuarioCreacion;
    }

    public void setIdUsuarioCreacion(int idUsuarioCreacion) {
        this.idUsuarioCreacion.set(idUsuarioCreacion);
    }

    public CategoriaModel getCategoria() {
        return categoria.get();
    }

    public ObjectProperty<CategoriaModel> categoriaProperty() {
        return categoria;
    }

    public void setCategoria(CategoriaModel categoria) {
        this.categoria.set(categoria);
    }

    public ProductoModel clone() {
        return new ProductoModel(this.idProducto.getValue(), this.nombreProducto.getValueSafe(),
                this.descripcionProducto.getValueSafe(), this.cantidad.getValue(), this.precioProducto.getValue(),
                this.estadoProducto.getValue(), this.getFechaCreacion(), this.fechaModificacion.getValue(),
                this.idUsuarioCreacion.getValue(), this.categoria.getValue());
    }
}
