package com.proyectotpv.proyectotpv.modelos.main.categorias;

import javafx.beans.property.*;

import java.time.LocalDateTime;

public class CategoriaModel implements Cloneable {
    private IntegerProperty idCategoria;
    private StringProperty nombreCategoria;
    private BooleanProperty estadoCategoria;
    private ObjectProperty<LocalDateTime> fechaCreacion;
    private ObjectProperty<LocalDateTime> fechaModificacion;
    private IntegerProperty usuarioCreacion;

    public CategoriaModel() {
        this.idCategoria = new SimpleIntegerProperty();
        this.nombreCategoria = new SimpleStringProperty();
        this.estadoCategoria = new SimpleBooleanProperty();
        this.fechaCreacion = new SimpleObjectProperty<LocalDateTime>();
        this.fechaModificacion = new SimpleObjectProperty<LocalDateTime>();
        this.usuarioCreacion = new SimpleIntegerProperty();
    }

    public CategoriaModel(int idCategoria, String nombreCategoria, boolean estadoCategoria, LocalDateTime fechaCreacion,
                          LocalDateTime fechaModificacion, int usuarioCreacion) {
        this.idCategoria = new SimpleIntegerProperty(idCategoria);
        this.nombreCategoria = new SimpleStringProperty(nombreCategoria);
        this.estadoCategoria = new SimpleBooleanProperty(estadoCategoria);
        this.fechaCreacion = new SimpleObjectProperty<LocalDateTime>(fechaCreacion);
        this.fechaModificacion = new SimpleObjectProperty<LocalDateTime>(fechaModificacion);
        this.usuarioCreacion = new SimpleIntegerProperty(usuarioCreacion);
    }

    public int getIdCategoria() {
        return idCategoria.get();
    }

    public IntegerProperty idCategoriaProperty() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria.set(idCategoria);
    }

    public String getNombreCategoria() {
        return nombreCategoria.get();
    }

    public StringProperty nombreCategoriaProperty() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria.set(nombreCategoria);
    }

    public boolean isEstadoCategoria() {
        return estadoCategoria.get();
    }

    public BooleanProperty estadoCategoriaProperty() {
        return estadoCategoria;
    }

    public void setEstadoCategoria(boolean estadoCategoria) {
        this.estadoCategoria.set(estadoCategoria);
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion.get();
    }

    public ObjectProperty<LocalDateTime> fechaCreacionProperty() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion.set(fechaCreacion);
    }

    public LocalDateTime getFechaModificacion() {
        return fechaModificacion.get();
    }

    public ObjectProperty<LocalDateTime> fechaModificacionProperty() {
        return fechaModificacion;
    }

    public void setFechaModificacion(LocalDateTime fechaModificacion) {
        this.fechaModificacion.set(fechaModificacion);
    }

    public int getUsuarioCreacion() {
        return usuarioCreacion.get();
    }

    public IntegerProperty usuarioCreacionProperty() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(int usuarioCreacion) {
        this.usuarioCreacion.set(usuarioCreacion);
    }

    @Override
    public String toString() {
        return getNombreCategoria();
    }

    public CategoriaModel clone() {
        return new CategoriaModel(getIdCategoria(), getNombreCategoria(), this.estadoCategoriaProperty().getValue(), getFechaCreacion(), getFechaModificacion(), getUsuarioCreacion());
    }
}
