package com.proyectotpv.proyectotpv.modelos.main;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Locale;

public class PaisModel {
    private StringProperty codigoPais;
    private StringProperty regionPais;
    private StringProperty nombrePais;

    public PaisModel(String codigoPais, String regionPais, String nombrePais) {
        this.codigoPais = new SimpleStringProperty(codigoPais);
        this.regionPais = new SimpleStringProperty(regionPais);
        this.nombrePais = new SimpleStringProperty(nombrePais);
    }

    public PaisModel() {
        this.codigoPais = new SimpleStringProperty();
        this.regionPais = new SimpleStringProperty();
        this.nombrePais = new SimpleStringProperty();
    }

    public String getCodigoPais() {
        return codigoPais.get();
    }

    public StringProperty codigoPaisProperty() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais.set(codigoPais);
    }

    public String getRegionPais() {
        return regionPais.get();
    }

    public StringProperty regionPaisProperty() {
        return regionPais;
    }

    public void setRegionPais(String regionPais) {
        this.regionPais.set(regionPais);
    }

    public String getNombrePais() {
        return nombrePais.get();
    }

    public StringProperty nombrePaisProperty() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais.set(nombrePais);
    }

    public Locale getLocale() {
        return new Locale(this.getCodigoPais(), this.getRegionPais());
    }

    public String toString() {
        return getNombrePais();
    }
}
