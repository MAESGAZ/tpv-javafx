package com.proyectotpv.proyectotpv.modelos.logininicio;

import javafx.beans.property.*;

import java.time.LocalDateTime;

public class UsuarioModel implements Cloneable {
    private IntegerProperty idUsuario;
    private StringProperty usuario;
    private StringProperty nombreUsuario;
    private StringProperty apellidosUsuario;
    private BooleanProperty estadoUsuario;
    private StringProperty correoElectronico;
    private StringProperty claveUsuario;
    private BooleanProperty cambioClave;
    private ObjectProperty<LocalDateTime> fechaCreacion;
    private ObjectProperty<LocalDateTime> fechaCambioClave;
    private ObjectProperty<RolModel> rolUsuario;

    public UsuarioModel() {
        this.idUsuario = new SimpleIntegerProperty();
        this.usuario = new SimpleStringProperty();
        this.nombreUsuario = new SimpleStringProperty();
        this.apellidosUsuario = new SimpleStringProperty();
        this.estadoUsuario = new SimpleBooleanProperty();
        this.correoElectronico = new SimpleStringProperty();
        this.claveUsuario = new SimpleStringProperty();
        this.cambioClave = new SimpleBooleanProperty();
        this.fechaCreacion = new SimpleObjectProperty<LocalDateTime>();
        this.fechaCambioClave = new SimpleObjectProperty<LocalDateTime>();
        this.rolUsuario = new SimpleObjectProperty<RolModel>();
    }

    public UsuarioModel(int idUsuario, String usuario, String nombreUsuario, String apellidosUsuario,
                        boolean estadoUsuario, String correoElectronico, String claveUsuario, boolean cambioClave, LocalDateTime fechaCreacion,
                        LocalDateTime fechaCambioClave, RolModel rolUsuario) {
        this.idUsuario = new SimpleIntegerProperty(idUsuario);
        this.usuario = new SimpleStringProperty(usuario);
        this.nombreUsuario = new SimpleStringProperty(nombreUsuario);
        this.apellidosUsuario = new SimpleStringProperty(apellidosUsuario);
        this.estadoUsuario = new SimpleBooleanProperty(estadoUsuario);
        this.correoElectronico = new SimpleStringProperty(correoElectronico);
        this.claveUsuario = new SimpleStringProperty(claveUsuario);
        this.cambioClave = new SimpleBooleanProperty(cambioClave);
        this.fechaCreacion = new SimpleObjectProperty<LocalDateTime>(fechaCreacion);
        this.fechaCambioClave = new SimpleObjectProperty<LocalDateTime>(fechaCambioClave);
        this.rolUsuario = new SimpleObjectProperty<RolModel>(rolUsuario);
    }

    public IntegerProperty getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(IntegerProperty idUsuario) {
        this.idUsuario = idUsuario;
    }

    public StringProperty getUsuario() {
        return usuario;
    }

    public void setUsuario(StringProperty usuario) {
        this.usuario = usuario;
    }

    public StringProperty getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(StringProperty nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public StringProperty getApellidosUsuario() {
        return apellidosUsuario;
    }

    public void setApellidosUsuario(StringProperty apellidosUsuario) {
        this.apellidosUsuario = apellidosUsuario;
    }

    public BooleanProperty getEstadoUsuario() {
        return estadoUsuario;
    }

    public void setEstadoUsuario(BooleanProperty estadoUsuario) {
        this.estadoUsuario = estadoUsuario;
    }

    public StringProperty getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(StringProperty correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public StringProperty getClaveUsuario() {
        return claveUsuario;
    }

    public void setClaveUsuario(StringProperty claveUsuario) {
        this.claveUsuario = claveUsuario;
    }

    public BooleanProperty getCambioClave() {
        return cambioClave;
    }

    public void setCambioClave(BooleanProperty cambioClave) {
        this.cambioClave = cambioClave;
    }

    public ObjectProperty<LocalDateTime> getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(ObjectProperty<LocalDateTime> fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public ObjectProperty<LocalDateTime> getFechaCambioClave() {
        return fechaCambioClave;
    }

    public void setFechaCambioClave(ObjectProperty<LocalDateTime> fechaCambioClave) {
        this.fechaCambioClave = fechaCambioClave;
    }

    public ObjectProperty<RolModel> getRolUsuario() {
        return rolUsuario;
    }

    public void setRolUsuario(ObjectProperty<RolModel> rolUsuario) {
        this.rolUsuario = rolUsuario;
    }

    public UsuarioModel clone() {
        return new UsuarioModel(this.idUsuario.get(), this.usuario.get(), this.nombreUsuario.get(),
                this.apellidosUsuario.get(), this.estadoUsuario.get(), this.correoElectronico.get(),
                this.claveUsuario.get(), this.cambioClave.get(), this.fechaCreacion.get(), this.fechaCambioClave.get(), this.rolUsuario.get());
    }
}
