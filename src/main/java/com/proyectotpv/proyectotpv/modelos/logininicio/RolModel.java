package com.proyectotpv.proyectotpv.modelos.logininicio;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class RolModel {
    private IntegerProperty idRol;
    private StringProperty nombreRol;

    public RolModel() {
        this.idRol = new SimpleIntegerProperty();
        this.nombreRol = new SimpleStringProperty();
    }

    public RolModel(int idRol, String nombreRol) {
        this.idRol = new SimpleIntegerProperty(idRol);
        this.nombreRol = new SimpleStringProperty(nombreRol);
    }

    public IntegerProperty getIdRol() {
        return idRol;
    }

    public void setIdRol(IntegerProperty idRol) {
        this.idRol = idRol;
    }

    public StringProperty getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(StringProperty nombreRol) {
        this.nombreRol = nombreRol;
    }

    public String toString() {
        return this.nombreRol.get();
    }
}
