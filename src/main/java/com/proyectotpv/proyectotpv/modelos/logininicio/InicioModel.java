package com.proyectotpv.proyectotpv.modelos.logininicio;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InicioModel {
    private StringProperty usuario;
    private StringProperty claveUsuario;

    public InicioModel() {
        this.usuario = new SimpleStringProperty();
        this.claveUsuario = new SimpleStringProperty();
    }

    public InicioModel(String usuario, String claveUsuario) {
        this.usuario = new SimpleStringProperty(usuario);
        this.claveUsuario = new SimpleStringProperty(claveUsuario);
    }

    public StringProperty getUsuario() {
        return usuario;
    }

    public void setUsuario(StringProperty usuario) {
        this.usuario = usuario;
    }

    public StringProperty getClaveUsuario() {
        return claveUsuario;
    }

    public void setClaveUsuario(StringProperty claveUsuario) {
        this.claveUsuario = claveUsuario;
    }

    public InicioModel clone() {
        return new InicioModel(this.usuario.get(), this.claveUsuario.get());
    }
}
