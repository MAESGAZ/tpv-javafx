package com.proyectotpv.proyectotpv.recursos;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Logs {
    private Logger logger;
    private FileHandler fileHandler;

    public Logs() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");

        this.logger = Logger.getLogger(Logs.class.getName());
        try {
            this.fileHandler = new FileHandler("TPVLogs_"+format.format(new Date())+".log", true);
            this.formatoLogs();
            this.fileHandler.setEncoding("UTF-8");
            this.logger.addHandler(fileHandler);
            this.logger.setLevel(Level.ALL);
            this.logger.log(Level.INFO,"---> INICIO DEL LOG <---");
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Logger getLogs() {
        return this.logger;
    }

    private void formatoLogs() {
        this.fileHandler.setFormatter(new SimpleFormatter());
    }
}
