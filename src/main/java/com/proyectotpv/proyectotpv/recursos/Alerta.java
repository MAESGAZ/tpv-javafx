package com.proyectotpv.proyectotpv.recursos;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class Alerta extends Alert {
    public Alerta(AlertType alertType) {
        super(alertType);
        // TODO Auto-generated constructor stub
    }

    public Alerta(AlertType alertType, String title, String header, String content) {
        super(alertType);
        this.setTitle(title);
        this.setHeaderText(header);
        this.setContentText(content);
    }

    public Alerta(String titulo, String header, String cuerpo, ButtonType boton1, ButtonType boton2) {
        super(AlertType.CONFIRMATION, cuerpo, boton1, boton2);
        this.setTitle(titulo);
        this.setHeaderText(header);
    }
}
