package com.proyectotpv.proyectotpv.recursos;

public class AplicacionExcepcion extends Exception {
    public AplicacionExcepcion() {
        super();
    }

    public AplicacionExcepcion(String mensaje) {
        super(mensaje);
    }
}
