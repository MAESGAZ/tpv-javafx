package com.proyectotpv.proyectotpv.recursos;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

public class CifradorClave {
    private int longitud;
    private String cifrado;

    public CifradorClave() {
        this.longitud = Constantes.CLAVE_LONGITUD;
        this.cifrado = Constantes.ALGORITMO_SHA;
    }

    public CifradorClave(int longitud, String cifrado) {
        this.longitud = longitud;
        this.cifrado = cifrado;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public String getCifrado() {
        return cifrado;
    }

    public void setCifrado(String cifrado) {
        this.cifrado = cifrado;
    }

    public String generarSSHA(String password) throws NoSuchAlgorithmException {
        byte[] salt = new byte[this.longitud];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(salt);
        return generarSSHA(password, salt);
    }

    private String generarSSHA(String password, byte[] salt) throws NoSuchAlgorithmException {
        MessageDigest crypt = MessageDigest.getInstance(this.cifrado);
        crypt.reset();
        crypt.update(password.getBytes());
        crypt.update(salt);
        byte[] hash = crypt.digest();

        byte[] hashPlushSalt = new byte[hash.length + salt.length];
        System.arraycopy(hash, 0, hashPlushSalt, 0, hash.length);
        System.arraycopy(salt, 0, hashPlushSalt, hash.length, salt.length);

        return new StringBuilder().append("{SSHA}").append(Base64.getEncoder().encodeToString(hashPlushSalt))
                .toString();
    }

    public boolean comprobarClaves(String ssha, String password) {
        String s = ssha.substring(ssha.indexOf("}") + 1);
        byte[] decodedSsha = Base64.getDecoder().decode(s);
        byte[] salt = new byte[this.longitud];

        System.arraycopy(decodedSsha, decodedSsha.length - this.longitud, salt, 0, this.longitud);

        String ssha2 = "";
        try {
            ssha2 = generarSSHA(password, salt);
        } catch(NoSuchAlgorithmException e) {
            return false;
        }
        return ssha.equals(ssha2);
    }
}
