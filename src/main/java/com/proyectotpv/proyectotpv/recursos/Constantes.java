package com.proyectotpv.proyectotpv.recursos;

public class Constantes {
    /* Rutas Package principales*/
    public static final String PATH_CONTROLLER = "/controller";
    public static final String PATH_MODEL = "/model";
    public static final String PATH_RESOURCES = "com.proyectotpv.proyectotpv";
    public static final String PATH_RESOURCES_BARRA = "/com/proyectotpv/proyectotpv";
    public static final String PATH_VIEWS = "vistas";

    /* Cifrado PASSWORD */
    public static final String ALGORITMO_SHA = "SHA-512";
    public static final int CLAVE_LONGITUD = 8;

    /* Rutas Estilos */
    public static final String PATH_STYLES = PATH_RESOURCES + "/styles";
    public static final String STL_BARRA_ITEM = PATH_STYLES + "/barraitem.css";
    public static final String STL_CATEGORIA_ITEM = PATH_STYLES + "/categoriaItem.css";
    public static final String STL_LOGIN_INICIO = PATH_STYLES + "/loginInicio.css";
    public static final String STL_MAIN_APLICACION = PATH_STYLES + "/mainAplicacion.css";
    public static final String STL_MESA_ITEM = PATH_STYLES + "/mesaitem.css";
    public static final String STL_PEDIDO = PATH_STYLES + "/pedido.css";
    public static final String STL_TERRAZA_ITEM = PATH_STYLES + "/terrazaitem.css";


    /* Elementos de Interfaz */
    public static final String ELMT_ICON = PATH_RESOURCES_BARRA + "/imagenes/icon-tpv.png";

    /* Internacionalización */
    public static final String PATH_INTERNATIONALIZATION = PATH_RESOURCES + ".lenguage.lenguage";

    /* Rutas VIEWS */
    public static final String PATH_INICIO = PATH_RESOURCES_BARRA + "/vistas";
    public static final String PATH_CAMBIO_CLAVE = PATH_INICIO + "/cambioclave";
    public static final String PATH_MAIN = PATH_INICIO + "/main";
    public static final String PATH_MAIN_CATEGORIAS = PATH_MAIN + "/categorias";
    public static final String PATH_MAIN_CLIENTE = PATH_MAIN + "/clientes";
    public static final String PATH_MAIN_DISTRIBUCION = PATH_MAIN + "/distribucion";
    public static final String PATH_MAIN_DISTRIBUCION_BARRA = PATH_MAIN_DISTRIBUCION + "/barraitem";
    public static final String PATH_MAIN_DISTRIBUCION_DOMICILIO = PATH_MAIN_DISTRIBUCION + "/domicilioitem";
    public static final String PATH_MAIN_DISTRIBUCION_SALON = PATH_MAIN_DISTRIBUCION + "/mesaitem";
    public static final String PATH_MAIN_DISTRIBUCION_TERRAZA = PATH_MAIN_DISTRIBUCION + "/terrazaitem";
    public static final String PATH_MAIN_INFORMES = PATH_MAIN + "/informes";
    public static final String PATH_MAIN_PRODUCTO = PATH_MAIN + "/producto";
    public static final String PATH_MAIN_USUARIO = PATH_MAIN + "/usuario";
    public static final String PATH_PEDIDO = PATH_INICIO + "/pedido";
    public static final String PATH_PEDIDO_CATEGORIA = PATH_PEDIDO + "/categoria";
    public static final String PATH_PEDIDO_PRODUCTO = PATH_PEDIDO + "/producto";

    /* Ficheros FXML Inicio */
    public static final String FXML_LOGIN_INICIO = "vistas/logininicio/ViewLoginInicio.fxml";
    public static final String FXML_CAMBIO_CLAVE = PATH_CAMBIO_CLAVE + "/ViewCambioClave.fxml";
    public static final String FXML_MAIN_APLICACION = PATH_MAIN + "/ViewMainAplicacion.fxml";
    public static final String FXML_CATEGORIAS = PATH_MAIN_CATEGORIAS + "/ViewCategorias.fxml";
    public static final String FXML_EDITAR_CATEGORIA = PATH_MAIN_CATEGORIAS + "/ViewEditarCategoria.fxml";
    public static final String FXML_NUEVA_CATEGORIA = PATH_MAIN_CATEGORIAS + "/ViewNuevaCategoria.fxml";
    public static final String FXML_BUSCAR_CLIENTE = PATH_MAIN_CLIENTE + "/ViewBuscarCliente.fxml";
    public static final String FXML_CLIENTES = PATH_MAIN_CLIENTE + "/ViewClientes.fxml";
    public static final String FXML_EDITAR_CLIENTE = PATH_MAIN_CLIENTE + "/ViewEditarCliente.fxml";
    public static final String FXML_NUEVO_CLIENTE = PATH_MAIN_CLIENTE + "/ViewNuevoCliente.fxml";
    public static final String FXML_DISTRIBUCION_SALA = PATH_MAIN_DISTRIBUCION + "/ViewDistribucionSala.fxml";
    public static final String FXML_BARRA_ITEM = PATH_MAIN_DISTRIBUCION_BARRA + "/ViewBarraItem.fxml";
    public static final String FXML_DOMICILIO_ITEM = PATH_MAIN_DISTRIBUCION_DOMICILIO + "/ViewDomicilioItem.fxml";
    public static final String FXML_SALON_ITEM = PATH_MAIN_DISTRIBUCION_SALON + "/ViewMesaItem.fxml";
    public static final String FXML_TERRAZA_ITEM = PATH_MAIN_DISTRIBUCION_TERRAZA + "/ViewTerrazaItem.fxml";
    public static final String FXML_INFORMES = PATH_MAIN_INFORMES + "/ViewInformes.fxml";
    public static final String FXML_PRODUCTOS = PATH_MAIN_PRODUCTO + "/ViewProductos.fxml";
    public static final String FXML_EDITAR_PRODUCTO = PATH_MAIN_PRODUCTO + "/ViewEditarProducto.fxml";
    public static final String FXML_NUEVO_PRODUCTO = PATH_MAIN_PRODUCTO + "/ViewNuevoProducto.fxml";
    public static final String FXML_USUARIOS = PATH_MAIN_USUARIO + "/ViewUsuarios.fxml";
    public static final String FXML_EDITAR_USUARIO = PATH_MAIN_USUARIO + "/ViewEditarUsuario.fxml";
    public static final String FXML_NUEVO_USUARIO = PATH_MAIN_USUARIO + "/ViewNuevoUsuario.fxml";
    public static final String FXML_PEDIDO = PATH_PEDIDO + "/ViewPedido.fxml";
    public static final String FXML_PEDIDO_CATEGORIA = PATH_PEDIDO_CATEGORIA + "/ViewPedidoCategoria.fxml";
    public static final String FXML_PEDIDO_PRODUCTO = PATH_PEDIDO_PRODUCTO + "/ViewProductoPedido.fxml";
    public static final String FXML_MAIN_MODIFICAR_USUARIO = PATH_MAIN + "/ViewMainModificarUsuario.fxml";

    /* Configuracion BBDD */
    public static final String BBDD_CONF_DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String BBDD_CONF_HOSTNAME = "localhost";
    public static final String BBDD_CONF_DATABASE = "proyectotpv";
    public static final String BBDD_CONF_USUARIO = "root";
    public static final String BBDD_CONF_PASSWORD = "";
    public static final String BBDD_CONF_URL = "jdbc:mysql://";
    public static final String BBDD_CONF_PORT = "3306";


    /* Distribución Pedidos */
    public static final String SALON_PEDIDO = "Mesa Salón";
    public static final String TERRAZA_PEDIDO = "Mesa Terraza";
    public static final String BARRA_PEDIDO = "Asiento Barra";
    public static final String DOMICILIO_PEDIDO = "Pedido Domicilio";

    /* Subestados Pedido */
    public static final String ESTADO_INICIADO = "inicio";
    public static final String ESTADO_ENVIADO = "enviado";
    public static final String ESTADO_FACTURA = "facturado";
    public static final String ESTADO_COBRADO = "cobrado";

    public static final int CLIENTE_FANTASMA = 1;

    /* Lenguajes Internacionalizacion */
    public static final String CODIGO_IDIOMA_ESP = "es";
    public static final String CODIGO_IDIOMA_ENG = "en";
    public static final String CODIGO_REGION_ESP = "ES";
    public static final String CODIGO_REGION_ENG = "UK";
    public static final String TEXTO_IDIOMA_ESP = "Español";
    public static final String TEXTO_IDIOMA_ENG = "English";

    /* Formato Fecha Reloj y patrones */
    public static final String FORMATO_FECHA_RELOJ = "dd-MM-YYYY HH:mm:ss";
    public static final String PATRON_CORREO= "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    public static final String FECHA_VACIA = "##-##-####";
    public static final String PATRON_NUMERO = "[^\\d]";
    public static final String PATRON_DECIMALES = "^[0-9]*\\,[0-9]{2}";
    public static final String EMPTY_STRING="";

    /* ID Rol Usuarios */
    public static final int ID_ROL_ADMINISTRADOR = 1;
    public static final int ID_ROL_GERENTE = 2;
    public static final int ID_ROL_CAMARERO = 3;

    /* Estados Usuarios, categorias, productos, clientes */
    public static final String ESTADO_ACTIVADO = "Activado";
    public static final String ESTADO_DESACTIVADO = "Desactivado";

    /* Metodos HTTP */
    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";
    public static final String HTTP_PUT = "PUT";
    public static final String HTTP_DELETE = "DELETE";

    public static final String URL_API = "localhost:3000/proyectotpv/v1";
    public static final String URL_API_INICIO = URL_API + "/inicio";
}
