package com.proyectotpv.proyectotpv;

import com.proyectotpv.proyectotpv.controladores.logininicio.ControladorLoginInicio;
import com.proyectotpv.proyectotpv.recursos.Constantes;
import com.proyectotpv.proyectotpv.recursos.Internacionalizacion;
import com.proyectotpv.proyectotpv.recursos.Logs;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;
import java.util.ResourceBundle;

public class HelloApplication extends Application {
    private Stage primaryStage;

    private Logs logs;

    @Override
    public void start(Stage stage) throws IOException {
        this.primaryStage = stage;
        this.primaryStage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
        logs = new Logs();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource(Constantes.FXML_LOGIN_INICIO));
            fxmlLoader.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getDefaultLocale()));
            primaryStage.setScene(new Scene((AnchorPane) fxmlLoader.load()));
            ControladorLoginInicio controladorLoginInicio = fxmlLoader.getController();
            controladorLoginInicio.setLogs(logs);
            primaryStage.setTitle(Internacionalizacion.get("title"));
            primaryStage.setResizable(false);
            primaryStage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            primaryStage.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        launch();
    }
}