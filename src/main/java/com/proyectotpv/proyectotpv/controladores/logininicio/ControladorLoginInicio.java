package com.proyectotpv.proyectotpv.controladores.logininicio;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import com.proyectotpv.proyectotpv.controladores.cambioclave.ControladorCambioClave;
import com.proyectotpv.proyectotpv.controladores.main.ControladorMainAplicacion;
import com.proyectotpv.proyectotpv.modelos.logininicio.InicioModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.usuario.UsuarioUtilsBD;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ControladorLoginInicio {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ImageView iwImagen;

    @FXML
    private PasswordField tfPassword;

    @FXML
    private TextField tfUsuario;

    private Logs logs;

    private InicioModel usuario = new InicioModel();

    @FXML
    void iniciarSesion(ActionEvent event) {
        logs.getLogs().info("-- Inicio sesión con el usuario " + usuario.getUsuario().get() + " --");
        UsuarioUtilsBD utilidadesUsuario = null;
        try {
            // Realizamos las comprobaciones.
            this.comprobaciones();

            CifradorClave cifrador = new CifradorClave();
            utilidadesUsuario = new UsuarioUtilsBD();
            // Obtenemos el hash o clave cifrada del usuario introducido.
            String claveUsuario = utilidadesUsuario.obtenerClaveUsuario(usuario);
            if (claveUsuario.isEmpty()) {
                this.logs.getLogs().warning("El usuario " + usuario.getUsuario().getValueSafe() + " no existe.");
                throw new Exception(Internacionalizacion.get("aler.controllerlogin.noexiste"));
            }
            // Comprobamos las claves para comprobar que es la misma.
            if (!cifrador.comprobarClaves(claveUsuario,
                    this.usuario.getClaveUsuario().getValueSafe())) {
                this.logs.getLogs().warning("Contraseña equivocada para el usuario " + usuario.getUsuario().getValueSafe());
                throw new Exception(Internacionalizacion.get("aler.controllerlogin.noexiste"));
            }
            // Recuperamos los datos del usuario.
            UsuarioModel usuarioSesion = utilidadesUsuario.iniciarSesion(usuario);
            if (!usuarioSesion.getEstadoUsuario().get()) {
                this.logs.getLogs().warning("El usuario " + this.usuario.getUsuario().getValueSafe() + " no está activo.");
                throw new Exception(Internacionalizacion.get("aler.controllerlogin.noactivo"));
            }
            // Añadimos un evento para matar los hilos si se cierra la siguiente ventana.
            Stage stage = new Stage();
            stage.setOnCloseRequest(evento -> {
                Platform.exit();
                System.exit(0);
            });
            // Si el usuario tiene la tiene que cambiar la contraseña para dirigirlo a cambio de clave.
            if (usuarioSesion.getCambioClave().get()
                    || usuarioSesion.getFechaCambioClave().get().isBefore(LocalDateTime.now())) {
                stage.setTitle(Internacionalizacion.get("camb.title"));
                FXMLLoader viewCambioClave = new FXMLLoader();
                viewCambioClave.setLocation(
                        getClass().getResource(Constantes.FXML_CAMBIO_CLAVE));
                viewCambioClave.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
                stage.setScene(new Scene((AnchorPane) viewCambioClave.load()));
                stage.setResizable(false);
                ControladorCambioClave controlador = viewCambioClave.getController();
                controlador.setUsuario(usuarioSesion);
                controlador.setLogs(logs);
                controlador.iniciarControlador();
            } else {
                // Iniciamos la ventana principal de la aplicación.
                stage.setTitle(Internacionalizacion.get("title"));
                FXMLLoader viewMainAplicacion = new FXMLLoader();
                viewMainAplicacion.setLocation(getClass().getResource(/*Constantes.FXML_MAIN_APLICACION*/"/com/proyectotpv/proyectotpv/vistas/main/ViewMainAplicacion.fxml"));
                viewMainAplicacion.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
                stage.setScene(new Scene((ScrollPane) viewMainAplicacion.load()));
                ControladorMainAplicacion controlador = viewMainAplicacion.getController();
                controlador.setLogs(logs);
                controlador.setUsuarioSesion(usuarioSesion);
                controlador.iniciarControlador();
            }
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            stage.show();
            ((Stage) this.tfUsuario.getScene().getWindow()).close();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"), Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (Exception e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"), Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (utilidadesUsuario != null)
                utilidadesUsuario.cerrarConexion();
        }
    }

    @FXML
    void initialize() {
        this.iwImagen.imageProperty().set(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
        this.tfUsuario.textProperty().bindBidirectional(this.usuario.getUsuario());
        this.tfPassword.textProperty().bindBidirectional(this.usuario.getClaveUsuario());
    }

    public void setLogs(Logs logs) {
        this.logs = logs;
    }

    private void comprobaciones() throws AplicacionExcepcion {
        if (this.usuario.getUsuario().isEmpty().getValue())
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.controllerlogin.usuario"));
        if (this.usuario.getClaveUsuario().isEmpty().getValue())
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.controllerlogin.clave"));
    }

}
