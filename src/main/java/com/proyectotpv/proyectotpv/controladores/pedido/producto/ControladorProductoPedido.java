package com.proyectotpv.proyectotpv.controladores.pedido.producto;

import com.proyectotpv.proyectotpv.modelos.main.producto.ProductoModel;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class ControladorProductoPedido {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lbDescripcion;

    @FXML
    private Label lbNombreProducto;

    @FXML
    private Label lbPrecio;

    private ProductoModel producto;

    @FXML
    void initialize() {

    }

    public void setProducto(ProductoModel producto) {
        this.producto = producto;
    }

    public void iniciarControlador() {
        this.lbNombreProducto.textProperty().bindBidirectional(this.producto.nombreProductoProperty());
        this.lbDescripcion.textProperty().bindBidirectional(this.producto.descripcionProductoProperty());
        this.lbPrecio.textProperty().setValue(this.producto.precioProductoProperty().asString("%.2f").getValueSafe().toString());
        //this.lbPrecio.textProperty().bindBidirectional(this.producto.getPrecioProducto(), new NumberStringConverter());
    }
}
