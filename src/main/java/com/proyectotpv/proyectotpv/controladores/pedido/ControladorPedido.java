package com.proyectotpv.proyectotpv.controladores.pedido;

import com.proyectotpv.proyectotpv.controladores.pedido.categoria.ControladorCategoriaPedido;
import com.proyectotpv.proyectotpv.controladores.pedido.producto.ControladorProductoPedido;
import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import com.proyectotpv.proyectotpv.modelos.main.producto.ProductoModel;
import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import com.proyectotpv.proyectotpv.modelos.pedido.ProductoLineaModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.categoria.CategoriasUtilsBD;
import com.proyectotpv.proyectotpv.utiles.basedatos.pedido.PedidoUtilsBD;
import com.proyectotpv.proyectotpv.utiles.basedatos.pedido.ProductosLineaUtilsBD;
import com.proyectotpv.proyectotpv.utiles.basedatos.producto.ProductoUtilsBD;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ControladorPedido {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btCobrarPedido;

    @FXML
    private Button btDescuento;

    @FXML
    private Button btEliminar;

    @FXML
    private Button btEnviarPedido;

    @FXML
    private Button btFactura;

    @FXML
    private Button btPrecio;

    @FXML
    private Button btRetrocederPedido;

    @FXML
    private Button btnBorrar;

    @FXML
    private Button btnCal0;

    @FXML
    private Button btnCal1;

    @FXML
    private Button btnCal2;

    @FXML
    private Button btnCal3;

    @FXML
    private Button btnCal4;

    @FXML
    private Button btnCal5;

    @FXML
    private Button btnCal6;

    @FXML
    private Button btnCal7;

    @FXML
    private Button btnCal8;

    @FXML
    private Button btnCal9;

    @FXML
    private Button btnCalComa;

    @FXML
    private Button btnConfirmar;

    @FXML
    private TableColumn<ProductoLineaModel, String> ctbCantidad;

    @FXML
    private TableColumn<ProductoLineaModel, String> ctbPrecio;

    @FXML
    private TableColumn<ProductoLineaModel, String> ctbProducto;

    @FXML
    private GridPane gridCategorias;

    @FXML
    private GridPane gridProductos;

    @FXML
    private Label lbEstadoPedido;

    @FXML
    private Label lbHora;

    @FXML
    private Label lbIdPedido;

    @FXML
    private Label lbLugarPedido;

    @FXML
    private Label lbNumeroPedido;

    @FXML
    private Label lbPrecioCalculadora;

    @FXML
    private Label lbTotal;

    @FXML
    private TableView<ProductoLineaModel> tbListaProductos;

    private ObservableList<CategoriaModel> listadoCategorias = FXCollections.observableArrayList();
    private ObservableList<ProductoModel> listadoProductos = FXCollections.observableArrayList();
    private ObservableList<ProductoLineaModel> listadoLineaProductos = FXCollections.observableArrayList();

    private boolean existeProducto;

    private double totalPrecio = 0;

    private PedidoModel pedido;

    private Logs logs;

    private boolean precioValidado = false;

    private boolean descuentoValidado = false;

    private boolean permitirAgregar = false;

    /**
     * Valida para aplica un descuento.
     * @param event
     */
    @FXML
    void aplicarDescuento(ActionEvent event) {
        if (!this.permitirAgregar) {
            this.precioValidado = false;
            try {
                if (this.lbPrecioCalculadora.textProperty().getValueSafe().contains(","))
                    throw new AplicacionExcepcion(Internacionalizacion.get("aler.pedi.coma"));
                int numero = Integer.parseInt(this.lbPrecioCalculadora.textProperty().getValueSafe());
                if (numero > 100)
                    throw new AplicacionExcepcion(Internacionalizacion.get("aler.pedi.limite"));
                this.descuentoValidado = true;
            } catch (AplicacionExcepcion e) {
                Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                        Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
                Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().warning("Aviso: " + e.getMessage());
                stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            }
        } else {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"),
                    Internacionalizacion.get("aler.pedi.descuentoestado"));
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + "Producto con factura impresa, no se puede editar.");
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Valida para aplicar un precio.
     * @param event
     */
    @FXML
    void aplicarPrecio(ActionEvent event) {
        if (!this.permitirAgregar) {
            this.descuentoValidado = false;
            String patron = Constantes.PATRON_DECIMALES;
            Pattern pat = Pattern.compile(patron);
            Matcher mat = pat.matcher(this.lbPrecioCalculadora.textProperty().getValueSafe());
            if (mat.matches()) {
                this.precioValidado = true;
            } else {
                Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                        Internacionalizacion.get("aler.warn.cabecera"),
                        Internacionalizacion.get("aler.pedi.formatoprecio"));
                Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().warning("Aviso: " + "El precio introducido no cumple el formato correcto.");
                stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            }
        } else {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"), Internacionalizacion.get("aler.pedi.precioestado"));
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + "Producto con factura impresa, no se puede editar.");
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Obtiene el valor del boton pulsado.
     */
    @FXML
    void botonCalculadora(ActionEvent event) {
        lbPrecioCalculadora.textProperty()
                .setValue(lbPrecioCalculadora.getText().concat(((Button) event.getSource()).getText()));
        this.precioValidado = false;
    }

    /**
     * Cambia el estado del pedido a cobrado y finaliza el pedido.
     * @param event
     */
    @FXML
    void cobrarPedido(ActionEvent event) {
        boolean exito = false;
        this.pedido.setPedidoTerminado(true);
        this.pedido.setSubestados(Constantes.ESTADO_COBRADO);
        PedidoUtilsBD pedidosUtils = null;
        ProductosLineaUtilsBD productosUtils = null;
        try {
            pedidosUtils = new PedidoUtilsBD();
            pedidosUtils.modificarPedido(pedido);
            productosUtils = new ProductosLineaUtilsBD();
            productosUtils.eliminarPedido(pedido);
            productosUtils.guardarEnHistorico(listadoLineaProductos);
            exito = true;
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (pedidosUtils != null)
                pedidosUtils.cerrarConexion();
            if (productosUtils != null)
                productosUtils.cerrarConexion();
        }
        if (exito)
            ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Confirma los cambios de aplicar descuentos o precios.
     * @param event
     */
    @FXML
    void confirmarCalculadora(ActionEvent event) {
        if (this.tbListaProductos.getSelectionModel().getSelectedItem() != null) {
            if (!this.descuentoValidado) {
                if (this.precioValidado) {
                    double precio = Double
                            .parseDouble(this.lbPrecioCalculadora.textProperty().getValueSafe().replace(",", "."));
                    this.tbListaProductos.getSelectionModel().getSelectedItem()
                            .setPrecioModificado(precio);
                    this.actualizarPrecioProducto(this.tbListaProductos.getSelectionModel().getSelectedItem());
                    this.refrescarListadoProductos();
                    this.eliminarNumeroCalculadora(event);
                    this.calcularTotal();
                } else {
                    Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                            Internacionalizacion.get("aler.warn.cabecera"),
                            Internacionalizacion.get("aler.pedi.preciovalidado"));
                    Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                    this.logs.getLogs().warning("Aviso: " + "El precio introducido no ha sido validado.");
                    stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                    alerta.showAndWait();
                }
            } else if (!this.precioValidado) {
                if (this.descuentoValidado) {
                    ProductoLineaModel producto = this.tbListaProductos.getSelectionModel().getSelectedItem();
                    double precio = producto.precioModificadoProperty().getValue()
                            * Double.parseDouble("0." + this.lbPrecioCalculadora.textProperty().getValueSafe());
                    producto.precioModificadoProperty().setValue(
                            this.tbListaProductos.getSelectionModel().getSelectedItem().getPrecioModificado()
                                    - precio);
                    this.actualizarPrecioProducto(this.tbListaProductos.getSelectionModel().getSelectedItem());
                    this.refrescarListadoProductos();
                    this.eliminarNumeroCalculadora(event);
                    this.calcularTotal();
                } else {
                    Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                            Internacionalizacion.get("aler.warn.cabecera"),
                            Internacionalizacion.get("aler.pedi.descuentovalidado"));
                    Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                    this.logs.getLogs().warning("Aviso: " + "El descuento introducido no ha sido validado.");
                    stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                    alerta.showAndWait();
                }
            }
        } else {
            Alerta alert = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"),
                    Internacionalizacion.get("aler.pedi.seleccionaritem"));
            Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
            stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alert.showAndWait();
        }
        this.precioValidado = false;
        this.descuentoValidado = false;
    }

    /**
     * Elimina el producto seleccionado de la comanda.
     * @param event
     * @throws AplicacionExcepcion
     */
    @FXML
    void eliminarLinea(ActionEvent event) throws AplicacionExcepcion {
        if (!this.permitirAgregar) {
            if (this.tbListaProductos.getSelectionModel().getSelectedItem() != null) {
                if (tbListaProductos.getSelectionModel().getSelectedItem().cantidadProperty().isEqualTo(1).getValue()) {
                    this.eliminarLineaProducto(this.tbListaProductos.getSelectionModel().getSelectedItem());
                    this.listadoLineaProductos.remove(this.tbListaProductos.getSelectionModel().getSelectedItem());
                } else {
                    this.tbListaProductos.getSelectionModel().getSelectedItem().setCantidad(
                            this.tbListaProductos.getSelectionModel().getSelectedItem().getCantidad() - 1);
                    this.actualizarLineaProducto(this.tbListaProductos.getSelectionModel().getSelectedItem());

                }
                this.tbListaProductos.refresh();
                this.calcularTotal();
            } else {
                Alerta alert = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                        Internacionalizacion.get("aler.warn.cabecera"),
                        Internacionalizacion.get("aler.pedi.eliminaritem"));
                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alert.showAndWait();
            }
        } else {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"),
                    Internacionalizacion.get("aler.pedi.eliminarestado"));
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + "No permitido la modificación del pedido en este estado");
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Elimina el número introducido por el teclado.
     * @param event
     */
    @FXML
    void eliminarNumeroCalculadora(ActionEvent event) {
        this.lbPrecioCalculadora.textProperty().setValue(Constantes.EMPTY_STRING);
    }

    /**
     * Cambia el estado del pedido y lo envía a cocina.
     * @param event
     */
    @FXML
    void enviarPedido(ActionEvent event) {
        this.pedido.subestadosProperty().setValue(Constantes.ESTADO_ENVIADO);
        PedidoUtilsBD pedidosUtils = null;
        try {
            pedidosUtils = new PedidoUtilsBD();
            pedidosUtils.modificarPedido(pedido);
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (pedidosUtils != null)
                pedidosUtils.cerrarConexion();
        }
        this.comprobarBotones();
    }

    /**
     * Cambia el estado de la comanda a facturado.
     * @param event
     */
    @FXML
    void facturaPedido(ActionEvent event) {
        this.permitirAgregar = true;
        this.pedido.subestadosProperty().setValue(Constantes.ESTADO_FACTURA);
        PedidoUtilsBD pedidosUtils = null;
        try {
            pedidosUtils = new PedidoUtilsBD();
            pedidosUtils.modificarPedido(pedido);
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (pedidosUtils != null)
                pedidosUtils.cerrarConexion();
        }
        this.comprobarBotones();
    }

    /**
     * Recupera los datos para iniciar las visuales de la visual.
     */
    @FXML
    void initialize() {
        this.listadoCategoriasActivas();
        this.crearElementosCategorias();
        this.ctbProducto
                .setCellValueFactory(cellData -> cellData.getValue().productoProperty().getValue().nombreProductoProperty());
        this.ctbCantidad.setCellValueFactory(cellData -> cellData.getValue().cantidadProperty().asString());
        this.ctbPrecio
                .setCellValueFactory(cellData -> Bindings.format("%.2f", cellData.getValue().getPrecioModificado()));
        this.tbListaProductos.setItems(listadoLineaProductos);

        Thread hiloHora = new Thread(() -> {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.FORMATO_FECHA_RELOJ);
            while (true) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    this.logs.getLogs().warning(e.getMessage());
                }
                String hora = simpleDateFormat.format(new Date());
                Platform.runLater(() -> {
                    this.lbHora.textProperty().setValue(hora);
                });
            }
        });
        hiloHora.start();
    }

    /**
     * Retrocede el estado del pedido.
     * @param event
     */
    @FXML
    void retrocederPedido(ActionEvent event) {
        this.pedido.subestadosProperty().setValue(Constantes.ESTADO_ENVIADO);
        this.permitirAgregar = false;
        PedidoUtilsBD pedidosUtils = null;
        try {
            pedidosUtils = new PedidoUtilsBD();
            if (pedidosUtils.modificarPedido(pedido) != 1)
                throw new AplicacionExcepcion();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (pedidosUtils != null)
                pedidosUtils.cerrarConexion();
        }
        this.comprobarBotones();
    }

    /**
     * Comprueba el estado del pedido para activar o desactivar los botones.
     */
    private void comprobarBotones() {
        if (this.pedido.subestadosProperty().isEqualTo(Constantes.ESTADO_ENVIADO).getValue()) {
            this.btFactura.setDisable(false);
            this.btCobrarPedido.setDisable(true);
            this.btEnviarPedido.setDisable(false);
            this.btRetrocederPedido.setDisable(true);
        }
        if (this.pedido.subestadosProperty().isEqualTo(Constantes.ESTADO_INICIADO).getValue()) {
            this.btFactura.setDisable(true);
            this.btCobrarPedido.setDisable(true);
            this.btEnviarPedido.setDisable(false);
            this.btRetrocederPedido.setDisable(true);
        }
        if (this.pedido.subestadosProperty().isEqualTo(Constantes.ESTADO_FACTURA).getValue()) {
            this.permitirAgregar=true;
            this.btFactura.setDisable(true);
            this.btCobrarPedido.setDisable(false);
            this.btEnviarPedido.setDisable(true);
            this.btRetrocederPedido.setDisable(false);
        }
    }

    /**
     * Obtiene las categorías activas.
     */
    private void listadoCategoriasActivas() {
        CategoriasUtilsBD categoriasUtils = null;
        try {
            categoriasUtils = new CategoriasUtilsBD();
            this.listadoCategorias.setAll(categoriasUtils.listadoCategoriasActivas());
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (categoriasUtils != null)
                categoriasUtils.cerrarConexion();
        }
    }

    /**
     * Crea los elementos de las categorías en la visual.
     */
    private void crearElementosCategorias() {
        int columna = 0;
        int fila = 1;
        for (CategoriaModel categoria : this.listadoCategorias) {
            FXMLLoader viewCategoriaItem = new FXMLLoader();
            viewCategoriaItem.setLocation(getClass().getResource(Constantes.FXML_PEDIDO_CATEGORIA));
            try {
                AnchorPane elemento = (AnchorPane) viewCategoriaItem.load();
                ControladorCategoriaPedido controlador = viewCategoriaItem.getController();
                controlador.setCategoria(categoria);
                controlador.iniciarControlador();
                elemento.setOnMouseClicked(event -> {
                    crearElementosProductos(categoria);
                });

                gridCategorias.add(elemento, columna, fila++);
                gridCategorias.setMinWidth(Region.USE_COMPUTED_SIZE);
                gridCategorias.setPrefWidth(Region.USE_COMPUTED_SIZE);
                gridCategorias.setMaxWidth(Region.USE_PREF_SIZE);

                gridCategorias.setMinHeight(Region.USE_COMPUTED_SIZE);
                gridCategorias.setPrefHeight(Region.USE_COMPUTED_SIZE);
                gridCategorias.setMaxHeight(Region.USE_PREF_SIZE);
                gridCategorias.setMargin(elemento, new Insets(10));
            } catch (IOException e) {
                Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error: " + e.getMessage());
                stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            }

        }
    }

    /**
     * Crea los elementos visuales de los productos.
     * @param categoria
     */
    private void crearElementosProductos(CategoriaModel categoria) {
        this.listadoProductosCategoria(categoria);
        int columna = 0;
        int fila = 1;
        for (ProductoModel producto : this.listadoProductos) {
            if (columna == 3) {
                columna = 0;
                fila++;
            }
            FXMLLoader viewProductoItem = new FXMLLoader();
            viewProductoItem.setLocation(getClass().getResource(Constantes.FXML_PEDIDO_PRODUCTO));
            try {
                AnchorPane elemento = (AnchorPane) viewProductoItem.load();
                elemento.setOnMouseClicked(event -> {
                    this.agregarProductoAlListado(producto);
                });
                ControladorProductoPedido controlador = viewProductoItem.getController();
                controlador.setProducto(producto);
                controlador.iniciarControlador();

                this.gridProductos.add(elemento, columna++, fila);
                gridProductos.setMinWidth(Region.USE_COMPUTED_SIZE);
                gridProductos.setPrefWidth(Region.USE_COMPUTED_SIZE);
                gridProductos.setMaxWidth(Region.USE_PREF_SIZE);

                gridProductos.setMinHeight(Region.USE_COMPUTED_SIZE);
                gridProductos.setPrefHeight(Region.USE_COMPUTED_SIZE);
                gridProductos.setMaxHeight(Region.USE_PREF_SIZE);
                gridProductos.setMargin(elemento, new Insets(20));
            } catch (IOException e) {
                Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error: " + e.getMessage());
                stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            }

        }
    }

    /**
     * Agrega el producto seleccioando a la comanda.
     * @param producto
     */
    private void agregarProductoAlListado(ProductoModel producto) {
        if (!this.permitirAgregar) {
            this.existeProducto = false;
            ProductoLineaModel lineaProducto = new ProductoLineaModel();
            lineaProducto.setProducto(producto);
            lineaProducto.setCantidad(1);
            lineaProducto.setPrecioModificado(producto.getPrecioProducto());
            lineaProducto.setPedido(this.pedido);
            this.listadoLineaProductos.forEach(linea -> {
                if (linea.productoProperty().getValue().idProductoProperty().isEqualTo(producto.getIdProducto()).getValue()) {
                    linea.setCantidad(linea.cantidadProperty().getValue() + 1);
                    this.actualizarLineaProducto(linea);
                    this.existeProducto = true;
                }
            });
            if (!this.existeProducto) {
                this.listadoLineaProductos.add(lineaProducto);
                this.agregarProductoLineaProducto(lineaProducto);
            }
            this.tbListaProductos.refresh();
            this.calcularTotal();
        } else {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"),
                    Internacionalizacion.get("aler.pedi.productosestado"));
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + "Producto con factura impresa, no se puede editar.");
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Obtiene las categoráis de la BBDD.
     * @param categoria
     */
    private void listadoProductosCategoria(CategoriaModel categoria) {
        ProductoUtilsBD productosUtils = null;
        try {
            productosUtils = new ProductoUtilsBD();
            this.listadoProductos.clear();
            this.gridProductos.getChildren().clear();
            this.listadoProductos.setAll(productosUtils.listadoProductosPorCategoria(categoria));
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (productosUtils != null)
                productosUtils.cerrarConexion();
        }
    }

    /**
     * Calcula el precio total de la comanda.
     */
    private void calcularTotal() {
        this.totalPrecio = 0;
        this.listadoLineaProductos.forEach(producto -> {
            totalPrecio += producto.precioModificadoProperty().getValue() * producto.cantidadProperty().getValue();
        });

        this.lbTotal.setText(String.format("%.2f", totalPrecio));
    }

    /**
     * Setter Pedido.
     * @param pedido
     */
    public void setPedido(PedidoModel pedido) {
        this.pedido = pedido;
        this.lbIdPedido.textProperty().bindBidirectional(this.pedido.idPedidoProperty(), new NumberStringConverter());
        this.lbEstadoPedido.textProperty().bindBidirectional(this.pedido.subestadosProperty());
        this.lbLugarPedido.textProperty().bindBidirectional(this.pedido.sitioPedidoProperty());
        this.lbNumeroPedido.textProperty().bind(Bindings.when(this.pedido.numeroSitioProperty().isNotEqualTo(0))
                .then(this.pedido.numeroSitioProperty().asString()).otherwise(new SimpleStringProperty("")));
        if (this.pedido.subestadosProperty().isEqualTo(Constantes.ESTADO_FACTURA).getValue())
            this.btEnviarPedido.setDisable(true);
        this.comprobarBotones();
    }

    /**
     * Inicia el controlador.
     */
    public void iniciarControlador() {
        this.refrescarListadoProductos();
        this.calcularTotal();
    }

    /**
     * Obtiene los productos de BBDD.
     */
    private void refrescarListadoProductos() {
        ProductosLineaUtilsBD productosLinea = null;
        try {
            productosLinea = new ProductosLineaUtilsBD();
            this.listadoLineaProductos.setAll(productosLinea.obtenerProductosPedido(pedido));
            this.tbListaProductos.refresh();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (productosLinea != null)
                productosLinea.cerrarConexion();
        }
    }

    /**
     * Elimina el producto seleccionado de BBDD.
     * @param lineaProducto
     */
    private void eliminarLineaProducto(ProductoLineaModel lineaProducto) {
        ProductosLineaUtilsBD productosLinea = null;
        try {
            productosLinea = new ProductosLineaUtilsBD();
            productosLinea.eliminarProductoLinea(lineaProducto);
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (productosLinea != null)
                productosLinea.cerrarConexion();
        }


    }

    /**
     * Obtiene el listado de productos de la comanda.
     * @param lineaProducto
     */
    private void actualizarLineaProducto(ProductoLineaModel lineaProducto) {
        ProductosLineaUtilsBD productosLinea = null;
        try {
            productosLinea = new ProductosLineaUtilsBD();
            productosLinea.cambiarCantidadProductoLinea(lineaProducto);
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (productosLinea != null)
                productosLinea.cerrarConexion();
        }
    }

    /**
     * Agrega un producto a la BBDD.
     * @param lineaProducto
     */
    private void agregarProductoLineaProducto(ProductoLineaModel lineaProducto) {
        ProductosLineaUtilsBD productosLinea = null;
        try {
            productosLinea = new ProductosLineaUtilsBD();
            productosLinea.agregarProductoLinea(lineaProducto);
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (productosLinea != null)
                productosLinea.cerrarConexion();
        }
    }

    /**
     * Actualiza el precio de un producto en BBDD.
     * @param lineaProducto
     */
    private void actualizarPrecioProducto(ProductoLineaModel lineaProducto) {
        ProductosLineaUtilsBD productosLinea = null;
        try {
            productosLinea = new ProductosLineaUtilsBD();
            productosLinea.cambiarPrecioProductoLinea(lineaProducto);
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (productosLinea != null)
                productosLinea.cerrarConexion();
        }
    }

    /**
     * Setter logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }
}
