package com.proyectotpv.proyectotpv.controladores.pedido.categoria;

import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class ControladorCategoriaPedido {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lbNombreCategoria;

    private CategoriaModel categoria;

    @FXML
    void initialize() {


    }

    public void setCategoria(CategoriaModel categoria) {
        this.categoria = categoria;
    }

    public void iniciarControlador() {
        this.lbNombreCategoria.textProperty().bindBidirectional(categoria.nombreCategoriaProperty());
    }
}
