package com.proyectotpv.proyectotpv.controladores.main.clientes;

import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.modelos.pedido.ClienteModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.cliente.ClientesUtilsBD;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ControladorNuevoCliente {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btCancelar;

    @FXML
    private Button btCrearCliente;

    @FXML
    private TextField tfCiudadCliente;

    @FXML
    private TextField tfCodigoPostalCliente;

    @FXML
    private TextArea tfDireccionCliente;

    @FXML
    private TextField tfLetraPuertaCliente;

    @FXML
    private TextField tfNumeroPuertaCliente;

    @FXML
    private TextField tfNumeroTelefonoCliente;

    @FXML
    private TextField tfProvinciaCliente;

    public Logs logs;

    private UsuarioModel usuarioSesion;

    private ClienteModel clienteNuevo;

    /**
     * Función del botón cancelar.
     * @param event
     */
    @FXML
    void cancelar(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Función para crear un cliente con los datos introducidos.
     * @param event
     */
    @FXML
    void crearCliente(ActionEvent event) {
        boolean exito = false;
        ClientesUtilsBD clientesUtils = null;
        try {
            this.comprobaciones();
            clientesUtils = new ClientesUtilsBD();
            clientesUtils.nuevoCliente(clienteNuevo);
            exito = true;
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (clientesUtils != null)
                clientesUtils.cerrarConexion();
            if (exito)
                ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
        }
    }

    /**
     * Inicia los textfields y el binding bidireccional.
     */
    @FXML
    void initialize() {
        this.clienteNuevo = new ClienteModel();
        this.tfNumeroTelefonoCliente.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*"))
                    tfNumeroTelefonoCliente.setText(newValue.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING));
            }
        });

        this.tfCodigoPostalCliente.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*"))
                    tfCodigoPostalCliente.setText(newValue.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING));
            }
        });

        this.tfNumeroPuertaCliente.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*"))
                    tfNumeroPuertaCliente.setText(newValue.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING));
            }
        });

        StringConverter<Number> converter = new StringConverter<Number>() {

            @Override
            public String toString(Number object) {
                return object == null ? Constantes.EMPTY_STRING : object.toString();
            }

            @Override
            public Number fromString(String string) {
                if (string == null) {
                    return 0;
                } else {
                    if (string.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING).isEmpty())
                        return 0;
                    else
                        return Integer.parseInt(string.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING));
                }
            }
        };
        this.tfNumeroTelefonoCliente.textProperty().bindBidirectional(this.clienteNuevo.numeroTelefonoProperty(), converter);
        this.tfProvinciaCliente.textProperty().bindBidirectional(this.clienteNuevo.provinciaProperty());
        this.tfCiudadCliente.textProperty().bindBidirectional(this.clienteNuevo.ciudadProperty());
        this.tfCodigoPostalCliente.textProperty().bindBidirectional(this.clienteNuevo.codigoPostalProperty(), converter);
        this.tfDireccionCliente.textProperty().bindBidirectional(this.clienteNuevo.direccionClienteProperty());
        this.tfNumeroPuertaCliente.textProperty().bindBidirectional(this.clienteNuevo.numeroPuertaClienteProperty(),
                converter);
        this.tfLetraPuertaCliente.textProperty().bindBidirectional(this.clienteNuevo.letraPuertaClienteProperty());
    }

    /**
     * Setter Usuario sesion
     * @param usuarioSesion
     */
    public void setUsuarioSesion(UsuarioModel usuarioSesion) {
        this.usuarioSesion = usuarioSesion;
        this.clienteNuevo.setIdUsuarioCreacion(this.usuarioSesion.getIdUsuario().get());
    }

    /**
     * Realiza las comprobaciones de los datos introducidos.
     * @throws AplicacionExcepcion
     */
    private void comprobaciones() throws AplicacionExcepcion {
        if (this.clienteNuevo.numeroTelefonoProperty().asString().isEmpty().getValue()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.numero"));
        }
        if (this.clienteNuevo.getProvincia().isEmpty()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.provincia"));
        }
        if (this.clienteNuevo.getCiudad().isEmpty()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.ciudad"));
        }
        if (this.clienteNuevo.getDireccionCliente().isEmpty()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.direccion"));
        }
        if (this.clienteNuevo.numeroPuertaClienteProperty().asString().isEmpty().getValue()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.numeropuerta"));
        }
        if (this.clienteNuevo.numeroTelefonoProperty().getValue().toString().length() != 9) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.numerolongitud"));
        }
        if (this.clienteNuevo.provinciaProperty().getValueSafe().length() >= 100) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.provincialongitud"));
        }
        if (this.clienteNuevo.ciudadProperty().getValueSafe().length() >= 100) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.ciudadlongitud"));
        }
        if (this.clienteNuevo.codigoPostalProperty().getValue().toString().length() > 6) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.codigopostallongitud"));
        }
        if (this.clienteNuevo.direccionClienteProperty().getValueSafe().length() >= 200) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.direccionlongitud"));
        }
        if (this.clienteNuevo.numeroPuertaClienteProperty().asString().getValueSafe().length() > 5) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.numeropuertalongitud"));
        }
        if (this.clienteNuevo.letraPuertaClienteProperty().getValueSafe().length() > 10) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.letrapuertacliente"));
        }
    }

    /**
     * Setter logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }
}
