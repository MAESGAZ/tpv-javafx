package com.proyectotpv.proyectotpv.controladores.main.clientes;

import com.proyectotpv.proyectotpv.modelos.pedido.ClienteModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.cliente.ClientesUtilsBD;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.net.URL;
import java.sql.SQLException;
import java.util.Objects;
import java.util.ResourceBundle;

public class ControladorEditarCliente {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btCancelar;

    @FXML
    private Button btEditarCliente;

    @FXML
    private TextField tfCiudadCliente;

    @FXML
    private TextField tfCodigoPostalCliente;

    @FXML
    private TextArea tfDireccionCliente;

    @FXML
    private TextField tfLetraPuertaCliente;

    @FXML
    private TextField tfNumeroPuertaCliente;

    @FXML
    private TextField tfNumeroTelefonoCliente;

    @FXML
    private TextField tfProvinciaCliente;

    private ClienteModel clienteOriginal;

    private ClienteModel cliente;

    private Logs logs;

    /**
     * Función del botón cancelar.
     * @param event
     */
    @FXML
    void cancelar(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Función para aplicar los cambios al cliente.
     * @param event
     */
    @FXML
    void editarCliente(ActionEvent event) {
        boolean exito = false;
        ClientesUtilsBD clientesUtils = null;
        try {
            this.comprobaciones();
            clientesUtils = new ClientesUtilsBD();
            clientesUtils.modificarCliente(cliente);
            exito = true;
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            alerta.showAndWait();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            alerta.showAndWait();
        } finally {
            if (clientesUtils != null)
                clientesUtils.cerrarConexion();
            if (exito)
                ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
        }
    }

    /**
     * Inicializa los textfield en pantalla
     */
    @FXML
    void initialize() {
        this.tfNumeroTelefonoCliente.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*"))
                    tfNumeroTelefonoCliente
                            .setText(newValue.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING));
            }
        });
        this.tfCodigoPostalCliente.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*"))
                    tfCodigoPostalCliente
                            .setText(newValue.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING));
            }
        });
        this.tfNumeroPuertaCliente.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*"))
                    tfNumeroPuertaCliente
                            .setText(newValue.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING));
            }
        });
    }

    /**
     * Setter Cliente
     * @param cliente
     */
    public void setCliente(ClienteModel cliente) {
        this.clienteOriginal = cliente;
        this.cliente = cliente.clone();
    }

    /**
     * Inicia el controlador, el converter numérico y el binding bidireccional.
     */
    public void iniciarControlador() {
        StringConverter<Number> converter = new StringConverter<Number>() {

            @Override
            public String toString(Number object) {
                return object == null ? Constantes.EMPTY_STRING : object.toString();
            }

            @Override
            public Number fromString(String string) {
                if (string == null) {
                    return 0;
                } else {
                    if (string.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING).isEmpty())
                        return 0;
                    else
                        return Integer.parseInt(string.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING));
                }
            }
        };
        this.tfNumeroTelefonoCliente.textProperty().bindBidirectional(this.cliente.numeroTelefonoProperty(), converter);
        this.tfProvinciaCliente.textProperty().bindBidirectional(cliente.provinciaProperty());
        this.tfCiudadCliente.textProperty().bindBidirectional(cliente.ciudadProperty());
        this.tfCodigoPostalCliente.textProperty().bindBidirectional(cliente.codigoPostalProperty(), converter);
        this.tfDireccionCliente.textProperty().bindBidirectional(cliente.direccionClienteProperty());
        this.tfNumeroPuertaCliente.textProperty().bindBidirectional(cliente.numeroPuertaClienteProperty(), converter);
        this.tfLetraPuertaCliente.textProperty().bindBidirectional(cliente.letraPuertaClienteProperty());
    }

    /**
     * Realiza las comprobaciones de los campos.
     * @throws AplicacionExcepcion
     */
    private void comprobaciones() throws AplicacionExcepcion {
        if (this.cliente.numeroTelefonoProperty().asString().isEmpty().getValue()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.numero"));
        }
        if (this.cliente.getProvincia().isEmpty()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.provincia"));
        }
        if (this.cliente.getCiudad().isEmpty()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.ciudad"));
        }
        if (this.cliente.getDireccionCliente().isEmpty()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.direccion"));
        }
        if (this.cliente.numeroPuertaClienteProperty().asString().isEmpty().getValue()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.numeropuerta"));
        }
        if (this.cliente.numeroTelefonoProperty().getValue().toString().length() != 9) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.numerolongitud"));
        }
        if (this.cliente.getProvincia().length() >= 100) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.provincialongitud"));
        }
        if (this.cliente.getCiudad().length() >= 100) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.ciudadlongitud"));
        }
        if (this.cliente.codigoPostalProperty().asString().getValueSafe().length() > 6) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.codigopostallongitud"));
        }
        if (this.cliente.direccionClienteProperty().getValueSafe().length() >= 200) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.direccionlongitud"));
        }
        if (this.cliente.numeroPuertaClienteProperty().asString().getValueSafe().length() > 5) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.numeropuertalongitud"));
        }
        if (this.cliente.letraPuertaClienteProperty().getValueSafe().length() > 10) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.clie.letrapuertacliente"));
        }
    }

    /**
     * Setter Logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }
}
