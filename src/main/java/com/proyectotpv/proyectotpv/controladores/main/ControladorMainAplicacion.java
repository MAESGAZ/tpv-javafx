package com.proyectotpv.proyectotpv.controladores.main;

import com.proyectotpv.proyectotpv.controladores.logininicio.ControladorLoginInicio;
import com.proyectotpv.proyectotpv.controladores.main.categorias.ControladorViewCategorias;
import com.proyectotpv.proyectotpv.controladores.main.clientes.ControladorClientes;
import com.proyectotpv.proyectotpv.controladores.main.distribucion.ControladorDistribucionSala;
import com.proyectotpv.proyectotpv.controladores.main.producto.ControladorProductos;
import com.proyectotpv.proyectotpv.controladores.main.usuario.ControladorUsuarios;
import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.modelos.main.PaisModel;
import com.proyectotpv.proyectotpv.recursos.Alerta;
import com.proyectotpv.proyectotpv.recursos.Constantes;
import com.proyectotpv.proyectotpv.recursos.Internacionalizacion;
import com.proyectotpv.proyectotpv.recursos.Logs;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.ResourceBundle;

public class ControladorMainAplicacion {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btCategorias;

    @FXML
    private Button btCerrarSesion;

    @FXML
    private Button btClientes;

    @FXML
    private Button btDistriSala;

    @FXML
    private Button btInformes;

    @FXML
    private Button btModificarUsuario;

    @FXML
    private Button btProductos;

    @FXML
    private Button btUsuarios;

    @FXML
    private BorderPane pagina;

    @FXML
    private Label lbReloj;

    @FXML
    private ScrollPane centro;

    @FXML
    private Label lbNombreSesion;

    @FXML
    private ChoiceBox<PaisModel> cbIdiomas;

    @FXML
    private Label lbUsuarioSesion;

    @FXML
    private Label tfIdioma;

    @FXML
    private Label tfNombre;

    @FXML
    private Label tfTitulo;

    @FXML
    private Label tfUsuario;

    private UsuarioModel usuarioSesion = new UsuarioModel();

    private ObservableList<PaisModel> listadoIdiomas;

    private StringProperty hora = new SimpleStringProperty();

    private Logs logs;

    private boolean romperReloj = false;

    /**
     * Cierra la sesión. Cierra la actual visual y abre la de inicio login.
     * @param event Evento del botón.
     */
    @FXML
    void cerrarSesion(ActionEvent event) {
        this.logs.getLogs().info("Cerramos sesión del usuario " + this.usuarioSesion.getUsuario().getValueSafe());
        romperReloj = true;
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(Constantes.FXML_LOGIN_INICIO));
        loader.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION,
                Internacionalizacion.getDefaultLocale()));
        try {
            stage.setScene(new Scene((AnchorPane) loader.load()));
            ControladorLoginInicio controllerLogin = loader.getController();
            controllerLogin.setLogs(logs);
            stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            stage.setResizable(false);
            stage.setTitle(Internacionalizacion.get("title"));
            stage.show();
        } catch (IOException e) {
            this.logs.getLogs().severe("Error: " + e.getMessage());
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
            stageAlerta.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            alerta.showAndWait();
        }

    }

    /**
     * Abre la visual para modificar el usuario de la sesión.
     * @param event Evento del botón.
     */
    @FXML
    void modificarUsuario(ActionEvent event) {
        FXMLLoader viewModificarUsuario = new FXMLLoader();
        viewModificarUsuario.setLocation(getClass().getResource(Constantes.FXML_MAIN_MODIFICAR_USUARIO));
        viewModificarUsuario.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        Stage stageModificarUsuario = new Stage();
        stageModificarUsuario.setTitle(Internacionalizacion.get("main.modi.titulo"));
        stageModificarUsuario.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
        try {
            stageModificarUsuario.setScene(new Scene((AnchorPane) viewModificarUsuario.load()));
            ControladorMainModificarUsuario controlador = viewModificarUsuario.getController();
            controlador.setLogs(logs);
            controlador.setUsuario(usuarioSesion);
            controlador.iniciarControlador();
            stageModificarUsuario.showAndWait();
            this.usuarioSesion=controlador.getUsuarioSesion();
            this.iniciarControlador();
        } catch (IOException e) {
            this.logs.getLogs().severe("Error: " + e.getMessage());
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Muestra la visual de categorías en el centro de la pantalla.
     * @param event
     */
    @FXML
    void mostrarCategorias(ActionEvent event) {
        FXMLLoader viewCategorias = new FXMLLoader();
        viewCategorias.setLocation(getClass().getResource(Constantes.FXML_CATEGORIAS));
        viewCategorias.setResources(
                ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));

        try {
            Parent elemento = (Parent) viewCategorias.load();
            ControladorViewCategorias controlador = viewCategorias.getController();
            controlador.setUsuarioSesion(usuarioSesion);
            controlador.setLogs(logs);
            this.centro.setContent(elemento);
            elemento.autosize();
            this.centro.setFitToHeight(true);
            this.centro.setFitToWidth(true);
        } catch (IOException e) {
            this.logs.getLogs().severe("Error: " + e.getMessage());
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Muestra la visual de clientes en la parte central de la aplicación.
     * @param event
     */
    @FXML
    void mostrarClientes(ActionEvent event) {
        FXMLLoader viewClientes = new FXMLLoader();
        viewClientes.setLocation(getClass().getResource(Constantes.FXML_CLIENTES));
        viewClientes.setResources(
                ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            Parent elemento = (Parent) viewClientes.load();
            ControladorClientes controlador = viewClientes.getController();
            controlador.setUsuarioSesion(usuarioSesion);
            controlador.setLogs(logs);
            this.centro.setContent(elemento);
            elemento.autosize();
            this.centro.setFitToHeight(true);
            this.centro.setFitToHeight(true);
        } catch (IOException e) {
            this.logs.getLogs().severe("Error: " + e.getMessage());
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Muestra la distribución de la sala en la parte central de la visual actual.
     * @param event
     */
    @FXML
    void mostrarDistribucionSala(ActionEvent event) {
        FXMLLoader viewDistribucionSala = new FXMLLoader();
        viewDistribucionSala.setLocation(getClass().getResource(Constantes.FXML_DISTRIBUCION_SALA));
        viewDistribucionSala.setResources(
                ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            Parent elemento = (Parent) viewDistribucionSala.load();
            ControladorDistribucionSala controlador = viewDistribucionSala.getController();
            controlador.setUsuarioSesion(usuarioSesion);
            controlador.setLogs(logs);
            controlador.iniciarControlador();
            centro.setContent(elemento);
            centro.setFitToWidth(true);
            centro.setFitToHeight(true);
            elemento.autosize();
        } catch (IOException e) {
            this.logs.getLogs().severe("Error: " + e.getMessage());
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Muestra la visual de informes en la parte central de la actual visual.
     * @param event
     */
    @FXML
    void mostrarInformes(ActionEvent event) {

    }

    /**
     * Muestra los productos en la parte central de la actual visual.
     * @param event
     */
    @FXML
    void mostrarProductos(ActionEvent event) {
        FXMLLoader viewProductos = new FXMLLoader();
        viewProductos.setLocation(getClass().getResource(Constantes.FXML_PRODUCTOS));
        viewProductos.setResources(
                ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            Parent elemento = (Parent) viewProductos.load();
            ControladorProductos controlador = viewProductos.getController();
            controlador.setUsuarioSesion(usuarioSesion);
            controlador.setLogs(logs);
            this.centro.setContent(elemento);
            elemento.autosize();
            this.centro.setFitToHeight(true);
            this.centro.setFitToWidth(true);
        } catch (IOException e) {
            this.logs.getLogs().severe("Error: " + e.getMessage());
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }

    }

    /**
     * Muestra la visual de Usuarios en la ventana principal de la aplicación.
     * @param event
     */
    @FXML
    void mostrarUsuarios(ActionEvent event) {
        FXMLLoader viewUsuarios = new FXMLLoader();
        viewUsuarios.setLocation(getClass().getResource(Constantes.FXML_USUARIOS));
        viewUsuarios.setResources(
                ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));

        try {
            Parent elemento = (Parent) viewUsuarios.load();
            ControladorUsuarios controlador = viewUsuarios.getController();
            controlador.setLogs(logs);
            this.centro.setContent(elemento);
            elemento.autosize();
            this.centro.setFitToHeight(true);
            this.centro.setFitToWidth(true);
        } catch (IOException e) {
            this.logs.getLogs().severe("Error: " + e.getMessage());
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Iniciamos los binding y atributos de la clase.
     */
    @FXML
    void initialize() {
        //Iniciamos los binding de internacionalizacion y el desplegable
        this.internacionalizacion();
        this.listadoIdiomas = FXCollections.observableArrayList();
        this.listadoIdiomas.add(new PaisModel(Constantes.CODIGO_IDIOMA_ESP, Constantes.CODIGO_REGION_ESP, Constantes.TEXTO_IDIOMA_ESP));
        this.listadoIdiomas.add(new PaisModel(Constantes.CODIGO_IDIOMA_ENG, Constantes.CODIGO_REGION_ENG, Constantes.TEXTO_IDIOMA_ENG));
        this.cbIdiomas.setItems(listadoIdiomas);
        this.cbIdiomas.getSelectionModel().selectFirst();

        this.cbIdiomas.valueProperty().addListener(new ChangeListener<PaisModel>() {

            @Override
            public void changed(ObservableValue<? extends PaisModel> observable, PaisModel oldValue,
                                PaisModel newValue) {
                Internacionalizacion.setLocale(newValue.getLocale());

            }
        });

        // Iniciamos el hilo del reloj.
        this.lbReloj.textProperty().bindBidirectional(hora);

        Thread hiloHora = new Thread(() -> {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.FORMATO_FECHA_RELOJ);
            while (!romperReloj) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    this.logs.getLogs().warning(e.getMessage());
                }
                String hora = simpleDateFormat.format(new Date());
                Platform.runLater(() -> {
                    this.hora.setValue(hora);
                });
            }
        });
        hiloHora.start();
    }

    /**
     * Setter Usuario Sesion
     * @param usuarioSesion
     */
    public void setUsuarioSesion(UsuarioModel usuarioSesion) {
        this.usuarioSesion = usuarioSesion;
        this.comprobarRol();
    }

    /**
     * Inicializamos los binding del controlador.
     */
    public void iniciarControlador() {
        this.lbNombreSesion.textProperty().bindBidirectional(this.usuarioSesion.getNombreUsuario());
        this.lbUsuarioSesion.textProperty().bindBidirectional(this.usuarioSesion.getUsuario());
    }

    /**
     * Setter de los logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }

    /**
     * Binding de la internacionalización de la visual.
     */
    private void internacionalizacion() {
        Internacionalizacion.localeProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.btDistriSala.textProperty().bind(Internacionalizacion.createStringBinding("main.distrisala"));
                this.btProductos.textProperty().bind(Internacionalizacion.createStringBinding("main.producto"));
                this.btCategorias.textProperty().bind(Internacionalizacion.createStringBinding("main.categoria"));
                this.btUsuarios.textProperty().bind(Internacionalizacion.createStringBinding("main.usuario"));
                this.btClientes.textProperty().bind(Internacionalizacion.createStringBinding("main.cliente"));
                this.btModificarUsuario.textProperty().bind(Internacionalizacion.createStringBinding("main.modificar"));
                this.btCerrarSesion.textProperty().bind(Internacionalizacion.createStringBinding("main.cerrar"));
                this.tfNombre.textProperty().bind(Internacionalizacion.createStringBinding("main.nombre"));
                this.tfUsuario.textProperty().bind(Internacionalizacion.createStringBinding("main.usuariosesion"));
                this.tfIdioma.textProperty().bind(Internacionalizacion.createStringBinding("main.idioma"));
                this.btInformes.textProperty().bind(Internacionalizacion.createStringBinding("main.informes"));
            }
        });
    }

    /**
     * Comprobación del rol del usuario para activar o desactivar los botones.
     */
    private void comprobarRol() {
        if (this.usuarioSesion.getRolUsuario().getValue().getIdRol().isEqualTo(Constantes.ID_ROL_CAMARERO).getValue()) {
            this.btProductos.setDisable(true);
            this.btCategorias.setDisable(true);
            this.btUsuarios.setDisable(true);
        }
        if (this.usuarioSesion.getRolUsuario().getValue().getIdRol().isEqualTo(Constantes.ID_ROL_GERENTE).getValue()) {
            this.btUsuarios.setDisable(true);
        }
        if (this.usuarioSesion.getRolUsuario().getValue().getIdRol().isEqualTo(Constantes.ID_ROL_ADMINISTRADOR).getValue()) {
            this.btProductos.setDisable(false);
            this.btCategorias.setDisable(false);
            this.btUsuarios.setDisable(false);
            this.btClientes.setDisable(false);
        }
    }
}
