package com.proyectotpv.proyectotpv.controladores.main.distribucion;

import com.proyectotpv.proyectotpv.controladores.main.clientes.ControladorBuscarCliente;
import com.proyectotpv.proyectotpv.controladores.main.distribucion.barraitem.ControladorBarraItem;
import com.proyectotpv.proyectotpv.controladores.main.distribucion.mesaitem.ControladorMesaItem;
import com.proyectotpv.proyectotpv.controladores.main.distribucion.terrazaitem.ControladorTerrazaItem;
import com.proyectotpv.proyectotpv.controladores.pedido.ControladorPedido;
import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.modelos.pedido.ClienteModel;
import com.proyectotpv.proyectotpv.modelos.pedido.PedidoDomicilioModel;
import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import com.proyectotpv.proyectotpv.recursos.Alerta;
import com.proyectotpv.proyectotpv.recursos.Constantes;
import com.proyectotpv.proyectotpv.recursos.Internacionalizacion;
import com.proyectotpv.proyectotpv.recursos.Logs;
import com.proyectotpv.proyectotpv.utiles.basedatos.pedido.PedidoUtilsBD;
import com.proyectotpv.proyectotpv.utiles.basedatos.pedido.PedidosDomicilioUtilsBD;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class ControladorDistribucionSala {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btMostrarPedido;

    @FXML
    private Button btNuevoPedido;

    @FXML
    private GridPane gridItems;

    @FXML
    private GridPane gridItemsBarra;

    @FXML
    private GridPane gridItemsTerraza;

    @FXML
    private Tab tbBarra;

    @FXML
    private Tab tbComedor;

    @FXML
    private Tab tbDomicilio;

    @FXML
    private Tab tbTerraza;

    @FXML
    private TableView<PedidoDomicilioModel> tbPedidosDomicilio;

    @FXML
    private TableColumn<PedidoDomicilioModel, String> tcCiudad;

    @FXML
    private TableColumn<PedidoDomicilioModel, String> tcDireccion;

    @FXML
    private TableColumn<PedidoDomicilioModel, String> tcFechaCreacion;

    @FXML
    private TableColumn<PedidoDomicilioModel, String> tcLetraPuerta;

    @FXML
    private TableColumn<PedidoDomicilioModel, Number> tcNumeroPedido;

    @FXML
    private TableColumn<PedidoDomicilioModel, Number> tcNumeroPuerta;

    @FXML
    private TableColumn<PedidoDomicilioModel, Number> tcNumeroTelefono;

    @FXML
    private TableColumn<PedidoDomicilioModel, String> tcProvincia;

    private UsuarioModel usuarioSesion;

    private Logs logs;

    private ObservableList<PedidoModel> listadoPedidos;
    private ObservableList<PedidoModel> listadoPedidosSalon;
    private ObservableList<PedidoModel> listadoPedidosTerraza;
    private ObservableList<PedidoModel> listadoPedidosBarra;
    private ObservableList<PedidoDomicilioModel> listadoPedidosDomicilio;

    /**
     * Muestra el pedido a domicilio seleccionado en la visual de pedidos.
     * @param event
     */
    @FXML
    void mostrarPedido(ActionEvent event) {
        Stage stage = new Stage();
        FXMLLoader viewPedido = new FXMLLoader();
        stage.setTitle(Internacionalizacion.get("pedi.titulo"));
        stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
        viewPedido.setLocation(getClass().getResource(Constantes.FXML_PEDIDO));
        viewPedido.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            stage.setScene(new Scene((ScrollPane) viewPedido.load()));
            ControladorPedido controladorPedido = viewPedido.getController();
            controladorPedido
                    .setPedido(this.tbPedidosDomicilio.getSelectionModel().getSelectedItem().getPedido());
            controladorPedido.setLogs(logs);
            controladorPedido.iniciarControlador();
            stage.showAndWait();
            this.refrescarInterfaz();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Crea un nuevo pedido a domicilio.
     * @param event
     */
    @FXML
    void nuevoPedido(ActionEvent event) {
        FXMLLoader viewBuscarCliente = new FXMLLoader();
        viewBuscarCliente.setLocation(getClass().getResource(Constantes.FXML_BUSCAR_CLIENTE));
        viewBuscarCliente.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION,
                Internacionalizacion.getDefaultLocale()));
        Stage stage = new Stage();
        stage.setTitle(Internacionalizacion.get("clien.busc.titulo"));
        stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
        try {
            stage.setScene(new Scene((AnchorPane) viewBuscarCliente.load()));
            ControladorBuscarCliente controller = viewBuscarCliente.getController();
            controller.setUsuarioSesion(usuarioSesion);
            controller.setLogs(logs);
            stage.showAndWait();
            ClienteModel cliente = controller.getPedido().getClientePedido();
            PedidoDomicilioModel pedido = this.obtenerPedidoDomicilio(cliente);
            if (pedido.getPedido().idPedidoProperty().isNotEqualTo(0).getValue()) {
                FXMLLoader viewPedido = new FXMLLoader();
                viewPedido.setLocation(getClass().getResource(Constantes.FXML_PEDIDO));
                viewPedido.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
                stage.setScene(new Scene((ScrollPane) viewPedido.load()));
                ControladorPedido controlador = viewPedido.getController();
                controlador.setLogs(logs);
                controlador.setPedido(pedido.getPedido());
                controlador.iniciarControlador();
                stage.showAndWait();
            }
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();

        }
        this.refrescarInterfaz();
    }

    /**
     * Inicia la internacionalización, los atributos privados y los elementos de la tabla.
     */
    @FXML
    void initialize() {
        this.internacionalizacion();
        this.btMostrarPedido.setDisable(true);
        this.listadoPedidos = FXCollections.observableArrayList();
        this.listadoPedidosSalon = FXCollections.observableArrayList();
        this.listadoPedidosTerraza = FXCollections.observableArrayList();
        this.listadoPedidosBarra = FXCollections.observableArrayList();
        this.listadoPedidosDomicilio = FXCollections.observableArrayList();
        this.tbPedidosDomicilio.setItems(listadoPedidosDomicilio);
        this.tcNumeroTelefono.setCellValueFactory(
                cellData -> cellData.getValue().getClientePedido().numeroTelefonoProperty());
        this.tcNumeroPedido
                .setCellValueFactory(cellData -> cellData.getValue().getPedido().idPedidoProperty());
        this.tcFechaCreacion.setCellValueFactory(cellData -> {
            LocalDateTime tiempo = cellData.getValue().getPedido().getFechaCreacion();
            return new SimpleStringProperty(
                    tiempo.getDayOfMonth() + "-" + tiempo.getMonthValue() + "-" + tiempo.getYear());
        });
        this.tcProvincia
                .setCellValueFactory(cellData -> cellData.getValue().getClientePedido().provinciaProperty());
        this.tcCiudad.setCellValueFactory(cellData -> cellData.getValue().getClientePedido().ciudadProperty());
        this.tcDireccion.setCellValueFactory(
                cellData -> cellData.getValue().getClientePedido().direccionClienteProperty());
        this.tcNumeroPuerta.setCellValueFactory(
                cellData -> cellData.getValue().getClientePedido().numeroPuertaClienteProperty());
        this.tcLetraPuerta.setCellValueFactory(cellData -> {
            if (cellData.getValue().getClientePedido().letraPuertaClienteProperty().isNull().getValue())
                return new SimpleStringProperty();
            else
                return cellData.getValue().getClientePedido().letraPuertaClienteProperty();
        });
        this.tbPedidosDomicilio.getSelectionModel().selectedItemProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.btMostrarPedido.setDisable(false);
            } else {
                this.btMostrarPedido.setDisable(true);
            }
        });
        this.obtenerPedidos();
        this.obtenerPedidosDomicilio();
    }

    /**
     * Obtiene los pedidos de BBDD.
     */
    private void obtenerPedidos() {
        PedidoUtilsBD pedidosUtils = null;
        try {
            pedidosUtils = new PedidoUtilsBD();
            this.listadoPedidos.setAll(pedidosUtils.listadoPedidos());
            this.distribuirPedidos();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (pedidosUtils != null)
                pedidosUtils.cerrarConexion();
        }
    }

    /**
     * Distribuye los tipos de pedidos según su sitio.
     */
    private void distribuirPedidos() {
        this.listadoPedidosSalon.clear();
        this.listadoPedidosBarra.clear();
        this.listadoPedidosTerraza.clear();
        for (PedidoModel pedido : this.listadoPedidos) {
            if (pedido.getSitioPedido().equals(Constantes.SALON_PEDIDO))
                this.listadoPedidosSalon.add(pedido);
            if (pedido.getSitioPedido().equals(Constantes.TERRAZA_PEDIDO))
                this.listadoPedidosTerraza.add(pedido);
            if (pedido.getSitioPedido().equals(Constantes.BARRA_PEDIDO))
                this.listadoPedidosBarra.add(pedido);
        }
    }

    /**
     * Crea los items visuales de la terraza.
     */
    private void crearMesasTerraza() {
        int columna = 0;
        int fila = 1;
        for (int i = 0; i < 20; i++) {
            if (columna == 5) {
                columna = 0;
                fila++;
            }
            FXMLLoader viewItemTerraza = new FXMLLoader();
            viewItemTerraza.setLocation(getClass().getResource(Constantes.FXML_TERRAZA_ITEM));
            viewItemTerraza.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
            try {
                AnchorPane elemento = (AnchorPane) viewItemTerraza.load();
                ControladorTerrazaItem controlador = viewItemTerraza.getController();

                // Si existe un pedido asociado a una mesa, sino, lo creamos.
                boolean existePedido = false;
                for (PedidoModel pedido : this.listadoPedidosTerraza) {
                    if (pedido.getNumeroSitio() == i + 1) {
                        controlador.setPedido(pedido);
                        existePedido = true;
                        break;
                    }
                }

                if (!existePedido) {
                    PedidoModel pedidoTerraza = new PedidoModel();
                    pedidoTerraza.setSitioPedido(Constantes.TERRAZA_PEDIDO);
                    pedidoTerraza.setNumeroSitio(i + 1);
                    pedidoTerraza.setIdCliente(Constantes.CLIENTE_FANTASMA);
                    pedidoTerraza.setIdUsuario(this.usuarioSesion.getIdUsuario().getValue());
                    controlador.setPedido(pedidoTerraza);
                }
                // Evento al hacer click para visualizar la pestaña de pedidos.
                elemento.setOnMouseClicked(event -> {
                    //Comprobamos pedido recuperado de BBDD.
                    if (controlador.getPedido().getIdPedido() == 0) {
                        Alert alerta = new Alert(Alert.AlertType.CONFIRMATION,
                                Internacionalizacion.get("aler.dist.crearterraza")
                                        + controlador.getPedido().getNumeroSitio(),
                                ButtonType.YES, ButtonType.CANCEL);
                        Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
                        stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                        alerta.showAndWait();
                        // Si no tiene pedido existente, creamos uno nuevo y abrimos la visual de pedido.
                        if (alerta.getResult() == ButtonType.YES) {
                            this.crearPedido(controlador.getPedido());
                            Stage stage = new Stage();
                            FXMLLoader viewPedido = new FXMLLoader();
                            stage.setTitle(Internacionalizacion.get("pedi.titulo"));
                            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                            viewPedido.setLocation(getClass().getResource(Constantes.FXML_PEDIDO));
                            viewPedido.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
                            try {
                                stage.setScene(new Scene((ScrollPane) viewPedido.load()));
                                ControladorPedido controladorPedido = viewPedido.getController();
                                controladorPedido.setPedido(this.obtenerPedido(controlador.getPedido()));
                                controladorPedido.setLogs(logs);
                                controladorPedido.iniciarControlador();
                                stage.showAndWait();
                                this.refrescarInterfaz();
                            } catch (IOException e) {
                                this.logs.getLogs().severe("Error: " + e.getMessage());
                                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                                alert.showAndWait();
                            } catch (ClassNotFoundException e) {
                                this.logs.getLogs().severe("Error: " + e.getMessage());
                                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                                alert.showAndWait();
                            } catch (SQLException e) {
                                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                                        Internacionalizacion.get("aler.bbdd.cabecera"),
                                        e.getMessage());
                                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                                this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
                                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                                alert.showAndWait();
                            }
                        }
                    } else {
                        // Abrimos la visual de pedido con el pedido existente en BBDD.
                        Stage stage = new Stage();
                        FXMLLoader viewPedido = new FXMLLoader();
                        stage.setTitle(Internacionalizacion.get("pedi.titulo"));
                        stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                        viewPedido.setLocation(getClass().getResource(Constantes.FXML_PEDIDO));
                        viewPedido.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
                        try {
                            stage.setScene(new Scene((ScrollPane) viewPedido.load()));
                            ControladorPedido controladorPedido = viewPedido.getController();
                            controladorPedido.setPedido(controlador.getPedido());
                            controladorPedido.setLogs(logs);
                            controladorPedido.iniciarControlador();
                            stage.showAndWait();
                            this.refrescarInterfaz();
                        } catch (IOException e) {
                            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
                            this.logs.getLogs().severe("Error: " + e.getMessage());
                            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                            alerta.showAndWait();
                        }
                    }
                });

                // Añadimos el elemento visual al gridPane.
                gridItemsTerraza.add(elemento, columna++, fila);
                gridItemsTerraza.setAlignment(Pos.BASELINE_CENTER);
                gridItemsTerraza.setMinWidth(Region.USE_COMPUTED_SIZE);
                gridItemsTerraza.setPrefWidth(Region.USE_COMPUTED_SIZE);
                gridItemsTerraza.setMaxWidth(Region.USE_PREF_SIZE);
                gridItemsTerraza.setMinHeight(Region.USE_COMPUTED_SIZE);
                gridItemsTerraza.setPrefHeight(Region.USE_COMPUTED_SIZE);
                gridItemsTerraza.setMaxHeight(Region.USE_PREF_SIZE);
                gridItemsTerraza.setMargin(elemento, new Insets(20));
            } catch (IOException e) {
                Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error: " + e.getMessage());
                stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            }
        }
    }

    /**
     * Creamos los elementos de la barra.
     */
    private void crearSitiosBarra() {
        int columna = 0;
        int fila = 1;
        for (int i = 0; i < 10; i++) {
            if (columna == 5) {
                columna = 0;
                fila++;
            }
            FXMLLoader viewItemTerraza = new FXMLLoader();
            viewItemTerraza.setLocation(getClass().getResource(Constantes.FXML_BARRA_ITEM));
            viewItemTerraza.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
            try {
                AnchorPane elemento = (AnchorPane) viewItemTerraza.load();
                ControladorBarraItem controlador = viewItemTerraza.getController();

                // Recuperamos pedidos de BBDD.
                boolean existePedido = false;
                for (PedidoModel pedido : this.listadoPedidosBarra) {
                    if (pedido.getNumeroSitio() == i + 1) {
                        controlador.setPedido(pedido);
                        existePedido = true;
                        break;
                    }
                }
                // Si no hay pedido, creamos uno nuevo.
                if (!existePedido) {
                    PedidoModel pedidoTerraza = new PedidoModel();
                    pedidoTerraza.setSitioPedido(Constantes.BARRA_PEDIDO);
                    pedidoTerraza.setNumeroSitio(i + 1);
                    pedidoTerraza.setIdCliente(Constantes.CLIENTE_FANTASMA);
                    pedidoTerraza.setIdUsuario(this.usuarioSesion.getIdUsuario().getValue());
                    controlador.setPedido(pedidoTerraza);
                }
                // Evento del elemento
                elemento.setOnMouseClicked(event -> {
                    if (controlador.getPedido().getIdPedido() == 0) {
                        Alert alerta = new Alert(Alert.AlertType.CONFIRMATION,
                                Internacionalizacion.get("aler.dist.crearbarra")
                                        + controlador.getPedido().getNumeroSitio(),
                                ButtonType.YES, ButtonType.CANCEL);
                        Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
                        stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                        alerta.showAndWait();
                        if (alerta.getResult() == ButtonType.YES) {
                            // Creamos el pedido y abrimos la visual del pedido.
                            this.crearPedido(controlador.getPedido());
                            Stage stage = new Stage();
                            FXMLLoader viewPedido = new FXMLLoader();
                            stage.setTitle(Internacionalizacion.get("pedi.titulo"));
                            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                            viewPedido.setLocation(getClass().getResource(Constantes.FXML_PEDIDO));
                            viewPedido.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
                            try {
                                stage.setScene(new Scene((ScrollPane) viewPedido.load()));
                                ControladorPedido controladorPedido = viewPedido.getController();
                                controladorPedido.setPedido(this.obtenerPedido(controlador.getPedido()));
                                controladorPedido.setLogs(logs);
                                controladorPedido.iniciarControlador();
                                stage.showAndWait();
                                this.refrescarInterfaz();
                            } catch (IOException e) {
                                this.logs.getLogs().severe("Error: " + e.getMessage());
                                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                                alert.showAndWait();
                            } catch (ClassNotFoundException e) {
                                this.logs.getLogs().severe("Error: " + e.getMessage());
                                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                                alert.showAndWait();
                            } catch (SQLException e) {
                                this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
                                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                                        Internacionalizacion.get("aler.bbdd.cabecera"),
                                        e.getMessage());
                                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                                alert.showAndWait();
                            }
                        }
                    } else {
                        //Abrimos la visual con el pedido recuperado de BBDD.
                        Stage stage = new Stage();
                        FXMLLoader viewPedido = new FXMLLoader();
                        stage.setTitle(Internacionalizacion.get("pedi.titulo"));
                        stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                        viewPedido.setLocation(getClass().getResource(Constantes.FXML_PEDIDO));
                        viewPedido.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
                        try {
                            stage.setScene(new Scene((ScrollPane) viewPedido.load()));
                            ControladorPedido controladorPedido = viewPedido.getController();
                            controladorPedido.setPedido(controlador.getPedido());
                            controladorPedido.setLogs(logs);
                            controladorPedido.iniciarControlador();
                            stage.showAndWait();
                            this.refrescarInterfaz();
                        } catch (IOException e) {
                            this.logs.getLogs().severe("Error: " + e.getMessage());
                            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
                            this.logs.getLogs().severe("Error: " + e.getMessage());
                            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                            alerta.showAndWait();
                        }
                    }
                });
                // Agregamos la visual al gridPane.
                gridItemsBarra.add(elemento, columna++, fila);
                gridItemsBarra.setAlignment(Pos.BASELINE_CENTER);
                gridItemsBarra.setMinWidth(Region.USE_COMPUTED_SIZE);
                gridItemsBarra.setPrefWidth(Region.USE_COMPUTED_SIZE);
                gridItemsBarra.setMaxWidth(Region.USE_PREF_SIZE);
                gridItemsBarra.setMinHeight(Region.USE_COMPUTED_SIZE);
                gridItemsBarra.setPrefHeight(Region.USE_COMPUTED_SIZE);
                gridItemsBarra.setMaxHeight(Region.USE_PREF_SIZE);
                gridItemsBarra.setMargin(elemento, new Insets(20));
            } catch (IOException e) {
                Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error: " + e.getMessage());
                stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            }
        }
    }

    /**
     * Creamos los elementos visuales del salón.
     */
    private void crearMesasComedor() {
        int columna = 0;
        int fila = 1;
        for (int i = 0; i < 40; i++) {
            if (columna == 5) {
                columna = 0;
                fila++;
            }

            FXMLLoader viewItemMesa = new FXMLLoader();
            viewItemMesa.setLocation(getClass().getResource(Constantes.FXML_SALON_ITEM));
            viewItemMesa.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
            try {
                AnchorPane elemento = (AnchorPane) viewItemMesa.load();
                ControladorMesaItem controlador = viewItemMesa.getController();

                boolean existePedido = false;
                for (PedidoModel pedido : this.listadoPedidosSalon) {
                    if (pedido.getNumeroSitio() == i + 1) {
                        controlador.setPedido(pedido);
                        existePedido = true;
                        break;
                    }
                }
                // Buscamos en BBDD, y si la mesa no tiene un pedido, lo creamos.
                if (!existePedido) {
                    PedidoModel pedidoSalon = new PedidoModel();
                    pedidoSalon.setSitioPedido(Constantes.SALON_PEDIDO);
                    pedidoSalon.setNumeroSitio(i + 1);
                    pedidoSalon.setIdCliente(Constantes.CLIENTE_FANTASMA);
                    pedidoSalon.setIdUsuario(this.usuarioSesion.getIdUsuario().getValue());
                    controlador.setPedido(pedidoSalon);
                }

                // Evento al hacer click sobre el elemento visual.
                elemento.setOnMouseClicked(event -> {
                    if (controlador.getPedido().getIdPedido() == 0) {
                        Alert alerta = new Alert(Alert.AlertType.CONFIRMATION,
                                Internacionalizacion.get("aler.dist.crearsalon")
                                        + controlador.getPedido().getNumeroSitio(),
                                ButtonType.YES, ButtonType.CANCEL);
                        Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
                        stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                        alerta.showAndWait();
                        if (alerta.getResult() == ButtonType.YES) {
                            // Creamos un pedido nuevo y abrimos la visual del pedido.
                            this.crearPedido(controlador.getPedido());
                            Stage stage = new Stage();
                            FXMLLoader viewPedido = new FXMLLoader();
                            stage.setTitle(Internacionalizacion.get("pedi.titulo"));
                            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                            viewPedido.setLocation(getClass().getResource(Constantes.FXML_PEDIDO));
                            viewPedido.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
                            try {
                                stage.setScene(new Scene((ScrollPane) viewPedido.load()));
                                ControladorPedido controladorPedido = viewPedido.getController();
                                controladorPedido.setPedido(this.obtenerPedido(controlador.getPedido()));
                                controladorPedido.setLogs(logs);
                                controladorPedido.iniciarControlador();
                                stage.showAndWait();
                                this.refrescarInterfaz();
                            } catch (IOException e) {
                                this.logs.getLogs().severe("Error: " + e.getMessage());
                                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                                alert.showAndWait();
                            } catch (ClassNotFoundException e) {
                                this.logs.getLogs().severe("Error: " + e.getMessage());
                                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                                alert.showAndWait();
                            } catch (SQLException e) {
                                this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
                                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                                        Internacionalizacion.get("aler.bbdd.cabecera"),
                                        e.getMessage());
                                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                                alert.showAndWait();
                            }
                        }
                    } else {
                        // Abrimos la visual con el pedido recuperado de BBDD.
                        Stage stage = new Stage();
                        FXMLLoader viewPedido = new FXMLLoader();
                        stage.setTitle(Internacionalizacion.get("pedi.titulo"));
                        stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                        viewPedido.setLocation(getClass().getResource(Constantes.FXML_PEDIDO));
                        viewPedido.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
                        try {
                            stage.setScene(new Scene((ScrollPane) viewPedido.load()));
                            ControladorPedido controladorPedido = viewPedido.getController();
                            controladorPedido.setPedido(controlador.getPedido());
                            controladorPedido.setLogs(logs);
                            controladorPedido.iniciarControlador();
                            stage.showAndWait();
                            this.refrescarInterfaz();
                        } catch (IOException e) {
                            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
                            this.logs.getLogs().severe("Error: " + e.getMessage());
                            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                            alerta.showAndWait();
                        }
                    }
                });
                //Agregamos el elemento visual al gridPane
                gridItems.add(elemento, columna++, fila);
                gridItems.setAlignment(Pos.BASELINE_CENTER);
                gridItems.setMinWidth(Region.USE_COMPUTED_SIZE);
                gridItems.setPrefWidth(Region.USE_COMPUTED_SIZE);
                gridItems.setMaxWidth(Region.USE_PREF_SIZE);

                gridItems.setMinHeight(Region.USE_COMPUTED_SIZE);
                gridItems.setPrefHeight(Region.USE_COMPUTED_SIZE);
                gridItems.setMaxHeight(Region.USE_PREF_SIZE);
                gridItems.setMargin(elemento, new Insets(20));
            } catch (IOException e) {
                Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error: " + e.getMessage());
                stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            }
        }
    }

    private void crearPedido(PedidoModel pedido) {
        PedidoUtilsBD pedidosUtils = null;
        try {
            pedidosUtils = new PedidoUtilsBD();
            pedidosUtils.crearPedido(pedido);
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (pedidosUtils != null)
                pedidosUtils.cerrarConexion();
        }
    }

    /**
     * Obtiene un pedido de Base de Datos.
     * @param pedido
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private PedidoModel obtenerPedido(PedidoModel pedido) throws ClassNotFoundException, SQLException {
        PedidoUtilsBD pedidosUtils = null;
        PedidoModel pedidoModel = new PedidoModel();
        try {
            pedidosUtils = new PedidoUtilsBD();
            pedidoModel = pedidosUtils.obtenerPedido(pedido);
        } catch (ClassNotFoundException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        } finally {
            if (pedidosUtils != null)
                pedidosUtils.cerrarConexion();
        }
        return pedidoModel;
    }

    /**
     * Setter Usuario Sesion
     * @param usuario
     */
    public void setUsuarioSesion(UsuarioModel usuario) {
        this.usuarioSesion = usuario;
    }

    /**
     * Llamas a las funciones básicas para crear los elementos de pantalla.
     */
    public void iniciarControlador() {
        this.crearMesasComedor();
        this.crearMesasTerraza();
        this.crearSitiosBarra();
    }

    /**
     * Borra el contenido de la interfaz y vuelve a generarla
     */
    private void refrescarInterfaz() {
        this.obtenerPedidos();
        this.gridItems.getChildren().clear();
        this.gridItemsBarra.getChildren().clear();
        this.gridItemsTerraza.getChildren().clear();
        this.crearMesasComedor();
        this.crearMesasTerraza();
        this.crearSitiosBarra();
        this.obtenerPedidosDomicilio();
    }

    /**
     * Obtiene el listado de pedidos a domicilio.
     */
    private void obtenerPedidosDomicilio() {
        PedidosDomicilioUtilsBD pedidosUtils = null;
        try {
            pedidosUtils = new PedidosDomicilioUtilsBD();
            this.listadoPedidosDomicilio.setAll(pedidosUtils.listadoPedidosDomicilio());
            this.tbPedidosDomicilio.refresh();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }

    }

    private PedidoDomicilioModel obtenerPedidoDomicilio(ClienteModel pedido) {
        PedidosDomicilioUtilsBD pedidosUtils = null;
        PedidoDomicilioModel pedidoReturn = null;
        try {
            pedidosUtils = new PedidosDomicilioUtilsBD();
            pedidoReturn = pedidosUtils.obtenerPedidoPorCliente(pedido);
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (pedidosUtils != null)
                pedidosUtils.cerrarConexion();
        }
        return pedidoReturn;
    }

    /**
     * Setter logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }

    /**
     * Internacionalización de los elementos de pantalla.
     */
    private void internacionalizacion() {
        Internacionalizacion.localeProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.tbComedor.textProperty().bind(Internacionalizacion.createStringBinding("distr.sala.comedor"));
                this.tbTerraza.textProperty().bind(Internacionalizacion.createStringBinding("distr.sala.terraza"));
                this.tbDomicilio.textProperty().bind(Internacionalizacion.createStringBinding("distr.sala.domicilio"));
                this.tbBarra.textProperty().bind(Internacionalizacion.createStringBinding("distr.sala.barra"));
                this.btNuevoPedido.textProperty().bind(Internacionalizacion.createStringBinding("distr.domi.nuevopedido"));
                this.btMostrarPedido.textProperty().bind(Internacionalizacion.createStringBinding("distr.domi.modificar"));
                this.tcNumeroPedido.textProperty().bind(Internacionalizacion.createStringBinding("distr.domi.numeropedido"));
                this.tcFechaCreacion.textProperty().bind(Internacionalizacion.createStringBinding("distr.domi.fecha"));
                this.tcNumeroTelefono.textProperty().bind(Internacionalizacion.createStringBinding("distr.domi.numerotelefono"));
                this.tcProvincia.textProperty().bind(Internacionalizacion.createStringBinding("distr.domi.provincia"));
                this.tcCiudad.textProperty().bind(Internacionalizacion.createStringBinding("distr.domi.ciudad"));
                this.tcDireccion.textProperty().bind(Internacionalizacion.createStringBinding("distr.domi.direccion"));
                this.tcNumeroPuerta.textProperty().bind(Internacionalizacion.createStringBinding("distr.domi.numeropuerta"));
                this.tcLetraPuerta.textProperty().bind(Internacionalizacion.createStringBinding("distr.domi.letrapuerta"));
            }
        });
    }
}
