package com.proyectotpv.proyectotpv.controladores.main.usuario;

import com.proyectotpv.proyectotpv.modelos.logininicio.RolModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.usuario.UsuarioUtilsBD;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ControladorNuevoUsuario {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btCancelar;

    @FXML
    private Button btCrearUsuario;

    @FXML
    private ComboBox<RolModel> cbRol;

    @FXML
    private TextField tfApellidos;

    @FXML
    private TextField tfCorreoElectronico;

    @FXML
    private TextField tfNombre;

    @FXML
    private TextField tfUsuario;

    @FXML
    private Label lbTitulo;

    private UsuarioModel usuarioNuevo;

    private Logs logs;

    /**
     * Crea un nuevo usuario con los datos introducidos.
     * @param event
     */
    @FXML
    void aceptar(ActionEvent event) {
        UsuarioUtilsBD usuariosUtils = null;
        try {
            this.comprobacionesCampos();
            usuariosUtils = new UsuarioUtilsBD();
            usuariosUtils.nuevoUsuario(usuarioNuevo);
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (ClassNotFoundException | NoSuchAlgorithmException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (usuariosUtils != null) usuariosUtils.cerrarConexion();
            ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
        }

    }

    /**
     * Función del botón cancelar.
     * @param event
     */
    @FXML
    void cancelar(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Setter del titulo de la visual.
     * @param titulo
     */
    public void setTitulo(String titulo) {
        this.lbTitulo.textProperty().setValue(titulo);
    }

    /**
     * Inicializa la clase UsuarioModel y el binding bidireccional
     */
    @FXML
    void initialize() {
        this.usuarioNuevo = new UsuarioModel();
        this.tfUsuario.textProperty().bindBidirectional(this.usuarioNuevo.getUsuario());
        this.tfNombre.textProperty().bindBidirectional(this.usuarioNuevo.getNombreUsuario());
        this.tfApellidos.textProperty().bindBidirectional(this.usuarioNuevo.getApellidosUsuario());
        this.tfCorreoElectronico.textProperty().bindBidirectional(this.usuarioNuevo.getCorreoElectronico());
        this.cbRol.valueProperty().bindBidirectional(this.usuarioNuevo.getRolUsuario());

        this.obtenerListadoRoles();
    }

    /**
     * Realiza las comprobaciones de los datos introducidos.
     * @throws AplicacionExcepcion
     */
    private void comprobacionesCampos() throws AplicacionExcepcion {
        if (this.usuarioNuevo.getUsuario().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.usuario"));
        }
        if (this.usuarioNuevo.getNombreUsuario().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.nombre"));
        }
        if (this.usuarioNuevo.getApellidosUsuario().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.apellidos"));
        }
        if (this.usuarioNuevo.getCorreoElectronico().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.correo"));
        }
        if (!this.comprobarEmail()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.correoformato"));
        }
        if (this.cbRol.selectionModelProperty().get().isEmpty()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.rol"));
        }
        if (this.usuarioNuevo.getUsuario().length().get() >= 50) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.usuariolongitud"));
        }
        if (this.usuarioNuevo.getNombreUsuario().length().get() >= 40) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.nombrelongitud"));
        }
        if (this.usuarioNuevo.getApellidosUsuario().length().get() >= 40) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.apellidoslongitud"));
        }
        if (this.usuarioNuevo.getCorreoElectronico().length().get() >= 100) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.correolongitud"));
        }

    }

    /**
     * Comprueba el email mediante una expresión regular.
     * @return
     */
    private boolean comprobarEmail() {
        Pattern patron = Pattern.compile(Constantes.PATRON_CORREO, Pattern.CASE_INSENSITIVE);
        Matcher matcher = patron.matcher(this.usuarioNuevo.getCorreoElectronico().getValueSafe());
        return matcher.find();
    }

    /**
     * Obtiene el listado de roles.
     */
    private void obtenerListadoRoles() {
        UsuarioUtilsBD utilidadesUsuarios = null;
        try {
            utilidadesUsuarios = new UsuarioUtilsBD();
            this.cbRol.setItems(utilidadesUsuarios.obtenerRoles());
            this.cbRol.getSelectionModel().selectFirst();
            utilidadesUsuarios.cerrarConexion();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (utilidadesUsuarios != null)
                utilidadesUsuarios.cerrarConexion();
        }

    }

    /**
     * Setter logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }
}
