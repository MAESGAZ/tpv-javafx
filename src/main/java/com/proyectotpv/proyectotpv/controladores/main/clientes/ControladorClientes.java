package com.proyectotpv.proyectotpv.controladores.main.clientes;

import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.modelos.pedido.ClienteModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.cliente.ClientesUtilsBD;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class ControladorClientes {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btActivarCliente;

    @FXML
    private Button btDesactivarCliente;

    @FXML
    private Button btEditarCliente;

    @FXML
    private Button btEliminarCliente;

    @FXML
    private Button btNuevoCliente;

    @FXML
    private Label lbFiltro;

    @FXML
    private TableView<ClienteModel> tbClientes;

    @FXML
    private TableColumn<ClienteModel, String> tcCiudad;

    @FXML
    private TableColumn<ClienteModel, Number> tcCodigoPostal;

    @FXML
    private TableColumn<ClienteModel, String> tcDireccion;

    @FXML
    private TableColumn<ClienteModel, String> tcEstado;

    @FXML
    private TableColumn<ClienteModel, String> tcFechaCreacion;

    @FXML
    private TableColumn<ClienteModel, String> tcFechaModificacion;

    @FXML
    private TableColumn<ClienteModel, Number> tcNumero;

    @FXML
    private TableColumn<ClienteModel, Number> tcNumeroTelefono;

    @FXML
    private TableColumn<ClienteModel, String> tcProvincia;

    @FXML
    private TableColumn<ClienteModel, String> tcPuerta;

    @FXML
    private TextField tfFiltroClientes;

    private FilteredList<ClienteModel> filtro;

    private ObservableList<ClienteModel> listadoClientes = FXCollections.observableArrayList();

    private UsuarioModel usuarioSesion;

    private Logs logs;

    /**
     * Activa el cliente.
     * @param event
     */
    @FXML
    void activarCliente(ActionEvent event) {
        ClientesUtilsBD clientesUtils = null;
        try {
            clientesUtils = new ClientesUtilsBD();
            clientesUtils.activarCliente(this.tbClientes.getSelectionModel().getSelectedItem());
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (clientesUtils != null)
                clientesUtils.cerrarConexion();
        }
        this.refrescarListado();
    }

    /**
     * Desactiva el cliente.
     * @param event
     */
    @FXML
    void desactivarCliente(ActionEvent event) {
        ClientesUtilsBD clientesUtils = null;
        try {
            clientesUtils = new ClientesUtilsBD();
            if (clientesUtils.desactivarCliente(this.tbClientes.getSelectionModel().getSelectedItem()) != 1)
                throw new AplicacionExcepcion();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (clientesUtils != null)
                clientesUtils.cerrarConexion();
        }
        this.refrescarListado();
    }

    /**
     * Abre visual para editar el cliente.
     * @param event
     */
    @FXML
    void editarCliente(ActionEvent event) {
        Stage stage = new Stage();
        FXMLLoader viewEditarCliente = new FXMLLoader();
        viewEditarCliente.setLocation(getClass().getResource(Constantes.FXML_EDITAR_CLIENTE));
        viewEditarCliente.setResources(
                ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            stage.setScene(new Scene((AnchorPane) viewEditarCliente.load()));
            ControladorEditarCliente controlador = viewEditarCliente.getController();
            controlador.setCliente(this.tbClientes.getSelectionModel().getSelectedItem());
            controlador.setLogs(logs);
            controlador.iniciarControlador();
            stage.setResizable(false);
            stage.setTitle(Internacionalizacion.get("clien.edit.titulo"));
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            stage.showAndWait();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
        this.refrescarListado();
    }

    /**
     * Elimina el cliente si se confirma en la alerta.
     * @param event
     */
    @FXML
    void eliminarCliente(ActionEvent event) {
        Alerta alertaCon = new Alerta(Alert.AlertType.CONFIRMATION,
                Internacionalizacion.get("aler.clientecontroller.titulo"),
                Internacionalizacion.get("aler.clientecontroller.cabecera"),
                Internacionalizacion.get("aler.clientecontroller.cuerpo"));
        Stage stageAlertaCon = (Stage) alertaCon.getDialogPane().getScene().getWindow();
        stageAlertaCon.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
        alertaCon.showAndWait();
        if (alertaCon.getResult() == ButtonType.OK) {
            ClientesUtilsBD clientesUtils = null;
            try {
                clientesUtils = new ClientesUtilsBD();
                clientesUtils.eliminarCliente(this.tbClientes.getSelectionModel().getSelectedItem());
            } catch (ClassNotFoundException e) {
                Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error: " + e.getMessage());
                stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            } catch (SQLException e) {
                Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                        Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
                Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
                stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            }
            this.refrescarListado();
        }
    }

    /**
     * Abre la visual para crear un cliente.
     * @param event
     */
    @FXML
    void nuevoCliente(ActionEvent event) {
        Stage stage = new Stage();
        FXMLLoader viewNuevoCliente = new FXMLLoader();
        viewNuevoCliente.setLocation(getClass().getResource(Constantes.FXML_NUEVO_CLIENTE));
        viewNuevoCliente.setResources(
                ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            stage.setScene(new Scene((AnchorPane) viewNuevoCliente.load()));
            ControladorNuevoCliente controlador = viewNuevoCliente.getController();
            controlador.setUsuarioSesion(usuarioSesion);
            controlador.setLogs(logs);
            stage.setResizable(false);
            stage.setTitle(Internacionalizacion.get("clien.nuev.titulo"));
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            stage.showAndWait();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
        this.refrescarListado();
    }

    /**
     * Internacionalilzación y activación o desactivación de los botones, columnas de la tabla, etc.
     */
    @FXML
    void initialize() {
        this.internacionalizacion();
        this.btEditarCliente.setDisable(true);
        this.btDesactivarCliente.setDisable(true);
        this.btActivarCliente.setDisable(true);
        this.btEliminarCliente.setDisable(true);

        this.tcNumeroTelefono.setCellValueFactory(cellData -> cellData.getValue().numeroTelefonoProperty());
        this.tcProvincia.setCellValueFactory(cellData -> cellData.getValue().provinciaProperty());
        this.tcCiudad.setCellValueFactory(cellData -> cellData.getValue().ciudadProperty());
        this.tcCodigoPostal.setCellValueFactory(cellData -> cellData.getValue().codigoPostalProperty());
        this.tcDireccion.setCellValueFactory(cellData -> cellData.getValue().direccionClienteProperty());
        this.tcNumero.setCellValueFactory(cellData -> cellData.getValue().numeroPuertaClienteProperty());
        this.tcPuerta.setCellValueFactory(cellData -> cellData.getValue().letraPuertaClienteProperty());
        this.tcEstado.setCellValueFactory(cellData -> {
            if (cellData.getValue().isEstadoCliente())
                return new SimpleStringProperty(Constantes.ESTADO_ACTIVADO);
            else
                return new SimpleStringProperty(Constantes.ESTADO_DESACTIVADO);
        });
        this.tcFechaCreacion.setCellValueFactory(cellData -> {
            LocalDateTime fecha = cellData.getValue().getFechaCreacion();
            return new SimpleStringProperty(
                    fecha.getDayOfMonth() + "-" + fecha.getMonthValue() + "-" + fecha.getYear());
        });
        this.tcFechaModificacion.setCellValueFactory(cellData -> {
            if (cellData.getValue().fechaModificacionProperty().isNull().getValue()) {
                return new SimpleStringProperty(Constantes.FECHA_VACIA);
            } else {
                LocalDateTime fecha = cellData.getValue().getFechaModificacion();
                return new SimpleStringProperty(
                        fecha.getDayOfMonth() + "-" + fecha.getMonthValue() + "-" + fecha.getYear());
            }
        });

        this.tbClientes.getSelectionModel().selectedItemProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.btEditarCliente.setDisable(false);
                this.btEliminarCliente.setDisable(false);
                if (newValue.isEstadoCliente()) {
                    this.btActivarCliente.setDisable(true);
                    this.btDesactivarCliente.setDisable(false);
                } else {
                    this.btActivarCliente.setDisable(false);
                    this.btDesactivarCliente.setDisable(true);
                }
            } else {
                this.btActivarCliente.setDisable(true);
                this.btDesactivarCliente.setDisable(true);
                this.btEditarCliente.setDisable(true);
                this.btEliminarCliente.setDisable(true);
            }
        });

        this.tfFiltroClientes.textProperty().addListener((old, oldValue, newValue) -> {
            this.filtro.setPredicate(cliente -> {
                if (cliente.numeroTelefonoProperty().toString().toLowerCase().contains(newValue.toLowerCase())
                        || cliente.getProvincia().toLowerCase().contains(newValue.toLowerCase())
                        || cliente.getCiudad().toLowerCase().contains(newValue.toLowerCase())
                        || cliente.codigoPostalProperty().asString().getValueSafe().toLowerCase()
                        .contains(newValue.toLowerCase())
                        || cliente.getDireccionCliente().toLowerCase().contains(newValue.toLowerCase())
                        || cliente.numeroPuertaClienteProperty().asString().getValueSafe().toLowerCase()
                        .contains(newValue.toLowerCase())
                        || cliente.getLetraPuertaCliente().toLowerCase()
                        .contains(newValue.toLowerCase())) {
                    return true;
                } else {
                    return false;
                }
            });
        });

        this.refrescarListado();
        this.filtro = new FilteredList<ClienteModel>(this.listadoClientes);
        this.tbClientes.setItems(filtro);
    }

    /**
     * Setter Usuario Sesion
     * @param usuarioSesion
     */
    public void setUsuarioSesion(UsuarioModel usuarioSesion) {
        this.usuarioSesion = usuarioSesion;
    }

    /**
     * Obtiene el listado de Clientes de BBDD.
     */
    private void refrescarListado() {
        ClientesUtilsBD clientesUtils = null;
        try {
            clientesUtils = new ClientesUtilsBD();
            this.listadoClientes.setAll(clientesUtils.listadoClientes());
            this.tbClientes.refresh();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (clientesUtils != null)
                clientesUtils.cerrarConexion();
        }
    }

    /**
     * Internacionalización de los componentes.
     */
    private void internacionalizacion() {
        Internacionalizacion.localeProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.btNuevoCliente.textProperty().bind(Internacionalizacion.createStringBinding("clien.nuevocliente"));
                this.btEditarCliente.textProperty()
                        .bind(Internacionalizacion.createStringBinding("clien.editarcliente"));
                this.btDesactivarCliente.textProperty()
                        .bind(Internacionalizacion.createStringBinding("clien.desactcliente"));
                this.btActivarCliente.textProperty()
                        .bind(Internacionalizacion.createStringBinding("clien.activacliente"));
                this.btEliminarCliente.textProperty()
                        .bind(Internacionalizacion.createStringBinding("clien.eliminacliente"));
                this.lbFiltro.textProperty().bind(Internacionalizacion.createStringBinding("clien.filtro"));
                this.tcNumeroTelefono.textProperty()
                        .bind(Internacionalizacion.createStringBinding("clien.numerotelefono"));
                this.tcProvincia.textProperty().bind(Internacionalizacion.createStringBinding("clien.provincia"));
                this.tcCiudad.textProperty().bind(Internacionalizacion.createStringBinding("clien.ciudad"));
                this.tcCodigoPostal.textProperty().bind(Internacionalizacion.createStringBinding("clien.codigopostal"));
                this.tcDireccion.textProperty().bind(Internacionalizacion.createStringBinding("clien.direccion"));
                this.tcNumero.textProperty().bind(Internacionalizacion.createStringBinding("clien.numero"));
                this.tcPuerta.textProperty().bind(Internacionalizacion.createStringBinding("clien.puerta"));
                this.tcEstado.textProperty().bind(Internacionalizacion.createStringBinding("clien.estado"));
                this.tcFechaCreacion.textProperty().bind(Internacionalizacion.createStringBinding("clien.fechacrea"));
                this.tcFechaModificacion.textProperty()
                        .bind(Internacionalizacion.createStringBinding("clien.fechamodi"));
            }
        });
    }

    /**
     * Setter de los logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }
}
