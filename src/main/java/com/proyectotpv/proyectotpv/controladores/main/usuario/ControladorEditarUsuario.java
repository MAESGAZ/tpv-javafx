package com.proyectotpv.proyectotpv.controladores.main.usuario;

import com.proyectotpv.proyectotpv.modelos.logininicio.RolModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.usuario.UsuarioUtilsBD;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ControladorEditarUsuario {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btCancelar;

    @FXML
    private Button btCrearUsuario;

    @FXML
    private ComboBox<RolModel> cbRol;

    @FXML
    private Label lbTitulo;

    @FXML
    private TextField tfApellidos;

    @FXML
    private TextField tfCorreoElectronico;

    @FXML
    private TextField tfNombre;

    @FXML
    private TextField tfUsuario;

    private UsuarioModel usuarioOriginal;

    private UsuarioModel usuario;

    private Logs logs;

    /**
     * Función aceptar para guardar los cambios del usuario.
     *
     * @param event
     */
    @FXML
    void aceptar(ActionEvent event) {
        boolean exito = false;
        UsuarioUtilsBD usuariosUtils = null;
        try {
            this.comprobacionesCampos();
            usuariosUtils = new UsuarioUtilsBD();
            usuariosUtils.modificarUsuario(usuario);
            exito = true;
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (usuariosUtils != null)
                usuariosUtils.cerrarConexion();
            if (exito)
                ((Stage) this.tfUsuario.getScene().getWindow()).close();
        }
    }

    /**
     * Función del botón cancelar.
     */
    @FXML
    void cancelar(ActionEvent event) {
        ((Stage) this.tfUsuario.getScene().getWindow()).close();
    }

    /**
     * Inicia el controlador.
     */
    @FXML
    void initialize() {
        this.obtenerListadoRoles();

    }

    public void setUsuario(UsuarioModel usuario) {
        this.usuarioOriginal = usuario;
        this.usuario = usuario.clone();
    }

    /**
     * Inicia el binding bidireccional.
     */
    public void iniciarControlador() {
        this.tfUsuario.textProperty().bindBidirectional(this.usuario.getUsuario());
        this.tfNombre.textProperty().bindBidirectional(this.usuario.getNombreUsuario());
        this.tfApellidos.textProperty().bindBidirectional(this.usuario.getApellidosUsuario());
        this.tfCorreoElectronico.textProperty().bindBidirectional(this.usuario.getCorreoElectronico());
        this.cbRol.valueProperty().bindBidirectional(this.usuario.getRolUsuario());
    }

    /**
     * Obtiene el listado de roles.
     */
    private void obtenerListadoRoles() {
        UsuarioUtilsBD utilidadesUsuarios = null;
        try {
            utilidadesUsuarios = new UsuarioUtilsBD();
            this.cbRol.setItems(utilidadesUsuarios.obtenerRoles());
            this.cbRol.getSelectionModel().selectFirst();
            utilidadesUsuarios.cerrarConexion();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (utilidadesUsuarios != null)
                utilidadesUsuarios.cerrarConexion();
        }

    }

    /**
     * Realiza las comprobaciones de los datos introducidos.
     *
     * @throws AplicacionExcepcion
     */
    private void comprobacionesCampos() throws AplicacionExcepcion {
        if (this.usuario.getUsuario().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.usuario"));
        }
        if (this.usuario.getNombreUsuario().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.nombre"));
        }
        if (this.usuario.getApellidosUsuario().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.apellidos"));
        }
        if (this.usuario.getCorreoElectronico().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.correo"));
        }
        if (!this.comprobarEmail()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.correoformato"));
        }
        if (this.cbRol.selectionModelProperty().get().isEmpty()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.rol"));
        }
        if (this.usuario.getUsuario().length().get() >= 50) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.usuariolongitud"));
        }
        if (this.usuario.getNombreUsuario().length().get() >= 40) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.nombrelongitud"));
        }
        if (this.usuario.getApellidosUsuario().length().get() >= 40) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.apellidoslongitud"));
        }
        if (this.usuario.getCorreoElectronico().length().get() >= 100) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.correolongitud"));
        }
    }

    /**
     * Comprueba el email mediante expresión regular.
     *
     * @return
     */
    private boolean comprobarEmail() {
        Pattern patron = Pattern.compile(Constantes.PATRON_CORREO, Pattern.CASE_INSENSITIVE);
        Matcher matcher = patron.matcher(this.usuario.getCorreoElectronico().getValueSafe());
        return matcher.find();
    }

    /**
     * Setter logs
     *
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }
}
