package com.proyectotpv.proyectotpv.controladores.main.usuario;

import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.usuario.UsuarioUtilsBD;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class ControladorUsuarios {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btActivarUsuario;

    @FXML
    private Button btDesactivarUsuario;

    @FXML
    private Button btEditarUsuario;

    @FXML
    private Button btEliminarUsuario;

    @FXML
    private Button btNuevoUsuario;

    @FXML
    private Button btResetearClave;

    @FXML
    private Label lbFiltro;

    @FXML
    private TableView<UsuarioModel> tbUsuarios;

    @FXML
    private TableColumn<UsuarioModel, String> tcApellidos;

    @FXML
    private TableColumn<UsuarioModel, String> tcCambioClave;

    @FXML
    private TableColumn<UsuarioModel, String> tcEstado;

    @FXML
    private TableColumn<UsuarioModel, String> tcFechaCambioClave;

    @FXML
    private TableColumn<UsuarioModel, String> tcNombre;

    @FXML
    private TableColumn<UsuarioModel, String> tcRol;

    @FXML
    private TableColumn<UsuarioModel, String> tcUsuario;

    @FXML
    private TextField tfFiltroUsuario;

    private Logs logs;

    private FilteredList<UsuarioModel> filtro;

    private ObservableList<UsuarioModel> listadoUsuarios = FXCollections.observableArrayList();

    /**
     * Activa el usuario seleccionado.
     * @param event
     */
    @FXML
    void activarUsuario(ActionEvent event) {
        UsuarioUtilsBD usuariosUtils = null;
        try {
            usuariosUtils = new UsuarioUtilsBD();
            usuariosUtils.activarUsuario(this.tbUsuarios.getSelectionModel().getSelectedItem());
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (usuariosUtils != null)
                usuariosUtils.cerrarConexion();

        }
        this.refrescarListado();
    }

    /**
     * Desactiva el usuario seleccionado.
     * @param event
     */
    @FXML
    void desactivarUsuario(ActionEvent event) {
        UsuarioUtilsBD usuariosUtils = null;
        try {
            usuariosUtils = new UsuarioUtilsBD();
            usuariosUtils.desactivarUsuario(this.tbUsuarios.getSelectionModel().getSelectedItem());
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (usuariosUtils != null)
                usuariosUtils.cerrarConexion();

        }
        this.refrescarListado();
    }
    /**
     * Abre la visual para editar el usuario seleccionado.
     * @param event
     */
    @FXML
    void editarUsuario(ActionEvent event) {
        Stage stage = new Stage();
        FXMLLoader viewEditarUsuario = new FXMLLoader();
        viewEditarUsuario
                .setLocation(getClass().getResource(Constantes.FXML_EDITAR_USUARIO));
        viewEditarUsuario.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            Scene escena = new Scene((AnchorPane) viewEditarUsuario.load());
            ControladorEditarUsuario controlador = viewEditarUsuario.getController();
            controlador.setUsuario(this.tbUsuarios.getSelectionModel().getSelectedItem());
            controlador.setLogs(logs);
            controlador.iniciarControlador();
            stage.setScene(escena);
            stage.setTitle(Internacionalizacion.get("usua.edit.titulo"));
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stag = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
        this.refrescarListado();
    }

    /**
     * Elimina el usuario seleccionado.
     * @param event
     */
    @FXML
    void eliminarUsuario(ActionEvent event) {
        Alerta alerta = new Alerta("Eliminar Usuario", "Eliminar Usuario", "¿Seguro que quieres eliminar al usuario?", ButtonType.YES, ButtonType.CANCEL);
        Stage stage = ((Stage) alerta.getDialogPane().getScene().getWindow());
        stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
        alerta.showAndWait();
        if (alerta.getResult() == ButtonType.YES) {
            UsuarioUtilsBD usuariosUtils = null;
            try {
                usuariosUtils = new UsuarioUtilsBD();
                usuariosUtils.eliminarUsuario(this.tbUsuarios.getSelectionModel().getSelectedItem());
            } catch (ClassNotFoundException e) {
                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error: "+e.getMessage());
                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alert.showAndWait();
            } catch (SQLException e) {
                Alerta alert = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                        Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
                Stage stag = (Stage) alert.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
                stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alert.showAndWait();
            } finally {
                if (usuariosUtils != null)
                    usuariosUtils.cerrarConexion();
            }
            this.refrescarListado();
        }
    }

    /**
     * Abre la visual para crear un nuevo usuario.
     * @param event
     */
    @FXML
    void nuevoUsuario(ActionEvent event) {
        Stage stage = new Stage();
        FXMLLoader viewNuevoUsuario = new FXMLLoader();
        viewNuevoUsuario.setLocation(getClass().getResource(Constantes.FXML_NUEVO_USUARIO));
        viewNuevoUsuario.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            Scene escena = new Scene((AnchorPane) viewNuevoUsuario.load());
            ControladorNuevoUsuario controlador = viewNuevoUsuario.getController();
            controlador.setTitulo("Nuevo Usuario");
            controlador.setLogs(logs);
            stage.setScene(escena);
            stage.setTitle(Internacionalizacion.get("usua.nuev.titulo"));
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stag = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stag.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
        this.refrescarListado();
    }

    /**
     * Obliga al usuario indicado a cambiar la contraseña la proxima vez que inicie sesión.
     * @param event
     */
    @FXML
    void resetearClave(ActionEvent event) {
        UsuarioUtilsBD usuariosUtils = null;
        try {
            usuariosUtils = new UsuarioUtilsBD();
            usuariosUtils.resetearClave(this.tbUsuarios.getSelectionModel().getSelectedItem());
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (usuariosUtils != null)
                usuariosUtils.cerrarConexion();

        }
        this.refrescarListado();
    }

    /**
     * Inicializa la internacionalización, los botones, las columnas de la tabla y el binding bidireccional.
     */
    @FXML
    void initialize() {
        this.internacionalizacion();
        this.btActivarUsuario.setDisable(true);
        this.btDesactivarUsuario.setDisable(true);
        this.btEditarUsuario.setDisable(true);
        this.btResetearClave.setDisable(true);
        this.btEliminarUsuario.setDisable(true);

        this.tcUsuario.setCellValueFactory(cellData -> cellData.getValue().getUsuario());
        this.tcNombre.setCellValueFactory(cellData -> cellData.getValue().getNombreUsuario());
        this.tcApellidos.setCellValueFactory(cellData -> cellData.getValue().getApellidosUsuario());
        this.tcEstado.setCellValueFactory(cellData -> {
            if (cellData.getValue().getEstadoUsuario().getValue()) {
                return new SimpleStringProperty(Constantes.ESTADO_ACTIVADO);
            } else {
                return new SimpleStringProperty(Constantes.ESTADO_DESACTIVADO);
            }
        });
        this.tcCambioClave.setCellValueFactory(cellData -> {
            if (cellData.getValue().getCambioClave().getValue())
                return new SimpleStringProperty(Constantes.ESTADO_ACTIVADO);
            else
                return new SimpleStringProperty(Constantes.ESTADO_DESACTIVADO);
        });
        this.tcFechaCambioClave.setCellValueFactory(cellData -> {
            LocalDateTime fecha = cellData.getValue().getFechaCambioClave().getValue();
            return new SimpleStringProperty(
                    fecha.getDayOfMonth() + "-" + fecha.getMonthValue() + "-" + fecha.getYear());
        });
        this.tcRol.setCellValueFactory(cellData -> cellData.getValue().getRolUsuario().getValue().getNombreRol());

        this.refrescarListado();
        this.filtro = new FilteredList<UsuarioModel>(this.listadoUsuarios);
        this.tbUsuarios.setItems(filtro);

        this.tbUsuarios.getSelectionModel().selectedItemProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.btEditarUsuario.setDisable(false);
                this.btEliminarUsuario.setDisable(false);
                if (newValue.getEstadoUsuario().getValue()) {
                    this.btActivarUsuario.setDisable(true);
                    this.btDesactivarUsuario.setDisable(false);
                } else {
                    this.btActivarUsuario.setDisable(false);
                    this.btDesactivarUsuario.setDisable(true);
                }
                if (newValue.getCambioClave().getValue()) {
                    this.btResetearClave.setDisable(true);
                } else {
                    this.btResetearClave.setDisable(false);
                }
            } else {
                this.btEditarUsuario.setDisable(true);
                this.btResetearClave.setDisable(true);
                this.btEliminarUsuario.setDisable(true);
                this.btActivarUsuario.setDisable(true);
                this.btDesactivarUsuario.setDisable(true);
            }
        });

        this.tfFiltroUsuario.textProperty().addListener((old, oldValue, newValue) -> {
            this.filtro.setPredicate(usuario -> {
                if (usuario.getUsuario().getValueSafe().toLowerCase().contains(newValue.toLowerCase())
                        || usuario.getNombreUsuario().getValueSafe().toLowerCase().contains(newValue.toLowerCase())
                        || usuario.getApellidosUsuario().getValueSafe().toLowerCase().contains(newValue.toLowerCase())
                        || usuario.getRolUsuario().getValue().getNombreRol().getValueSafe().toLowerCase()
                        .contains(newValue.toLowerCase())) {
                    return true;
                } else {
                    return false;
                }
            });
        });

    }

    /**
     * Obtiene el listado de usuarios.
     */
    private void refrescarListado() {
        UsuarioUtilsBD usuariosUtils = null;
        try {
            usuariosUtils = new UsuarioUtilsBD();
            this.listadoUsuarios.setAll(usuariosUtils.listadoUsuarios());
            this.tbUsuarios.refresh();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (usuariosUtils != null)
                usuariosUtils.cerrarConexion();
        }
    }

    /**
     * Inicia la internacionalización.
     */
    private void internacionalizacion() {
        Internacionalizacion.localeProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.btNuevoUsuario.textProperty().bind(Internacionalizacion.createStringBinding("usua.nuevousua"));
                this.btActivarUsuario.textProperty().bind(Internacionalizacion.createStringBinding("usua.activarusua"));
                this.btDesactivarUsuario.textProperty().bind(Internacionalizacion.createStringBinding("usua.desactiusua"));
                this.btEditarUsuario.textProperty().bind(Internacionalizacion.createStringBinding("usua.editarusua"));
                this.btResetearClave.textProperty().bind(Internacionalizacion.createStringBinding("usua.reseteoclave"));
                this.btEliminarUsuario.textProperty().bind(Internacionalizacion.createStringBinding("usua.eliminarusua"));
                this.lbFiltro.textProperty().bind(Internacionalizacion.createStringBinding("usua.filtro"));
                this.tcUsuario.textProperty().bind(Internacionalizacion.createStringBinding("usua.tcusuario"));
                this.tcNombre.textProperty().bind(Internacionalizacion.createStringBinding("usua.tcnombre"));
                this.tcApellidos.textProperty().bind(Internacionalizacion.createStringBinding("usua.tcapellidos"));
                this.tcEstado.textProperty().bind(Internacionalizacion.createStringBinding("usua.tcestado"));
                this.tcCambioClave.textProperty().bind(Internacionalizacion.createStringBinding("usua.tccambio"));
                this.tcFechaCambioClave.textProperty().bind(Internacionalizacion.createStringBinding("usua.tcfecha"));
                this.tcRol.textProperty().bind(Internacionalizacion.createStringBinding("usua.tcrol"));
            }
        });
    }

    public void setLogs(Logs logs) {
        this.logs = logs;
    }
}
