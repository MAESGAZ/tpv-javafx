package com.proyectotpv.proyectotpv.controladores.main.clientes;

import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.modelos.pedido.ClienteModel;
import com.proyectotpv.proyectotpv.modelos.pedido.PedidoDomicilioModel;
import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.cliente.ClientesUtilsBD;
import com.proyectotpv.proyectotpv.utiles.basedatos.pedido.PedidosDomicilioUtilsBD;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Objects;
import java.util.ResourceBundle;

public class ControladorBuscarCliente {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btAceptar;

    @FXML
    private Button btCancelar;

    @FXML
    private TextField tfNumeroTelefono;

    private ClienteModel cliente;

    private UsuarioModel usuarioSesion;

    private Logs logs;

    private PedidoDomicilioModel pedidoDomicilio;

    /**
     * Función del botón aceptar.
     * Busca al cliente, y si existe crea el pedido, si no, redirige a crear el cliente.
     * @param event
     */
    @FXML
    void aceptar(ActionEvent event) {
        boolean exito = false;
        boolean crearNuevo = false;
        ClientesUtilsBD clientesUtils = null;
        try {
            this.comprobacion();
            clientesUtils = new ClientesUtilsBD();
            clientesUtils.buscarCliente(cliente);
            if (cliente.getDireccionCliente().isEmpty())
                throw new AplicacionExcepcion(Internacionalizacion.get("aler.controllerbuscarcliente.noexiste"));
            if (cliente.direccionClienteProperty().isNotEmpty().getValue())
                exito = true;
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            alerta.showAndWait();
            crearNuevo = true;
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (Exception e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (clientesUtils != null)
                clientesUtils.cerrarConexion();
            // Si el cliente existe, comprobamos que no tiene pedidos en vuelo. Si tiene, no puede crear un pedido nuevo.
            if (exito) {
                if (!this.comprobarClientePedidoEnVuelo(cliente)) {
                    ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
                    PedidoModel pedido = new PedidoModel();
                    pedido.setSitioPedido(Constantes.DOMICILIO_PEDIDO);
                    pedido.setNumeroSitio(0);
                    pedido.setIdCliente(this.cliente.getIdCliente());
                    pedido.setIdUsuario(this.usuarioSesion.getIdUsuario().getValue());
                    this.pedidoDomicilio = new PedidoDomicilioModel(pedido, this.cliente);
                    this.crearPedidoDomicilio(this.pedidoDomicilio);

                    ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
                } else {
                    Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.erro.titulo"),
                            Internacionalizacion.get("aler.erro.cabecera"),
                            Internacionalizacion.get("aler.controllerbuscarcliente.pedidovuelo"));
                    Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                    this.logs.getLogs().warning("Aviso: " + "Cliente con pedido en vuelo");
                    stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                    alerta.showAndWait();
                }
            }
            // Si hay que crear al cliente, redirige a la ventana de nuevo cliente.
            if (crearNuevo) {
                ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
                Stage stageNuevoCliente = new Stage();
                FXMLLoader viewNuevoCliente = new FXMLLoader();
                stageNuevoCliente.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                stageNuevoCliente.setTitle(Internacionalizacion.get("clien.nuevocliente"));
                viewNuevoCliente.setLocation(ControladorBuscarCliente.class.getResource(Constantes.FXML_NUEVO_CLIENTE));
                viewNuevoCliente.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION,
                        Internacionalizacion.getLocale()));
                try {
                    stageNuevoCliente.setScene(new Scene((AnchorPane) viewNuevoCliente.load()));
                    ControladorNuevoCliente controller = viewNuevoCliente.getController();
                    controller.setLogs(logs);
                    controller.setUsuarioSesion(usuarioSesion);
                    stageNuevoCliente.showAndWait();
                } catch (IOException e) {
                    Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                            Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                    Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                    this.logs.getLogs().severe("Error: " + e.getMessage());
                    stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                    alerta.showAndWait();
                }
            }
        }
    }

    /**
     * Función del botón cancelar
     * @param event
     */
    @FXML
    void cancelar(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Inicializa el convertidor numérico y el binding bidireccional
     */
    @FXML
    void initialize() {
        StringConverter<Number> converter = new StringConverter<Number>() {

            @Override
            public String toString(Number object) {
                return object == null ? Constantes.EMPTY_STRING : object.toString();
            }

            @Override
            public Number fromString(String string) {
                if (string == null) {
                    return 0;
                } else {
                    if (string.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING).isEmpty())
                        return 0;
                    else
                        return Integer.parseInt(string.replaceAll(Constantes.PATRON_NUMERO, Constantes.EMPTY_STRING));
                }
            }
        };
        cliente = new ClienteModel();
        this.tfNumeroTelefono.textProperty().bindBidirectional(this.cliente.numeroTelefonoProperty(), converter);

    }

    /**
     * Realiza las comprobaciones.
     * @throws Exception
     */
    private void comprobacion() throws Exception {
        if (this.cliente.numeroTelefonoProperty().asString().length().getValue() != 9) {
            throw new Exception(Internacionalizacion.get("aler.controllerbuscarcliente.numerolongitud"));
        }
    }

    /**
     * Setter Usuario Sesion
     * @param usuarioSesion
     */
    public void setUsuarioSesion(UsuarioModel usuarioSesion) {
        this.usuarioSesion = usuarioSesion;
    }

    /**
     * Crea el pedido a domicilio.
     * @param pedido
     */
    private void crearPedidoDomicilio(PedidoDomicilioModel pedido) {
        PedidosDomicilioUtilsBD pedidosUtils = null;
        try {
            pedidosUtils = new PedidosDomicilioUtilsBD();
            pedidosUtils.crearPedidoDomicilio(pedido);
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (pedidosUtils != null)
                pedidosUtils.cerrarConexion();
        }
    }


    /**
     * Setter logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }

    /**
     * Comprueba que el cliente tiene pedidos activos o en vuelo.
     * @param cliente
     * @return
     */
    private boolean comprobarClientePedidoEnVuelo(ClienteModel cliente) {
        PedidosDomicilioUtilsBD pedidosUtils = null;
        boolean pedidoActivo = false;
        try {
            pedidosUtils = new PedidosDomicilioUtilsBD();
            pedidoActivo = pedidosUtils.comprobarClientePedidoActivos(cliente);
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (pedidosUtils != null)
                pedidosUtils.cerrarConexion();
        }
        return pedidoActivo;
    }

    public PedidoDomicilioModel getPedido() {
        return this.pedidoDomicilio;
    }
}
