package com.proyectotpv.proyectotpv.controladores.main.categorias;

import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.categoria.CategoriasUtilsBD;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ControladorNuevaCategoria {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lbNombreCategoria;

    @FXML
    private Button btAceptar;

    @FXML
    private Button btCancelar;

    @FXML
    private Label lbTitulo;

    @FXML
    private TextField tfNombreCategoria;

    private Logs logs;

    private UsuarioModel usuarioSesion;

    private CategoriaModel categoria = new CategoriaModel();

    /**
     * Función del botón aceptar.
     * @param event
     */
    @FXML
    void aceptar(ActionEvent event) {
        CategoriasUtilsBD categoriasUtils = null;
        try {
            this.comprobarCampos();
            categoriasUtils = new CategoriasUtilsBD();
            categoriasUtils.nuevaCategoria(categoria);
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"), Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"), Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (categoriasUtils != null) categoriasUtils.cerrarConexion();
            ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
        }
    }

    /**
     * Función del botón cancelar.
     * @param event
     */
    @FXML
    void cancelar(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Inicializa los binding bidireccionales y la intnernacionalización de los componentes.
     */
    @FXML
    void initialize() {
        this.tfNombreCategoria.textProperty().bindBidirectional(this.categoria.nombreCategoriaProperty());
        this.internacionalizacion();

    }

    /**
     * Setter Usuario Sesion
     * @param usuarioSesion
     */
    public void setUsuarioSesion(UsuarioModel usuarioSesion) {
        this.usuarioSesion = usuarioSesion;
    }

    /**
     * Inicio del controlador.
     */
    public void iniciarControlador() {
        this.categoria.setUsuarioCreacion(this.usuarioSesion.getIdUsuario().getValue());
    }

    /**
     * Comprobaciones de los campos insertados.
     * @throws AplicacionExcepcion
     */
    public void comprobarCampos() throws AplicacionExcepcion {
        if (this.categoria.nombreCategoriaProperty().isEmpty().getValue()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.cate.nombre"));
        }
        if (this.categoria.nombreCategoriaProperty().getValueSafe().length() > 50) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.cate.nombrelongitud"));
        }
    }
    /**
     * Internacionalización de los elementos en pantalla.
     */
    private void internacionalizacion() {
        Internacionalizacion.localeProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.lbTitulo.textProperty().bind(Internacionalizacion.createStringBinding("cate.nuev.title"));
                this.lbNombreCategoria.textProperty().bind(Internacionalizacion.createStringBinding("cate.nuev.nombre"));
                this.btAceptar.textProperty().bind(Internacionalizacion.createStringBinding("cate.nuev.aceptar"));
                this.btCancelar.textProperty().bind(Internacionalizacion.createStringBinding("cate.nuev.cancelar"));
            }
        });
    }

    /**
     * Setter Logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }
}
