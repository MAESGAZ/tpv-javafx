package com.proyectotpv.proyectotpv.controladores.main;

import com.proyectotpv.proyectotpv.modelos.cambioclave.CambioClaveModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.cambioclave.CambioClaveUtilsBD;
import com.proyectotpv.proyectotpv.utiles.basedatos.usuario.UsuarioUtilsBD;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ControladorMainModificarUsuario {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btAceptar;

    @FXML
    private Button btCancelar;

    @FXML
    private TextField tfApellidos;

    @FXML
    private TextField tfClaveNueva;

    @FXML
    private TextField tfClaveRepetida;

    @FXML
    private TextField tfCorreoElectronico;

    @FXML
    private TextField tfNombre;

    @FXML
    private TextField tfUsuario;

    private boolean aplicarCambioClave = false;

    private UsuarioModel usuarioSesion;

    private UsuarioModel usuarioOriginal;

    private CambioClaveModel cambioClave;

    private Logs logs;

    /**
     * Función del botón cancelar
     * @param event
     */
    @FXML
    void cancelar(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Modifica los datos del usuario de la sesión.
     * @param event
     */
    @FXML
    void modificarUsuario(ActionEvent event) {
        boolean exito = false;
        UsuarioUtilsBD usuariosUtils = null;
        CambioClaveUtilsBD cambioClaveUtils = null;
        try {
            this.comprobaciones();
            usuariosUtils = new UsuarioUtilsBD();
            usuariosUtils.modificarUsuario(usuarioSesion);
            if (this.aplicarCambioClave) {
                cambioClaveUtils = new CambioClaveUtilsBD();
                cambioClaveUtils.cambiarClave(cambioClave);
            }
            exito = true;
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"), Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"), Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (NoSuchAlgorithmException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (usuariosUtils != null)
                usuariosUtils.cerrarConexion();
            if (exito) {
                ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
                this.usuarioOriginal = usuarioSesion;
            }

        }
    }

    /**
     * Inicializa la clase CambioClaveModel
     */
    @FXML
    void initialize() {
        this.cambioClave = new CambioClaveModel();

    }

    /**
     * Setter Usuario Sesion
     * @param usuarioSesion
     */
    public void setUsuario(UsuarioModel usuarioSesion) {
        this.usuarioOriginal = usuarioSesion;
        this.usuarioSesion = usuarioSesion.clone();
        this.cambioClave.setIdUsuario(this.usuarioSesion.getIdUsuario().getValue());
    }

    /**
     * Iniciamos los binding bidireccionales.
     */
    public void iniciarControlador() {
        this.tfUsuario.textProperty().bindBidirectional(this.usuarioSesion.getUsuario());
        this.tfNombre.textProperty().bindBidirectional(this.usuarioSesion.getNombreUsuario());
        this.tfApellidos.textProperty().bindBidirectional(this.usuarioSesion.getApellidosUsuario());
        this.tfCorreoElectronico.textProperty().bindBidirectional(this.usuarioSesion.getCorreoElectronico());
        this.tfClaveNueva.textProperty().bindBidirectional(this.cambioClave.claveNuevaRepetidaProperty());
        this.tfClaveRepetida.textProperty().bindBidirectional(this.cambioClave.claveNuevaRepetidaProperty());
    }

    /**
     * Comprobaciones de la aplicación.
     * @throws AplicacionExcepcion
     */
    private void comprobaciones() throws AplicacionExcepcion {
        if (this.usuarioSesion.getUsuario().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.usuario"));
        }
        if (this.usuarioSesion.getNombreUsuario().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.nombre"));
        }
        if (this.usuarioSesion.getApellidosUsuario().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.apellidos"));
        }
        if (this.usuarioSesion.getCorreoElectronico().isEmpty().get()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.correo"));
        }
        if (!this.comprobarEmail()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.correoformato"));
        }
        if (this.usuarioSesion.getUsuario().length().get() >= 50) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.usuariolongitud"));
        }
        if (this.usuarioSesion.getNombreUsuario().length().get() >= 40) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.nombrelongitud"));
        }
        if (this.usuarioSesion.getApellidosUsuario().length().get() >= 40) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.apellidoslongitud"));
        }
        if (this.usuarioSesion.getCorreoElectronico().length().get() >= 100) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.usua.correolongitud"));
        }
        if (this.cambioClave.claveNuevaProperty().isNotEmpty().getValue()) {
            if (this.cambioClave.claveNuevaProperty().isNotEqualTo(this.cambioClave.getClaveNuevaRepetida()).getValue()) {
                throw new AplicacionExcepcion(
                        Internacionalizacion.get("aler.usua.clavesrepetidas"));
            } else {
                this.aplicarCambioClave = true;
            }
        }
    }

    /**
     * Comprobamos que el Email cumple la expresión regular.
     * @return
     */
    private boolean comprobarEmail() {
        Pattern patron = Pattern.compile(Constantes.PATRON_CORREO, Pattern.CASE_INSENSITIVE);
        Matcher matcher = patron.matcher(this.usuarioSesion.getCorreoElectronico().getValueSafe());
        return matcher.find();
    }

    /**
     * Setter logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }

    /**
     * Getter Usuario Sesion
     * @return
     */
    public UsuarioModel getUsuarioSesion() {
        return this.usuarioOriginal;
    }
}
