package com.proyectotpv.proyectotpv.controladores.main.distribucion.terrazaitem;

import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import com.proyectotpv.proyectotpv.recursos.Internacionalizacion;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.util.converter.NumberStringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class ControladorTerrazaItem {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lbEstadoSitio;

    @FXML
    private Label lbNumeroSitio;

    @FXML
    private Label lbTitulo;

    @FXML
    private Label lbEstado;

    private PedidoModel pedido;

    /**
     * Inicializa el controlador y la internacionalización de la visual.
     */
    @FXML
    void initialize() {
        Internacionalizacion.localeProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.lbTitulo.textProperty().bind(Internacionalizacion.createStringBinding("distr.terr.title"));
                this.lbEstado.textProperty().bind(Internacionalizacion.createStringBinding("distr.terr.estado"));
            }
        });

    }

    /**
     * Setter Pedido
     * @param pedido
     */
    public void setPedido(PedidoModel pedido) {
        this.pedido = pedido;
        this.lbEstadoSitio.textProperty().bind(Bindings.when(this.pedido.subestadosProperty().isNotNull()).then(new SimpleStringProperty("Ocupado")).otherwise(new SimpleStringProperty("Libre")));
        this.lbNumeroSitio.textProperty().bindBidirectional(this.pedido.numeroSitioProperty(), new NumberStringConverter());
    }

    /**
     * Getter Pedido.
     * @return
     */
    public PedidoModel getPedido() {
        return this.pedido;
    }
}
