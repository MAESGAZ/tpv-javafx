package com.proyectotpv.proyectotpv.controladores.main.producto;

import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import com.proyectotpv.proyectotpv.modelos.main.producto.ProductoModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.categoria.CategoriasUtilsBD;
import com.proyectotpv.proyectotpv.utiles.basedatos.producto.ProductoUtilsBD;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ControladorEditarProducto {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ComboBox<CategoriaModel> cbCategoria;

    @FXML
    private TextArea taDescripcionProducto;

    @FXML
    private TextField tfCantidadProducto;

    @FXML
    private TextField tfNombreProducto;

    @FXML
    private TextField tfPrecioProducto;

    private ProductoModel producto;

    private ProductoModel productoOriginal;

    private ObservableList<CategoriaModel> listadoCategorias;

    private Logs logs;

    /**
     * Función del botón cancelar.
     * @param event
     */
    @FXML
    void cancelar(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Función para editar el producto con los datos introducidos
     * @param event
     */
    @FXML
    void crearProducto(ActionEvent event) {
        boolean exito = false;
        ProductoUtilsBD productosUtils = null;
        try {
            this.comprobaciones();
            productosUtils = new ProductoUtilsBD();
            productosUtils.modificarProducto(producto);
            exito = true;
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (productosUtils != null)
                productosUtils.cerrarConexion();
            if (exito)
                ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
        }
    }

    /**
     * Inicializa el controlador.
     */
    @FXML
    void initialize() {
        this.listadoCategoriasActivas();
        this.cbCategoria.setItems(listadoCategorias);
    }

    /**
     * Inicia los textfield con binding bidireccional y aplica restricciones.
     */
    public void iniciarControlador() {
        StringConverter<Number> converter = new StringConverter<Number>() {

            @Override
            public String toString(Number object) {
                return object == null ? Constantes.EMPTY_STRING : object.toString();
            }

            @Override
            public Number fromString(String string) {
                if (string == null) {
                    return 0;
                } else {
                    if (string.replaceAll(Constantes.PATRON_NUMERO, "").isEmpty())
                        return 0;
                    else
                        return Integer.parseInt(string.replaceAll(Constantes.PATRON_NUMERO, ""));
                }
            }
        };
        this.tfNombreProducto.textProperty().bindBidirectional(producto.nombreProductoProperty());
        this.taDescripcionProducto.textProperty().bindBidirectional(producto.descripcionProductoProperty());
        this.tfCantidadProducto.textProperty().bindBidirectional(producto.cantidadProperty(), converter);
        this.tfPrecioProducto.textProperty().bindBidirectional(producto.precioProductoProperty(),
                new NumberStringConverter());
        this.cbCategoria.valueProperty().bindBidirectional(producto.categoriaProperty());
        this.tfPrecioProducto.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue.contains("."))
                    tfPrecioProducto.setText(newValue.replace(".", ","));
            }
        });
    }

    /**
     * Setter Producto.
     * @param producto
     */
    public void setProducto(ProductoModel producto) {
        this.producto = producto.clone();
        this.productoOriginal = producto;
    }

    /**
     * Comprobaciones de los datos introducidos.
     * @throws AplicacionExcepcion
     */
    private void comprobaciones() throws AplicacionExcepcion {
        if (this.producto.getNombreProducto().isEmpty())
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.prod.nombre"));
        if (this.producto.getDescripcionProducto().isEmpty())
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.prod.descripcion"));
        if (this.producto.cantidadProperty().isEqualTo(0).getValue())
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.prod.cantidad"));
        if (this.producto.precioProductoProperty().isEqualTo(0).getValue())
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.prod.precio"));
        if (this.producto.nombreProductoProperty().length().greaterThan(50).getValue())
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.prod.nombrelongitud"));
        if (this.producto.descripcionProductoProperty().length().greaterThan(1000).getValue())
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.prod.descripcionlongitud"));
    }

    /**
     * Obtiene el listado de categorías activas.
     */
    private void listadoCategoriasActivas() {
        CategoriasUtilsBD categoriasUtils = null;
        try {
            categoriasUtils = new CategoriasUtilsBD();
            this.listadoCategorias = categoriasUtils.listadoCategoriasActivas();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (categoriasUtils != null)
                categoriasUtils.cerrarConexion();
        }
    }

    /**
     * Setter logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }
}
