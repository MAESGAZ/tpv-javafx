package com.proyectotpv.proyectotpv.controladores.main.producto;

import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.modelos.main.producto.ProductoModel;
import com.proyectotpv.proyectotpv.recursos.Alerta;
import com.proyectotpv.proyectotpv.recursos.Constantes;
import com.proyectotpv.proyectotpv.recursos.Internacionalizacion;
import com.proyectotpv.proyectotpv.recursos.Logs;
import com.proyectotpv.proyectotpv.utiles.basedatos.producto.ProductoUtilsBD;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class ControladorProductos {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btActivarProducto;

    @FXML
    private Button btDesactivarProducto;

    @FXML
    private Button btEditarProducto;

    @FXML
    private Button btEliminarProducto;

    @FXML
    private Button btNuevoProducto;

    @FXML
    private TableColumn<ProductoModel, Number> tcCantidad;

    @FXML
    private TableColumn<ProductoModel, String> tcCategoriaProducto;

    @FXML
    private TableColumn<ProductoModel, String> tcEstadoProducto;

    @FXML
    private TableColumn<ProductoModel, String> tcFechaCreacion;

    @FXML
    private TableColumn<ProductoModel, String> tcFechaModificacion;

    @FXML
    private TableColumn<ProductoModel, String> tcNombreProducto;

    @FXML
    private TableColumn<ProductoModel, Number> tcPrecioProducto;

    @FXML
    private TextField tfFiltroProductos;

    @FXML
    private TableView<ProductoModel> tvProductos;

    @FXML
    private Label lbFiltro;

    private Logs logs;

    private FilteredList<ProductoModel> filtro;

    private UsuarioModel usuarioSesion;

    private ObservableList<ProductoModel> listadoProductos = FXCollections.observableArrayList();

    /**
     * Activa el producto seleccionado.
     * @param event
     */
    @FXML
    void activarProducto(ActionEvent event) {
        ProductoUtilsBD productoUtils = null;
        try {
            productoUtils = new ProductoUtilsBD();
            productoUtils.activarProducto(this.tvProductos.getSelectionModel().getSelectedItem());
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (productoUtils != null)
                productoUtils.cerrarConexion();
        }
        this.refrescarListado();
    }

    /**
     * Desactiva el producto seleccionado.
     * @param event
     */
    @FXML
    void desactivarProducto(ActionEvent event) {
        ProductoUtilsBD productoUtils = null;
        try {
            productoUtils = new ProductoUtilsBD();
            productoUtils.desactivarProducto(this.tvProductos.getSelectionModel().getSelectedItem());
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (productoUtils != null)
                productoUtils.cerrarConexion();
        }
        this.refrescarListado();
    }

    /**
     * Abre la visual para editar un producto.
     */
    @FXML
    void editarProducto(ActionEvent event) {
        Stage stage = new Stage();
        FXMLLoader viewEditarProducto = new FXMLLoader();
        viewEditarProducto
                .setLocation(getClass().getResource(Constantes.FXML_EDITAR_PRODUCTO));
        viewEditarProducto.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            stage.setScene(new Scene((AnchorPane) viewEditarProducto.load()));
            ControladorEditarProducto controlador = viewEditarProducto.getController();
            controlador.setProducto(this.tvProductos.getSelectionModel().getSelectedItem());
            controlador.setLogs(logs);
            controlador.iniciarControlador();
            stage.setResizable(false);
            stage.setTitle(Internacionalizacion.get("prod.edit.titulo"));
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            stage.showAndWait();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
        this.refrescarListado();
    }

    /**
     * Elimina el producto seleccionado.
     * @param event
     */
    @FXML
    void eliminarProducto(ActionEvent event) {
        Alert alerta = new Alert(Alert.AlertType.CONFIRMATION, "¿Eliminar Producto?", ButtonType.YES, ButtonType.CANCEL);
        Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
        alerta.showAndWait();
        if (alerta.getResult() == ButtonType.YES) {
            ProductoUtilsBD productosUtils = null;
            try {
                productosUtils = new ProductoUtilsBD();
                productosUtils.eliminarProducto(this.tvProductos.getSelectionModel().getSelectedItem());
            } catch (ClassNotFoundException e) {
                Alerta alertaError = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                Stage stageAlerta = (Stage) alertaError.getDialogPane().getScene().getWindow();
                stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alertaError.showAndWait();
            } catch (SQLException e) {
                Alerta alertaError = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                        Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
                Stage stageAlerta = (Stage) alertaError.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
                stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alertaError.showAndWait();
            } finally {
                if (productosUtils != null)
                    productosUtils.cerrarConexion();
            }
            this.refrescarListado();
        }
    }

    /**
     * Abre la visual para crear un nuevo producto.
     * @param event
     */
    @FXML
    void nuevoProducto(ActionEvent event) {
        Stage stage = new Stage();
        FXMLLoader viewNuevoProducto = new FXMLLoader();
        viewNuevoProducto
                .setLocation(getClass().getResource(Constantes.FXML_NUEVO_PRODUCTO));
        viewNuevoProducto.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            stage.setScene(new Scene((AnchorPane) viewNuevoProducto.load()));
            ControladorNuevoProducto controlador = viewNuevoProducto.getController();
            controlador.setUsuarioSesion(usuarioSesion);
            controlador.setLogs(logs);
            stage.setResizable(false);
            stage.setTitle(Internacionalizacion.get("prod.nuev.titulo"));
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            stage.showAndWait();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
        this.refrescarListado();
    }

    /**
     * Inicia la internacionalización, activa o desactiva los botones, inicia las columnas de la tabla.
     */
    @FXML
    void initialize() {
        this.internacionalizacion();
        this.btActivarProducto.setDisable(true);
        this.btDesactivarProducto.setDisable(true);
        this.btEditarProducto.setDisable(true);
        this.btEliminarProducto.setDisable(true);

        this.tcNombreProducto.setCellValueFactory(cellData -> cellData.getValue().nombreProductoProperty());
        this.tcCantidad.setCellValueFactory(cellData -> cellData.getValue().cantidadProperty());
        this.tcPrecioProducto.setCellValueFactory(cellData -> cellData.getValue().precioProductoProperty());
        this.tcEstadoProducto.setCellValueFactory(cellData -> Bindings.when(cellData.getValue().estadoProductoProperty())
                .then(new SimpleStringProperty(Constantes.ESTADO_ACTIVADO)).otherwise(new SimpleStringProperty(Constantes.ESTADO_DESACTIVADO)));
        this.tcFechaCreacion.setCellValueFactory(cellData -> {
            LocalDateTime fecha = cellData.getValue().getFechaCreacion();
            return new SimpleStringProperty(
                    fecha.getDayOfMonth() + "-" + fecha.getMonthValue() + "-" + fecha.getYear());
        });
        this.tcFechaModificacion
                .setCellValueFactory(cellData -> {
                    if (cellData.getValue().fechaModificacionProperty().isNull().getValue()) {
                        return new SimpleStringProperty(Constantes.FECHA_VACIA);
                    } else {
                        LocalDateTime fecha = cellData.getValue().getFechaModificacion();
                        return new SimpleStringProperty(
                                fecha.getDayOfMonth() + "-" + fecha.getMonthValue() + "-" + fecha.getYear());
                    }
                });

        this.tcCategoriaProducto
                .setCellValueFactory(cellData -> cellData.getValue().getCategoria().nombreCategoriaProperty());

        this.refrescarListado();
        this.filtro = new FilteredList<ProductoModel>(this.listadoProductos);
        this.tvProductos.setItems(filtro);

        this.tvProductos.getSelectionModel().selectedItemProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.btEditarProducto.setDisable(false);
                this.btEliminarProducto.setDisable(false);
                if (newValue.isEstadoProducto()) {
                    this.btActivarProducto.setDisable(true);
                    this.btDesactivarProducto.setDisable(false);
                } else {
                    this.btActivarProducto.setDisable(false);
                    this.btDesactivarProducto.setDisable(true);
                }
            } else {
                this.btEditarProducto.setDisable(true);
                this.btActivarProducto.setDisable(true);
                this.btDesactivarProducto.setDisable(true);
                this.btEliminarProducto.setDisable(true);
            }
        });

        this.tfFiltroProductos.textProperty().addListener((old, oldValue, newValue) -> {
            this.filtro.setPredicate(producto -> {
                if (producto.getNombreProducto().toLowerCase().contains(newValue.toLowerCase()) ||
                        producto.cantidadProperty().asString().getValueSafe().contains(newValue) ||
                        producto.precioProductoProperty().asString().getValueSafe().contains(newValue) ||
                        producto.categoriaProperty().getValue().getNombreCategoria().toLowerCase().contains(newValue.toLowerCase())) {
                    return true;
                } else {
                    return false;
                }
            });
        });

    }

    /**
     * Setter Usuario Sesion
     * @param usuario
     */
    public void setUsuarioSesion(UsuarioModel usuario) {
        this.usuarioSesion = usuario;
    }

    /**
     * Obtiene el listado de productos de BBDD.
     */
    private void refrescarListado() {
        ProductoUtilsBD productosUtils = null;
        try {
            productosUtils = new ProductoUtilsBD();
            this.listadoProductos.setAll(productosUtils.listadoProductos());
            this.tvProductos.refresh();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (productosUtils != null)
                productosUtils.cerrarConexion();
        }
    }

    /**
     * Setter Logs
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }

    /**
     * Inicia la internacionalización de los componentes visuales.s
     */
    private void internacionalizacion() {
        Internacionalizacion.localeProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.btNuevoProducto.textProperty().bind(Internacionalizacion.createStringBinding("prod.nuevoproducto"));
                this.btEditarProducto.textProperty().bind(Internacionalizacion.createStringBinding("prod.editarproducto"));
                this.btDesactivarProducto.textProperty().bind(Internacionalizacion.createStringBinding("prod.desactivar"));
                this.btActivarProducto.textProperty().bind(Internacionalizacion.createStringBinding("prod.activar"));
                this.btEliminarProducto.textProperty().bind(Internacionalizacion.createStringBinding("prod.eliminar"));
                this.lbFiltro.textProperty().bind(Internacionalizacion.createStringBinding("prod.filtro"));
                this.tcNombreProducto.textProperty().bind(Internacionalizacion.createStringBinding("prod.nombre"));
                this.tcCantidad.textProperty().bind(Internacionalizacion.createStringBinding("prod.cantidad"));
                this.tcPrecioProducto.textProperty().bind(Internacionalizacion.createStringBinding("prod.precio"));
                this.tcEstadoProducto.textProperty().bind(Internacionalizacion.createStringBinding("prod.estado"));
                this.tcFechaCreacion.textProperty().bind(Internacionalizacion.createStringBinding("prod.fecha"));
                this.tcFechaModificacion.textProperty().bind(Internacionalizacion.createStringBinding("prod.fechaModi"));
                this.tcCategoriaProducto.textProperty().bind(Internacionalizacion.createStringBinding("prod.categoria"));
            }
        });
    }
}
