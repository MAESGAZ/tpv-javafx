package com.proyectotpv.proyectotpv.controladores.main.distribucion.barraitem;

import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import com.proyectotpv.proyectotpv.recursos.Internacionalizacion;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.util.converter.NumberStringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class ControladorBarraItem {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lbEstado;

    @FXML
    private Label lbEstadoSitio;

    @FXML
    private Label lbNumeroSitio;

    @FXML
    private Label lbTitulo;

    private PedidoModel pedido;

    /**
     * Inicializa el controlador y la internacionalización de los elementos.
     */
    @FXML
    void initialize() {
        Internacionalizacion.localeProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.lbTitulo.textProperty().bind(Internacionalizacion.createStringBinding("distr.barr.title"));
                this.lbEstado.textProperty().bind(Internacionalizacion.createStringBinding("distr.barr.estado"));
            }
        });
    }

    /**
     * Setter Pedido
     * @param pedido
     */
    public void setPedido(PedidoModel pedido) {
        this.pedido = pedido;
        this.lbEstadoSitio.textProperty().bind(Bindings.when(this.pedido.subestadosProperty().isNotNull()).then(new SimpleStringProperty("Ocupado")).otherwise(new SimpleStringProperty("Libre")));
        this.lbNumeroSitio.textProperty().bindBidirectional(this.pedido.numeroSitioProperty(), new NumberStringConverter());
    }

    /**
     * Getter Pedido.
     * @return
     */
    public PedidoModel getPedido() {
        return this.pedido;
    }
}
