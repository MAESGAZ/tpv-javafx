package com.proyectotpv.proyectotpv.controladores.main.categorias;

import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.modelos.main.categorias.CategoriaModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.categoria.CategoriasUtilsBD;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class ControladorViewCategorias {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btActivarCategoria;

    @FXML
    private Button btCrearCategoria;

    @FXML
    private Button btDesactivarCategoria;

    @FXML
    private Button btEditarCategoria;

    @FXML
    private Button btEliminarCategoria;

    @FXML
    private Label lbFiltro;

    @FXML
    private TableColumn<CategoriaModel, String> tcEstadoCategoria;

    @FXML
    private TableColumn<CategoriaModel, String> tcFechaCreacion;

    @FXML
    private TableColumn<CategoriaModel, String> tcFechaModificacion;

    @FXML
    private TableColumn<CategoriaModel, String> tcNombreCategoria;

    @FXML
    private TextField tfFiltroCategorias;

    @FXML
    private TableView<CategoriaModel> tvCategorias;

    private Logs logs;

    private FilteredList<CategoriaModel> filtro;

    private UsuarioModel usuarioSesion;

    private ObservableList<CategoriaModel> listadoCategorias = FXCollections.observableArrayList();

    /**
     * Activa una Categoría desactivada.
     *
     * @param event
     */
    @FXML
    void activarCategoria(ActionEvent event) {
        CategoriasUtilsBD categoriasUtils = null;
        try {
            categoriasUtils = new CategoriasUtilsBD();
            if (categoriasUtils.activarCategoria(this.tvCategorias.getSelectionModel().getSelectedItem()) != 1) {
                throw new AplicacionExcepcion("Ha ocurrido un erro al actualizar el estado de la categoría");
            }
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (categoriasUtils != null)
                categoriasUtils.cerrarConexion();
            this.refrescarListado();
        }
    }

    /**
     * Abre la visual para crear una nueva Categoría.
     *
     * @param event
     */
    @FXML
    void crearCategoria(ActionEvent event) {
        Stage stage = new Stage();
        stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
        stage.setTitle(Internacionalizacion.get("cate.nuev.title"));
        FXMLLoader viewNuevaCategoria = new FXMLLoader();
        viewNuevaCategoria.setLocation(getClass().getResource(Constantes.FXML_NUEVA_CATEGORIA));
        viewNuevaCategoria.setResources(
                ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            stage.setScene(new Scene((AnchorPane) viewNuevaCategoria.load()));
            ControladorNuevaCategoria controller = viewNuevaCategoria.getController();
            controller.setLogs(logs);
            controller.setUsuarioSesion(usuarioSesion);
            stage.setResizable(false);
            stage.showAndWait();
            refrescarListado();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
    }

    /**
     * Desactiva la categoría seleccionada.
     */
    @FXML
    void desactivarCategoria(ActionEvent event) {
        CategoriasUtilsBD categoriasUtils = null;
        try {
            categoriasUtils = new CategoriasUtilsBD();
            if (categoriasUtils.desactivarCategoria(this.tvCategorias.getSelectionModel().getSelectedItem()) != 1)
                throw new AplicacionExcepcion("Ha ocurrido un error al desactivar la categoria");
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (categoriasUtils != null)
                categoriasUtils.cerrarConexion();
            this.refrescarListado();
        }
    }

    /**
     * Edita la categoría seleccionada abriendo la visual para editar la categoría.
     *
     * @param event
     */
    @FXML
    void editarCategoria(ActionEvent event) {
        Stage stage = new Stage();
        stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
        stage.setTitle(Internacionalizacion.get("cate.edit.title"));
        stage.setResizable(false);
        FXMLLoader viewEditarCategoria = new FXMLLoader();
        viewEditarCategoria.setLocation(getClass().getResource(Constantes.FXML_EDITAR_CATEGORIA));
        viewEditarCategoria.setResources(
                ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getLocale()));
        try {
            stage.setScene(new Scene((AnchorPane) viewEditarCategoria.load()));
            ControladorEditarCategoria controlador = viewEditarCategoria.getController();
            controlador.setCategoria(this.tvCategorias.getSelectionModel().getSelectedItem());
            controlador.setLogs(logs);
            controlador.setUsuarioSesion(usuarioSesion);
            controlador.iniciarControlador();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stageAlerta = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stageAlerta.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        }
        stage.showAndWait();
        this.refrescarListado();
    }

    /**
     * Elimina la categoría seleccionada. Muestra una alerta para confirmar.
     *
     * @param event
     */
    @FXML
    void eliminarCategoria(ActionEvent event) {
        Alerta alertaCon = new Alerta(Alert.AlertType.CONFIRMATION,
                Internacionalizacion.get("aler.controllercategoria.titulo"),
                Internacionalizacion.get("aler.controllercategoria.cabecera"),
                Internacionalizacion.get("aler.controllercategoria.cuerpo"));
        alertaCon.showAndWait();
        if (alertaCon.getResult() == ButtonType.OK) {
            CategoriasUtilsBD categoriasUtils = null;
            try {
                categoriasUtils = new CategoriasUtilsBD();
                int prueba = categoriasUtils.eliminarCategoria(this.tvCategorias.getSelectionModel().getSelectedItem());
            } catch (ClassNotFoundException e) {
                Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                        Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
                Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error: " + e.getMessage());
                stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            } catch (SQLException e) {
                Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                        Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
                Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
                this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
                stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
                alerta.showAndWait();
            } finally {
                if (categoriasUtils != null)
                    categoriasUtils.cerrarConexion();
            }
            this.refrescarListado();
        }

    }

    /**
     * Inicializa los botones y las columnas de la tabla para mostra la información.
     * También obtenemos el listado de categorías.
     */
    @FXML
    void initialize() {
        this.internacionalizacion();
        this.btActivarCategoria.setDisable(true);
        this.btDesactivarCategoria.setDisable(true);
        this.btEditarCategoria.setDisable(true);
        this.btEliminarCategoria.setDisable(true);
        this.tcNombreCategoria.setCellValueFactory(cellData -> cellData.getValue().nombreCategoriaProperty());
        this.tcEstadoCategoria.setCellValueFactory(cellData -> cellData.getValue().estadoCategoriaProperty().getValue()
                ? new SimpleStringProperty(Constantes.ESTADO_ACTIVADO)
                : new SimpleStringProperty(Constantes.ESTADO_DESACTIVADO));
        this.tcFechaCreacion.setCellValueFactory(cellData -> {
            LocalDateTime fechaCreacion = cellData.getValue().getFechaCreacion();
            return new SimpleStringProperty(fechaCreacion.getDayOfMonth() + "-" + fechaCreacion.getMonthValue() + "-"
                    + fechaCreacion.getYear());
        });
        this.tcFechaModificacion.setCellValueFactory(cellData -> {
            if (!cellData.getValue().fechaModificacionProperty().isNull().getValue()) {
                LocalDateTime fechaModificacion = cellData.getValue().getFechaModificacion();
                return new SimpleStringProperty(fechaModificacion.getDayOfMonth() + "-"
                        + fechaModificacion.getMonthValue() + "-" + fechaModificacion.getYear());
            } else {
                return new SimpleStringProperty(Constantes.FECHA_VACIA);
            }
        });

        this.filtro = new FilteredList<CategoriaModel>(this.listadoCategorias);
        this.tvCategorias.setItems(filtro);
        this.refrescarListado();
        // Escuchador para cuando cambiamos de item, para bloquear o desbloquear
        // botones.
        this.tvCategorias.getSelectionModel().selectedItemProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.btEditarCategoria.setDisable(false);
                this.btEliminarCategoria.setDisable(false);
                if (newValue.isEstadoCategoria()) {
                    this.btDesactivarCategoria.setDisable(false);
                    this.btActivarCategoria.setDisable(true);
                } else {
                    this.btDesactivarCategoria.setDisable(true);
                    this.btActivarCategoria.setDisable(false);
                }
            } else {
                this.btEditarCategoria.setDisable(true);
                this.btEliminarCategoria.setDisable(true);
                this.btDesactivarCategoria.setDisable(true);
                this.btActivarCategoria.setDisable(true);
            }
        });
        // Funcionalidad del filtro.
        this.tfFiltroCategorias.textProperty().addListener((old, oldValue, newValue) -> {
            this.filtro.setPredicate(categoria -> {
                if (categoria.getNombreCategoria().toLowerCase().contains(newValue.toLowerCase())) {
                    return true;
                } else {
                    return false;
                }
            });
        });
    }

    /**
     * Setter del usuario de la sesión.
     *
     * @param usuarioSesion
     */
    public void setUsuarioSesion(UsuarioModel usuarioSesion) {
        this.usuarioSesion = usuarioSesion;
    }

    /**
     * Consulta el listado de categorías en BBDD.
     */
    private void refrescarListado() {
        CategoriasUtilsBD categoriasUtils = null;
        try {
            categoriasUtils = new CategoriasUtilsBD();
            this.listadoCategorias.setAll(categoriasUtils.listadoCategorias());
            this.tvCategorias.refresh();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"),
                    Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"),
                    Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: " + e.getMessage());
            stage.getIcons().add(new Image(getClass().getResourceAsStream(Constantes.ELMT_ICON)));
            alerta.showAndWait();
        } finally {
            if (categoriasUtils != null)
                categoriasUtils.cerrarConexion();
        }
    }

    /**
     * Internacionalización de los componentes de la visual.
     */
    private void internacionalizacion() {
        Internacionalizacion.localeProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.btCrearCategoria.textProperty().bind(Internacionalizacion.createStringBinding("cate.crearcate"));
                this.btEditarCategoria.textProperty().bind(Internacionalizacion.createStringBinding("cate.editarcate"));
                this.btActivarCategoria.textProperty()
                        .bind(Internacionalizacion.createStringBinding("cate.activarcate"));
                this.btDesactivarCategoria.textProperty()
                        .bind(Internacionalizacion.createStringBinding("cate.desactivarcate"));
                this.btEliminarCategoria.textProperty()
                        .bind(Internacionalizacion.createStringBinding("cate.eliminarcate"));
                this.lbFiltro.textProperty().bind(Internacionalizacion.createStringBinding("cate.filtro"));
                this.tcNombreCategoria.textProperty().bind(Internacionalizacion.createStringBinding("cate.tcnombre"));
                this.tcEstadoCategoria.textProperty().bind(Internacionalizacion.createStringBinding("cate.tcestado"));
                this.tcFechaCreacion.textProperty()
                        .bind(Internacionalizacion.createStringBinding("cate.tcfechacreacion"));
                this.tcFechaModificacion.textProperty()
                        .bind(Internacionalizacion.createStringBinding("cate.tcfechamodifi"));
            }
        });
    }

    /**
     * Setter de los logs.
     *
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }
}
