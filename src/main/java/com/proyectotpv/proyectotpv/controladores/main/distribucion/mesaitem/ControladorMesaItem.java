package com.proyectotpv.proyectotpv.controladores.main.distribucion.mesaitem;

import com.proyectotpv.proyectotpv.modelos.pedido.PedidoModel;
import com.proyectotpv.proyectotpv.recursos.Internacionalizacion;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.util.converter.NumberStringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class ControladorMesaItem {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label estadoMesa;

    @FXML
    private Label numeroMesa;

    @FXML
    private Label lbTitulo;

    @FXML
    private Label lbEstado;

    private PedidoModel pedido;

    /**
     * Inicia el controlador y la internacionalización de los elementos.
     */
    @FXML
    void initialize() {
        Internacionalizacion.localeProperty().addListener((old, oldValue, newValue) -> {
            if (newValue != null) {
                this.lbTitulo.textProperty().bind(Internacionalizacion.createStringBinding("distr.mesa.title"));
                this.lbEstado.textProperty().bind(Internacionalizacion.createStringBinding("distr.mesa.estado"));
            }
        });
    }

    /**
     * Setter Pedido.
     * @param pedido
     */
    public void setPedido(PedidoModel pedido) {
        this.pedido = pedido;
        this.numeroMesa.textProperty().bindBidirectional(this.pedido.numeroSitioProperty(), new NumberStringConverter());
        this.estadoMesa.textProperty().bind(Bindings.when(this.pedido.subestadosProperty().isNotNull()).then(new SimpleStringProperty("Ocupado")).otherwise(new SimpleStringProperty("Libre")));
    }

    /**
     * Getter Pedido
     * @return
     */
    public PedidoModel getPedido() {
        return this.pedido;
    }
}
