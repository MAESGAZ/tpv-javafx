package com.proyectotpv.proyectotpv.controladores.cambioclave;

import com.proyectotpv.proyectotpv.controladores.logininicio.ControladorLoginInicio;
import com.proyectotpv.proyectotpv.modelos.cambioclave.CambioClaveModel;
import com.proyectotpv.proyectotpv.modelos.logininicio.UsuarioModel;
import com.proyectotpv.proyectotpv.recursos.*;
import com.proyectotpv.proyectotpv.utiles.basedatos.cambioclave.CambioClaveUtilsBD;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Objects;
import java.util.ResourceBundle;

public class ControladorCambioClave {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btAceptar;

    @FXML
    private Button btCancelar;

    @FXML
    private Label lbTextoInformativo;

    @FXML
    private Label lbTitulo;

    @FXML
    private TextField tfClaveNueva;

    @FXML
    private TextField tfClaveNuevaRepetida;

    @FXML
    private TextField tfUsuario;

    private UsuarioModel usuario;

    private Logs logs;

    private final CambioClaveModel claveNueva = new CambioClaveModel();

    /**
     * Función que recoge los datos del formulario y los envía a Base de Datos para modificar la contraseña.
     * @param event Envento del botón.
     */
    @FXML
    void aceptar(ActionEvent event) {
        boolean exitoOperacion = false;
        CambioClaveUtilsBD cambioClaveUtils = null;
        try {
            //Comprobamos que cumple los requisitos.
            this.comprobaciones();
            cambioClaveUtils = new CambioClaveUtilsBD();
            cambioClaveUtils.cambiarClave(claveNueva);
            exitoOperacion = true;
            //Excepciones por si falla, muestra un tipo distinto de alerta y su descripción.
        } catch (AplicacionExcepcion e) {
            Alerta alerta = new Alerta(Alert.AlertType.WARNING, Internacionalizacion.get("aler.warn.titulo"), Internacionalizacion.get("aler.warn.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().warning("Aviso: "+e.getMessage());
            stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            alerta.showAndWait();
        } catch (NoSuchAlgorithmException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            alerta.showAndWait();
        } catch (SQLException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.bbdd.titulo"), Internacionalizacion.get("aler.bbdd.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error Base de Datos: "+e.getMessage());
            stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            alerta.showAndWait();
        } catch (ClassNotFoundException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stage = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            alerta.showAndWait();
        } finally {
            if (cambioClaveUtils != null) cambioClaveUtils.cerrarConexion();
            // Si se ha cambiado la contraseña, redirigimos a la ventana de login.
            if (exitoOperacion) {
                ((Stage) this.tfUsuario.getScene().getWindow()).close();
                this.iniciarLoginInicio();
            }
        }
    }

    /**
     * Función del botón cancelar.
     * Cierra la ventana de cambio de clave y abre la de iniciar login.
     * @param event
     */
    @FXML
    void cancelar(ActionEvent event) {
        this.logs.getLogs().info("Salimos del cambio de clave.");
        ((Stage) this.tfUsuario.getScene().getWindow()).close();
        this.iniciarLoginInicio();
    }

    /**
     * Inicializa el controlador
     */
    @FXML
    void initialize() {

    }

    /**
     * Setter del usuario.
     * @param usuario Usuario de la sesion
     */
    public void setUsuario(UsuarioModel usuario) {
        this.usuario = usuario;
    }

    /**
     * Inicia los binding bidireccionales.
     */
    public void iniciarControlador() {
        this.tfUsuario.textProperty().bindBidirectional(usuario.getUsuario());
        this.claveNueva.setIdUsuario(this.usuario.getIdUsuario().getValue());
        this.tfClaveNueva.textProperty().bindBidirectional(claveNueva.claveNuevaProperty());
        this.tfClaveNuevaRepetida.textProperty().bindBidirectional(claveNueva.claveNuevaRepetidaProperty());
    }

    /**
     * Realiza las comprobaciones oportunas para el cambio de clave.
     * @throws AplicacionExcepcion Excepción si no se cumple una condición.
     */
    private void comprobaciones() throws AplicacionExcepcion {
        if (this.claveNueva.claveNuevaProperty().isEmpty().getValue()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.controllercambioclave.clavevacia"));
        }
        if (this.claveNueva.claveNuevaRepetidaProperty().isEmpty().getValue()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.controllercambioclave.claverepevacia"));
        }
        if (this.claveNueva.claveNuevaProperty().isNotEqualTo(this.claveNueva.claveNuevaRepetidaProperty()).getValue()) {
            throw new AplicacionExcepcion(Internacionalizacion.get("aler.controllercambioclave.repetido"));
        }
    }

    /**
     * Setter de los logs de la aplicación.
     * @param logs
     */
    public void setLogs(Logs logs) {
        this.logs = logs;
    }

    /**
     * Inicia la visual de Inicio de login.
     */
    private void iniciarLoginInicio() {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        this.logs.getLogs().info("Cargamos la ventana de Login de Inicio.");
        loader.setLocation(getClass().getResource(Constantes.FXML_LOGIN_INICIO));
        loader.setResources(ResourceBundle.getBundle(Constantes.PATH_INTERNATIONALIZATION, Internacionalizacion.getDefaultLocale()));
        try {
            stage.setScene(new Scene((AnchorPane) loader.load()));
            ControladorLoginInicio controllerLogin = loader.getController();
            controllerLogin.setLogs(logs);
            stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            stage.setResizable(false);
            stage.setTitle(Internacionalizacion.get("title"));
            stage.show();
        } catch (IOException e) {
            Alerta alerta = new Alerta(Alert.AlertType.ERROR, Internacionalizacion.get("aler.erro.titulo"), Internacionalizacion.get("aler.erro.cabecera"), e.getMessage());
            Stage stageAlert = (Stage) alerta.getDialogPane().getScene().getWindow();
            this.logs.getLogs().severe("Error: "+e.getMessage());
            stageAlert.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream(Constantes.ELMT_ICON))));
            alerta.showAndWait();
        }

    }
}
