module com.proyectotpv.proyectotpv {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires java.logging;
    requires java.sql;

    opens com.proyectotpv.proyectotpv to javafx.fxml;
    exports com.proyectotpv.proyectotpv;

    exports com.proyectotpv.proyectotpv.controladores.logininicio;
    opens com.proyectotpv.proyectotpv.controladores.logininicio to javafx.fxml;
    exports com.proyectotpv.proyectotpv.basedatos;
    opens com.proyectotpv.proyectotpv.basedatos to javafx.fxml;
    exports com.proyectotpv.proyectotpv.controladores.main;
    opens com.proyectotpv.proyectotpv.controladores.main;
    exports com.proyectotpv.proyectotpv.controladores.main.categorias;
    opens com.proyectotpv.proyectotpv.controladores.main.categorias;
    exports com.proyectotpv.proyectotpv.controladores.main.clientes;
    opens com.proyectotpv.proyectotpv.controladores.main.clientes;
    exports com.proyectotpv.proyectotpv.controladores.main.distribucion;
    opens com.proyectotpv.proyectotpv.controladores.main.distribucion;
    exports com.proyectotpv.proyectotpv.controladores.main.distribucion.barraitem;
    opens com.proyectotpv.proyectotpv.controladores.main.distribucion.barraitem;
    exports com.proyectotpv.proyectotpv.controladores.main.distribucion.mesaitem;
    opens com.proyectotpv.proyectotpv.controladores.main.distribucion.mesaitem;
    exports com.proyectotpv.proyectotpv.controladores.main.distribucion.terrazaitem;
    opens com.proyectotpv.proyectotpv.controladores.main.distribucion.terrazaitem;
    exports com.proyectotpv.proyectotpv.controladores.main.producto;
    opens com.proyectotpv.proyectotpv.controladores.main.producto;
    exports com.proyectotpv.proyectotpv.controladores.main.usuario;
    opens com.proyectotpv.proyectotpv.controladores.main.usuario;
    exports com.proyectotpv.proyectotpv.controladores.pedido;
    opens com.proyectotpv.proyectotpv.controladores.pedido;
    exports com.proyectotpv.proyectotpv.controladores.pedido.categoria;
    opens com.proyectotpv.proyectotpv.controladores.pedido.categoria;
    exports com.proyectotpv.proyectotpv.controladores.pedido.producto;
    opens com.proyectotpv.proyectotpv.controladores.pedido.producto;
    exports com.proyectotpv.proyectotpv.controladores.cambioclave;
    opens com.proyectotpv.proyectotpv.controladores.cambioclave;
}